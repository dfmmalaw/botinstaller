﻿using Microsoft.Extensions.Configuration;

namespace OpenBots.Server.Infrastructure.Email
{
    /// <summary>
    /// Settings for SMTP Mail
    /// </summary>
    public class SmtpEmailSetting
    {

        public SmtpEmailSetting(IConfiguration config)
        {
            EnableSsl = bool.Parse(config["Email.Smtp:EnableSsl"]);
            Host = config["Email.Smtp:Host"];
            Port = int.Parse(config["Email.Smtp:Port"]);
            Username = config["Email.Smtp:Username"];
            Password = config["Email.Smtp:Password"];
        }
        
        /// <summary>
        /// Host or IP address of the SMTP Server
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// TCP Port for the SMTP Server 
        /// You can use Port 25 or 587
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// The Username for the SMTP Service
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Password for the SMTP Service
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Should SSL be enabled. SSL Should be Enabled for Security.
        /// </summary>
        public bool EnableSsl { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace OpenBots.Server.Infrastructure.Email
{
    public class SendEmailException : Exception
    {
        public SendEmailException()
        {
        }

        public SendEmailException(string message) : base(message)
        {
        }

        public SendEmailException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SendEmailException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

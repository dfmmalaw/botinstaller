﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace OpenBots.Server.Infrastructure.Email
{
    public class CannotSendEmailException : SendEmailException
    {
        public CannotSendEmailException()
        {
        }

        public CannotSendEmailException(string message) : base(message)
        {
        }

        public CannotSendEmailException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CannotSendEmailException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

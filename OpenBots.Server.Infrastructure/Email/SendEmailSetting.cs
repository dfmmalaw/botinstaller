﻿using Microsoft.Extensions.Configuration;

namespace OpenBots.Server.Infrastructure.Email
{
    /// <summary>
    /// Common Configuration for All Send Email Functionality in the System
    /// </summary>
    public class SendEmailSetting
    {
        public SendEmailSetting(IConfiguration config)
        {
            DisableEmail = bool.Parse(config["Email:DisableEmail"]);
            FromEmail = config["Email:FromEmail"];
            From = config["Email:From"];
            SubjectPrefix = config["Email:SubjectPrefix"];
            SubjectSuffix = config["Email:SubjectSuffix"];
            BodyPrefix = config["Email:BodyPrefix"];
            BodySuffix = config["Email:BodySuffix"];
            AllowedDomains = config["Email:AllowedDomains"];
            BlockedDomains = config["Email:BlockedDomains"];
            AddTo = config["Email:AddTo"];
            AddCC = config["Email:AddCC"];
            AddBCC = config["Email:AddBCC"];
        }

        /// <summary>
        /// Will force Disable sending out any emails from the System
        /// </summary>

        public bool DisableEmail { get; set; }

        /// <summary>
        /// The 'From Email Address' for all outgoing emails.If a from Address is NOT provided.
        /// </summary>
        public string FromEmail { get; set; }

        /// <summary>
        /// The 'From Name' for all outgoing emails.If a from is NOT provided.
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Forces to add a Prefix to the Subject of All Emails going out
        /// </summary>
        public string SubjectPrefix { get; set; }

        /// <summary>
        /// Forces to add a Suffix to the Subject of All Emails going out
        /// </summary>
        public string SubjectSuffix { get; set; }

        /// <summary>
        /// Forces to add a Prefix to the Body of all Emails.
        /// </summary>
        public string BodyPrefix { get; set; }


        /// <summary>
        /// Forces to add a Suffix to the Body of all Emails.
        /// </summary>
        public string BodySuffix { get; set; }

        /// <summary>
        /// Whitelist: Comma Seperated list of Domains that are allowed to send emails to.
        /// If Whitelist is Empty, only then the Blacklist is processed.
        /// If Whitelist is Empty, All Domains are allowed to be sent emails, except those that are in the blacklist
        /// </summary>
        /// <seealso cref="BlockedDomains"/>
        public string AllowedDomains { get; set; }

        /// <summary>
        /// Blacklist: Comma Seperated list of Domains that are NOT allowed to send emails to.
        /// Processed only when Whitelist is Empty. 
        /// </summary>
        /// <seealso cref="AllowedDomains"/>
        public string BlockedDomains { get; set; }


        /// <summary>
        /// Force Adds these Comma Seperated Email Addresses to the TO address of the Email being sent. 
        /// </summary>
        public string AddTo { get; set; }


        /// <summary>
        /// Force Adds these Comma Seperated Email Addresses to the CC address of the Email being sent.
        /// </summary>
        public string AddCC { get; set; }

        /// <summary>
        /// Force Adds these Comma Seperated Email Addresses to the BCC address of the Email being sent.
        /// </summary>
        public string AddBCC { get; set; }
    }
}

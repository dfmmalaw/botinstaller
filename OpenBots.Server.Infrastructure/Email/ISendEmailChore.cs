﻿using OpenBots.Server.Core;
using OpenBots.Server.Model.Core;
using System.Text;

namespace OpenBots.Server.Infrastructure.Email
{
    public interface ISendEmailChore : IChore
    {
        void SendEmail(EmailMessage message);

    }
}

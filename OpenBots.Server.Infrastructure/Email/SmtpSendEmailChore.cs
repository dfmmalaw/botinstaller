﻿using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace OpenBots.Server.Infrastructure.Email
{

    public class SmtpSendEmailChore : BaseSendEmailChore, ISendEmailChore
    {
        protected SmtpEmailSetting _smtpSetting;

        public SmtpSendEmailChore(SmtpEmailSetting smtpSetting, SendEmailSetting sendEmailSetting) : base(sendEmailSetting)
        {
            _smtpSetting = smtpSetting;
        }



        public override void SendEmail(EmailMessage message)
        {
            if (base.setting.DisableEmail)
                return;

            try
            {
                message = ApplySettings(message);

                System.Net.Mail.MailMessage mail = EmailMessage.FromStub(message);

                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(_smtpSetting.Username, _smtpSetting.Password);
                client.Port = _smtpSetting.Port; // You can use Port 25 if 587 is blocked (mine is!)
                client.Host = _smtpSetting.Host;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Send(mail);
            }
            catch(Exception ex)
            {
                throw new CannotSendEmailException("Cannot Send Email." + ex.Message, ex);
            }
        }
    }
}

﻿using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Linq;

namespace OpenBots.Server.Infrastructure.Email
{
    public abstract class BaseSendEmailChore : ISendEmailChore
    {
        protected SendEmailSetting setting;
        public BaseSendEmailChore(SendEmailSetting sendEmailSetting)
        {
            setting = sendEmailSetting;
        }

        public abstract void SendEmail(EmailMessage message);

        
        protected EmailMessage ApplySettings(EmailMessage message)
        {

            if (message.From.Count == 0)
            {
                EmailAddress from = new EmailAddress(setting.From, setting.FromEmail);
                message.From.Add(from);
            }

            message.Subject = string.Concat(setting.SubjectPrefix, message.Subject, setting.SubjectSuffix);

            if (!string.IsNullOrEmpty(setting.AddTo))
            {
                foreach (string toid in setting.AddTo.Split(','))
                {
                    EmailAddress to = new EmailAddress(toid, toid);
                    message.To.Add(to);
                }
            }

            if (!string.IsNullOrEmpty(setting.AddCC))
            {
                foreach (string ccid in setting.AddCC.Split(','))
                {
                    EmailAddress cc = new EmailAddress(ccid, ccid);
                    message.CC.Add(cc);
                }
            }

            if (!string.IsNullOrEmpty(setting.AddBCC))
            {
                foreach (string bccid in setting.AddBCC.Split(','))
                {
                    EmailAddress bcc = new EmailAddress(bccid, bccid);
                    message.Bcc.Add(bcc);
                }
            }

            if(message.IsBodyHtml)
            {
                message.Body = string.Concat(setting.BodyPrefix, message.Body, setting.BodySuffix);
            }
            else
            {
                message.PlainTextBody = string.Concat(setting.BodyPrefix, message.PlainTextBody, setting.BodySuffix);
            }

            List<EmailAddress> newToList = new List<EmailAddress>();

            if (string.IsNullOrEmpty(setting.AllowedDomains))
            {
                // use Black List
                // Remove Any Email in TO, CC, BCC that are in Black List
                IEnumerable<string> blacklist = (new List<string>(setting.BlockedDomains.Split(','))).Select( s => s.ToLowerInvariant().Trim());
                
                foreach(EmailAddress adr in message.To)
                {
                    MailAddress madr = new MailAddress(adr.Address);
                    if (!blacklist.Contains(madr.Host.ToLowerInvariant()))
                        newToList.Add(adr);
                }
            }
            else
            {
                // Use White List
                // Remove Any Email in TO, CC, BCC that are NOT IN White List3
                IEnumerable<string> whitelist = (new List<string>(setting.AllowedDomains.Split(','))).Select(s => s.ToLowerInvariant().Trim());

                foreach (EmailAddress adr in message.To)
                {
                    MailAddress madr = new MailAddress(adr.Address);
                    if (whitelist.Contains(madr.Host.ToLowerInvariant()))
                        newToList.Add(adr);
                }
            }

            message.To.Clear();
            message.To = newToList;


            return message;
        }
    }
}

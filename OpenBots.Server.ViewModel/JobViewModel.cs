﻿using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OpenBots.Server.Model
{
    public class JobViewModel : Job
    {
        public string AgentName { get; set; }
        public string ProcessName { get; set; }
    }
}

﻿using OpenBots.Server.Model;
using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.ViewModel
{
    /// <summary>
    /// ViewModel for Audit Log model
    /// </summary>
    public class AuditLogViewModel
    {
        /// <summary>
        /// Name of Service used
        /// </summary>
        public string ServiceName { get; set; }
        /// <summary>
        /// Name of Method used
        /// </summary>
        public string MethodName { get; set; }
        /// <summary>
        /// Who made the changes to the entity
        /// </summary>
        public string UpdatedBy { get; set; }
        /// <summary>
        ///When update was made
        /// </summary>
        public string UpdatedOn { get; set; }
    }
}

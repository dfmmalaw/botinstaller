﻿using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OpenBots.Server.ViewModel
{
    public class JobsLookupViewModel
    {
        public string AgentName { get; set; }
        public Guid AgentId { get; set; }
        public Guid ProcessId { get; set; }
        public string ProcessName { get; set; }
    }
}

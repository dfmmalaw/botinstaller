﻿using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OpenBots.Server.Model
{
    public class AgentModel : NamedEntity
    {
        public Guid? MachineCredentials { get; set; }
        [Required]
        public string MachineName { get; set; }
        [Required]
        public string MacAddresses { get; set; }
        [Required]
        public string IPAddresses { get; set; }
        [Required]
        public bool IsEnabled { get; set; }

        public DateTime? LastReportedOn { get; set; }

        public string? LastReportedStatus { get; set; }

        public string? LastReportedWork { get; set; }

        public string? LastReportedMessage { get; set; }

        public bool? IsHealthy { get; set; }
    }
}

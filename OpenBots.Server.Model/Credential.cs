﻿using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OpenBots.Server.Model
{
    public class Credential : NamedEntity
    {
        [Required]
        public string Provider { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        [Required]
        public string Domain { get; set; }
        [Required]
        public string UserName { get; set; }
        public string? PasswordSecret { get; set; }
        public string? PasswordHash { get; set; }
        public string? Certificate { get; set; }
    }
}


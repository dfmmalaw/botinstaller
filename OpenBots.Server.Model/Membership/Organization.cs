﻿using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
#nullable enable

namespace OpenBots.Server.Model.Membership
{
    
    public class Organization : Entity, ITenanted
    {
        public Organization()
        {
            Units = new List<OrganizationUnit>();
            AccessRequests = new List<AccessRequest>();
            Members = new List<OrganizationMember>();
            Settings = new List<OrganizationSetting>();
        }

        [MaxLength(100, ErrorMessage = "Name must be 100 characters or less.")]
        [Required]
        [MinLength(3, ErrorMessage = "Name must be at least 3 characters.")]
        [RegularExpression("^[A-Za-z0-9_. ]{3,100}$")] // Alphanumeric with Underscore and Dot only
        [Display(Name = "Name")]
        public string? Name { get; set; }

        //[MaxLength(500)]
        [StringLength(500,ErrorMessage ="Description must be 500 characters or less.")]
        [Display(Name = "Description")]
        //[MailMergeField(Name = "CompanyDescription", Condition = "< 10", Format= "[CompanyName] is accessed via {1} Intarnet.")]
        //[MailMergeField(Name = "CompanyDescription", Condition = "> 10", Format= "{0} is accessed via {1} .")]
        public string? Description { get; set; }
        

        [DefaultValue(false)]
        [Display(Name="IsPublic")]
        public bool? IsPublic { get; set; }

        [DefaultValue(false)]
        [Display(Name= "IsVisibleToEmailDomain")]
        public bool? IsVisibleToEmailDomain { get; set; }

        [MaxLength(250)]
        [Display(Name= "EMailDomain")]
        public string? EMailDomain { get; set; }

        [MaxLength(500)]
        [Display(Name = "PrivateKey")]
        public string? PrivateKey { get; set; }
                
        [MaxLength(500)]
        [Display(Name="Salt")]
        public string? Salt { get; set; }

        [Display(Name = "ProcessNumber")]
        public int? ProcessKeyNumber { get; set; }

        [Column(TypeName = "char(1)")]
        public string? ProcessKeyPrefix { get; set; }


        public List<OrganizationUnit> Units { get; set; }

        public List<AccessRequest> AccessRequests { get; set; }

        public List<OrganizationMember> Members { get; set; }

        public List<OrganizationSetting> Settings { get; set; }

        public int MyProperty { get; set; }

        [Display(Name = "AllowedStorageInBytes")]
        public double? AllowedStorageInBytes { get; set; }

        [Display(Name = "StorageUsedInBytes")]
        public double? StorageUsedInBytes { get; set; }

        [Display(Name = "IsDocumentStorageEnabled")]
        public bool? IsDocumentStorageEnabled { get; set; }

        [Display(Name = "IsDocumentStorageQuotaFull")]
        public bool? IsDocumentStorageQuotaFull { get; set; }

        [NotMapped]
        public Guid? OrganizationId { get; set; }
    }
}

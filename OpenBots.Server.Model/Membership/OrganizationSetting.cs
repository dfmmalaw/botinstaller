﻿using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
#nullable enable

namespace OpenBots.Server.Model.Membership
{
    public class OrganizationSetting : Entity, ITenanted
    {
        public OrganizationSetting()
        {
            BusinesProcessKeyPrefix = "P";
            BusinessProcessPrioritiesCSV = "High,Medium,Low";
            BusinessProcessStatusCSV = "Not Started, In Progress, Completed, Impediment, Abandoned";
        }

        [Display(Name= "OrganizationId")]
        public Guid? OrganizationId { get; set; }

        [Display(Name = "BusinesProcessKeyPrefix")]
        public string? BusinesProcessKeyPrefix { get; set; }

        [Display(Name = "BusinessProcessPrioritiesCSV")]
        public string? BusinessProcessPrioritiesCSV { get; set; }

        [Display(Name = "BusinessProcessStatusCSV")]
        public string? BusinessProcessStatusCSV { get; set; }

        [ForeignKey("OrganizationId")]
        public Organization? Organization { get; set; }

        [Display(Name = "SMTPConfiguration")]
        public string? SMTPConfiguration { get; set; }

        [Display(Name = "TimeZone")]
        public string? TimeZone { get; set; }

        [Display(Name = "StorageLocation")]
        public string? StorageLocation { get; set; }


    }
}

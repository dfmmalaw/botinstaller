﻿using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OpenBots.Server.Model
{
    public class Job: Entity
    {
        [Required]
        public Guid AgentId { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public DateTime? EnqueueTime { get; set; }
        public DateTime? DequeueTime { get; set; }
        [Required]
        public Guid ProcessId { get; set; }
        public string? JobStatus { get; set;}
        public string? Message { get; set; }
        public bool? IsSuccessful { get; set; }
    }
}

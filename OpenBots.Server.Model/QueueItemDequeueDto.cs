﻿using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OpenBots.Server.Model
{
    public class QueueItemDequeueDto
    {
        public Guid? OrganizationId { get; set; }

        public Guid? ProcessID { get; set; }

        public virtual string Name { get; set; }

        public virtual string Subtopic { get; set; }

        public virtual string Event { get; set; }

        public virtual string Source { get; set; }

        public virtual int? Priority { get; set; }

        public virtual string QueueItemType { get; set; }

        public virtual string EntityType { get; set; }

        public virtual string EntityStatus { get; set; }

        public virtual string DataJSON { get; set; }

        public virtual string DataText { get; set; }

        public virtual DateTime? DontDequeueUntil { get; set; }

        public virtual DateTime? DontDequeueAfter { get; set; }

        public virtual bool IsDequeued { get; set; }

        public virtual bool IsLocked { get; set; }

        public virtual DateTime? LockedOn { get; set; }

        public virtual DateTime? LockedUntil { get; set; }

        public virtual string LockedBy { get; set; }

        public virtual Guid LockTransactionKey { get; set; }

        public virtual int? RetryCount { get; set; }

        public virtual string LastOccuredError { get; set; }

        public virtual byte[]? Timestamp { get; set; }

        public virtual bool IsError { get; set; }

        public virtual bool IsItemAvailable { get; set; }

        public virtual int? RetryAttempt { get; set; }

        public virtual Guid TransactionKey { get; set; }

        public virtual Guid? AppQueueItemId { get; set; }

        public virtual QueueItem Item { get; set; }

        public virtual Guid? QueueId { get; set; }

    }

}

﻿using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OpenBots.Server.Model
{
	public class QueueItem : NamedEntity
	{
		public Guid? OrganizationId { get; set; }

		public Guid? ProcessID { get; set; }

		[Required]
		[StringLength(QueueItemConstants.MaxTopicLength, MinimumLength = QueueItemConstants.MinTopicLength)]
		public virtual string Name { get; set; }

		[StringLength(QueueItemConstants.MaxSubtopicLength, MinimumLength = QueueItemConstants.MinSubtopicLength)]
		public virtual string Subtopic { get; set; }

		[StringLength(QueueItemConstants.MaxEventLength, MinimumLength = QueueItemConstants.MinEventLength)]
		public virtual string Event { get; set; }

		[StringLength(QueueItemConstants.MaxSourceLength, MinimumLength = QueueItemConstants.MinSourceLength)]
		public virtual string Source { get; set; }

		[Range(QueueItemConstants.MinPriorityValue, QueueItemConstants.MaxPriorityValue)]
		[DefaultValue(1)]
		public virtual int? Priority { get; set; }

		[StringLength(QueueItemConstants.MaxQueueItemTypeLength, MinimumLength = QueueItemConstants.MinQueueItemTypeLength)]
		public virtual string QueueItemType { get; set; }

		[StringLength(QueueItemConstants.MaxEntityTypeLength, MinimumLength = QueueItemConstants.MinEntityTypeLength)]
		public virtual string EntityType { get; set; }


		[StringLength(QueueItemConstants.MaxEntityStatusLength, MinimumLength = QueueItemConstants.MinEntityStatusLength)]
		public virtual string EntityStatus { get; set; }

		public virtual string DataJSON { get; set; }

		public virtual string DataText { get; set; }

		public virtual DateTime? DontDequeueUntil { get; set; }

		public virtual DateTime? DontDequeueAfter { get; set; }

		[DefaultValue(false)]
		public virtual bool IsDequeued { get; set; }

		[DefaultValue(false)]
		public virtual bool IsLocked { get; set; }

		public virtual DateTime? LockedOn { get; set; }

		public virtual DateTime? LockedUntil { get; set; }

		public virtual Guid? LockedBy { get; set; }

		public virtual Guid LockTransactionKey { get; set; }

		public virtual int? RetryCount { get; set; }

		public virtual string LastOccuredError { get; set; }

		[Timestamp]
		public virtual byte[]? Timestamp { get; set; }

		[DefaultValue(false)]
		public virtual bool IsError { get; set; }

		public Guid? QueueID { get; set; }

		public virtual string? Data { get; set; }

		public virtual string? RawData { get; set; }

		public virtual string? Status { get; set; }

		public virtual string? StatusMessage { get; set; }

		public virtual string? ExceptionMessage { get; set; }

	}

	public class QueueItemConstants
	{

		public const int MinTopicLength = 1;
		public const int MaxTopicLength = 100;

		public const int MinSubtopicLength = 0;
		public const int MaxSubtopicLength = 100;

		public const int MinEventLength = 0;
		public const int MaxEventLength = 100;

		public const int MinSourceLength = 0;
		public const int MaxSourceLength = 100;

		public const long MinPriorityValue = 0;
		public const long MaxPriorityValue = 99;

		public const int MinQueueItemTypeLength = 0;
		public const int MaxQueueItemTypeLength = 20;

		public const int MinEntityTypeLength = 0;
		public const int MaxEntityTypeLength = 255;

		public const int MinEntityStatusLength = 0;
		public const int MaxEntityStatusLength = 255;





	}
}

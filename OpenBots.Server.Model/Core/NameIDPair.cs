﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.Model.Core
{
    public class NameIDPair : INameIDPair
    {
        public NameIDPair()
        {

        }

        public NameIDPair(INameIDPair nameIDPair)
        {
            Id = nameIDPair.Id;
            Name = nameIDPair.Name;
        }

        public Guid? Id { get ; set ; }
        public string Name { get ; set; }
    }

    public class NameIDPairs<T> : List<T>
        where T : INameIDPair, new()
    {
        public void Add(IEnumerable<INameIDPair> nameIDPairs)
        {
            foreach (INameIDPair nameIDPair in nameIDPairs)
            {
                T n = new T();
                n.Id = nameIDPair.Id;
                n.Name = nameIDPair.Name;
                this.Add(n);
            }
        }
    }
}

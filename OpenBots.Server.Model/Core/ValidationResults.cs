﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
#nullable enable

namespace OpenBots.Server.Model.Core
{
    public class ValidationResults : List<ValidationResult>
    {
        public ValidationResults()
        {
        }

        public ValidationResults(IEnumerable<ValidationResult> collection, bool isValid) : base(collection)
        {
            IsValid = isValid;
        }

        [Display(Name = "IsValid")]
        public bool IsValid { get; set; }
    }
}

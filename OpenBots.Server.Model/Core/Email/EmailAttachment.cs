﻿using System;

namespace OpenBots.Server.Model.Core
{
    public class EmailAttachment
    {
        public EmailAttachment()
        {
        }

        public EmailAttachment(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public EmailAttachment(string name, string contentType)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            ContentType = contentType ?? throw new ArgumentNullException(nameof(contentType));
        }

        public EmailAttachment(string iD, string name, string contentType)
        {
            ID = iD ?? throw new ArgumentNullException(nameof(iD));
            Name = name ?? throw new ArgumentNullException(nameof(name));
            ContentType = contentType ?? throw new ArgumentNullException(nameof(contentType));
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public double SizeInBytes { get; set; }
        public string FileName { get; set; }

        public string IsContentStored { get; set; }

        /// <summary>
        /// Address where Content of the Attachment is Stored
        /// </summary>
        /// <seealso cref="StorageAddress"/>
        public string ContentStorageAddress { get; set; }
        public byte[] Content { get; set; }
    }
}

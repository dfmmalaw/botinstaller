﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

namespace OpenBots.Server.Model.Core
{

    /// <summary>
    /// MailMessageStub is a simple serializable Stub for a Email message
    /// This would serve as a translation between various formats of mail messages 
    /// eg.: Aspose, System.Net, Amazon SES, Outlook etc.
    /// It can be serialized to a JSON object and can be stored in Entity or Blob Storage
    /// </summary>
    public class EmailMessage
    {
        public EmailMessage()
        {
            To = new List<EmailAddress>();
            CC = new List<EmailAddress>();
            Bcc = new List<EmailAddress>();
            From = new List<EmailAddress>();
            ReplyToList = new List<EmailAddress>();
            Headers = new List<EmailHeader>();
            Attachments = new List<EmailAttachment>();
        }

        public EmailMessage(string from, string to, string subject, string body) : this()
        {
            From.Add(new EmailAddress(from));
            To.Add(new EmailAddress(to));
            Subject = subject;
            PlainTextBody = body;
        }


        public static System.Net.Mail.MailMessage FromStub(EmailMessage msg)
        {
            System.Net.Mail.MailMessage outMsg = new System.Net.Mail.MailMessage();

            var from = msg.From.FirstOrDefault();

            outMsg.From = from.ToMailAddress();
            EmailAddress.IterateBack(msg.To).ForEach(addr => outMsg.To.Add(addr));
            EmailAddress.IterateBack(msg.CC).ForEach(addr => outMsg.CC.Add(addr));
            EmailAddress.IterateBack(msg.Bcc).ForEach(addr => outMsg.Bcc.Add(addr));
            outMsg.Subject = msg.Subject;
            outMsg.IsBodyHtml = msg.IsBodyHtml;
            outMsg.Body = msg.Body;


            return outMsg;
        }


        /// <summary>
        /// Simply maps a System.Net.Mail.Mailmessage to a Stub
        /// </summary>
        /// <param name="inMessage">Input Message</param>
        /// <returns>Stubbed out Message</returns>
        public static EmailMessage CreateStub(System.Net.Mail.MailMessage inMessage)
        {
            EmailMessage messageStub = new EmailMessage();

            messageStub.From.Add(new EmailAddress(inMessage.From));
            messageStub.To = EmailAddress.Iterate(inMessage.To);
            messageStub.CC = EmailAddress.Iterate(inMessage.CC);
            messageStub.Bcc = EmailAddress.Iterate(inMessage.Bcc);
            messageStub.ReplyToList = EmailAddress.Iterate(inMessage.ReplyToList);
            messageStub.Subject = ConvertStringToUnicode(inMessage.Subject, inMessage.SubjectEncoding);
            messageStub.Body = ConvertStringToUnicode(inMessage.Body, inMessage.BodyEncoding);
            messageStub.IsBodyHtml = inMessage.IsBodyHtml;
            messageStub.Headers = ConvertHeaders(inMessage.Headers, inMessage.HeadersEncoding);


            //Message-Id:<481aef8306d24519872c84bc6c1dffa4 @BYAPR10MB2485.namprd10.prod.outlook.com>
            var messageIdHeader = messageStub.Headers.Find(h => h.Name.Equals("Message-Id", StringComparison.InvariantCultureIgnoreCase));
            if (messageIdHeader != null)
                messageStub.MessageID = messageIdHeader.Value;

            //In-Reply-To:<BN6PR10MB196935EC633ABE79AA2C3258E1ED0@BN6PR10MB1969.namprd10.prod.outlook.com>
            var messageReplyIdHeader = messageStub.Headers.Find(h => h.Name.Equals("In-Reply-To", StringComparison.InvariantCultureIgnoreCase));
            if (messageReplyIdHeader != null)
                messageStub.InReplyToMessageID = messageReplyIdHeader.Value;


            //Thread-Topic: DONE: InsureTech Conference - Packages to Track
            var messageTopicHeader = messageStub.Headers.Find(h => h.Name.Equals("Thread-Topic", StringComparison.InvariantCultureIgnoreCase));
            if (messageTopicHeader != null)
                messageStub.MessageTopic = messageTopicHeader.Value;


            // Iterate thru attachments and add them at a time
            foreach (var attach in inMessage.Attachments)
            {
                EmailAttachment attachStub = new EmailAttachment();
                attachStub.ContentType = attach.ContentType.MediaType;
                attachStub.Name = ConvertStringToUnicode(attach.Name, attach.NameEncoding);
                attachStub.FileName = attach.ContentDisposition.FileName;
                attachStub.ID = attach.ContentId;
                attachStub.SizeInBytes = attach.ContentDisposition.Size;
                if (attach.ContentStream != null)
                {
                    MemoryStream memory = new MemoryStream();
                    attach.ContentStream.CopyTo(memory);
                    attachStub.Content = memory.ToArray();
                }

                messageStub.Attachments.Add(attachStub);
            }

            return messageStub;

        }

        public static List<EmailHeader> ConvertHeaders(System.Collections.Specialized.NameValueCollection headers, Encoding inEncoding)
        {
            List<EmailHeader> Newheaders = new List<EmailHeader>();
            foreach (string key in headers.AllKeys)
            {
                EmailHeader newHeader = new EmailHeader();
                newHeader.Name = key;
                newHeader.Value = ConvertStringToUnicode(headers[key], inEncoding);
                Newheaders.Add(newHeader);
            }

            return Newheaders;
        }


        public static string ConvertStringToUnicode(string inText, Encoding inEncoding)
        {
            Encoding unicode = Encoding.Unicode;
            Encoding coding = inEncoding;
            if (coding == null)
                coding = Encoding.Default;
            byte[] inBytes = coding.GetBytes(inText);
            byte[] outBytes = Encoding.Convert(coding, unicode, inBytes);
            return unicode.GetString(outBytes);
        }

        public string MessageID { get; set; }
        public string InReplyToMessageID { get; set; }
        public string MessageTopic { get; set; }

        public DateTime RecievedOnUTC { get; set; }

        public List<EmailAddress> From { get; set; }
        public EmailAddress Sender { get; set; }
        public List<EmailAddress> To { get; set; }
        public List<EmailAddress> CC { get; set; }
        public List<EmailAddress> Bcc { get; set; }
        public List<EmailAddress> ReplyToList { get; set; }

        public string Source { get; set; }

        public bool IsPossibleSpam { get; set; }

        public bool IsPossibleVirus { get; set; }

        public int Priority { get; set; }

        public string Subject { get; set; }

        public string PlainTextBody { get; set; }

        public string Body { get; set; }

        public bool IsBodyHtml { get; set; }

        public string IsBodyContentStored { get; set; }
        public string BodyContentStorageAddress { get; set; }


        public List<EmailHeader> Headers { get; set; }
        public int DeliveryNotificationOptions { get; set; }

        public List<EmailAttachment> Attachments { get; set; }
    }
}

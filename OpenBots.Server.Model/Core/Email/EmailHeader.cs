﻿using System;
using System.Collections.Generic;

namespace OpenBots.Server.Model.Core
{
    public class EmailHeader
    {
        public EmailHeader()
        {
        }

        public EmailHeader(string name, string value)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Value = value ?? throw new ArgumentNullException(nameof(value));
        }

        public List<EmailHeader> Create(System.Collections.Specialized.NameValueCollection coll)
        {
            List<EmailHeader> headers = new List<EmailHeader>();
            foreach (string key in coll.AllKeys)
            {
                headers.Add(new EmailHeader(key, coll[key]));
            }

            return headers;
        }

        public string Name { get; set; }
        public string Value { get; set; }
    }
}

﻿using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.Model
{
    public class Queue : NamedEntity
    {
        public string Description { get; set; }
    }
}

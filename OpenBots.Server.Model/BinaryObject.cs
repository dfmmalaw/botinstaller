﻿using OpenBots.Server.Model.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.Model
{
    public class BinaryObject : NamedEntity
    {

        public virtual string Name { get; set; }
        public virtual Guid? OrganizationId { get; set; }

        public virtual string ContentType { get; set; }

        public virtual Guid? CorelationEntityId { get; set; }

        public virtual string CorelationEntity { get; set; }

        public virtual string StoragePath { get; set; }

        public virtual string StorageProvider { get; set; }

        public virtual long? SizeInBytes { get; set; }

        public virtual string HashCode { get; set; }


    }
}

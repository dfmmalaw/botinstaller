﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
#nullable enable

namespace OpenBots.Server.Model.Identity
{
    public class UserSecurityContext
    {
        public UserSecurityContext()
        {
            OrganizationUnits = new Dictionary<Guid, bool>();
            UserInRoles = new List<string>();

            //// Default PersonID for Test 
            //PersonId = new Guid("786F998C-7FFD-43A0-B74A-ABF30619B188");
            //OrganizationId = new Guid("75EB8EE2-209E-433E-960C-F8503578C706");
    }

        [Display(Name = "PersonId")]
        public Guid PersonId { get; set; }

        [Display(Name = "OrganizationId")]
        public Guid[] OrganizationId { get; set; }

        [Display(Name = "IsOrganizationAdmin")]
        public bool IsOrganizationAdmin { get; set; }

        [Display(Name = "OrganizationUnits")]
        public Dictionary<Guid, bool> OrganizationUnits { get; set; }

        [Display(Name = "UserInRoles")]
        public List<string> UserInRoles { get; set; }
    }
}

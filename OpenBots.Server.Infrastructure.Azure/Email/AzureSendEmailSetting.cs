﻿using Microsoft.Extensions.Configuration;

namespace OpenBots.Server.Infrastructure.Azure.Email
{
    public class AzureSendEmailSetting
    {
        public AzureSendEmailSetting(IConfiguration config)
        {
            SendGridKey = config["Email.SendGrid:Key"];
        }
        public string SendGridKey { get; set; }

    }
}

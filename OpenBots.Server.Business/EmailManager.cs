﻿using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Infrastructure.Email;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OpenBots.Server.Business
{
    public class EmailManager : BaseManager, IEmailManager
    {
        ISendEmailChore emailChore;
        protected IPersonRepository _personRepo;
        protected IPersonEmailRepository _personEmailRepository;

        public EmailManager(ISendEmailChore sendEmailChore, IPersonRepository personRepo, IPersonEmailRepository personEmailRepository)
        {
            emailChore = sendEmailChore;
            _personRepo = personRepo;
            _personEmailRepository = personEmailRepository;
        }

        public override void SetContext(UserSecurityContext userSecurityContext)
        {

            this._personRepo.SetContext(userSecurityContext); ;
            this._personEmailRepository.SetContext(userSecurityContext); ;
            base.SetContext(userSecurityContext);
        }


        //public AuthMessageSenderOptions Options { get; } //set only via Secret Manager
        public Task SendEmailAsync(string email, string subject, string message)
        {
            EmailMessage emailMessage = new EmailMessage();
            emailMessage.To.Add(new Model.Core.EmailAddress(email));
            emailMessage.Subject = subject;
            emailMessage.PlainTextBody = message;
            emailMessage.IsBodyHtml = true;
            emailMessage.Body = message;

            emailChore.SendEmail(emailMessage);

            return Task.CompletedTask;
        }
        public Task SendEmailAsync(Guid toPersonId, string subject, string message)
        {

            return Task.CompletedTask;
        }


        public void SendSignupConfirmation(Guid? toPersonId, string confirmationLink, string password, string language = "en")
        {

        }

        public void ResendConfirmation(Guid? toPersonId, string confirmationLink, string password, string language = "en")
        {

        }
        public void SendAdditionalEmailVerification(Guid? toPersonId, string resetLink, string language = "en")
        {

        }

        public void ForgotPasswordConfirmation(Guid? toPersonId, string resetLink, string language = "en")
        {

        }

        public void InviteNewUser()
        {

        }

        public void UserAddedToOrganization()
        {

        }
        public void UserApprovedInOrganization()
        {

        }
        public void UserRejectedInOrganization()
        {

        }

        public void UserRemovedFromOrganization()
        {

        }



    }
}

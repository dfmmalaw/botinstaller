﻿using OpenBots.Server.DataAccess.Repositories;

namespace OpenBots.Server.Business
{
    class BinaryObjectManager : BaseManager
    {
        private readonly IBinaryObjectRepository repo;

        public BinaryObjectManager(IBinaryObjectRepository repo)
        {
            this.repo = repo;
        }
    }
}

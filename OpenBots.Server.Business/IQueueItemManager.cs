﻿using OpenBots.Server.Model;
using OpenBots.Server.Model.Membership;
using System;
using System.Threading.Tasks;

namespace OpenBots.Server.Business
{
    public interface IQueueItemManager : IManager
    {
        public Task<QueueItem> GetByID(Guid id);

        public Task<Guid> Enqueue(QueueItem input);
        
        public Task<QueueItemDequeueDto> Dequeue(string topic, string subtopic = "", string entityName = "", string status = "", bool includeError = false, string lockedby = "");
        
        public Task Commit(Guid queueItemId, Guid transactionKey);

        public Task Rollback(Guid queueItemId, Guid transactionKey, string error);

        public Task Extend(Guid queueItemId, Guid transactionKey);

    }
}
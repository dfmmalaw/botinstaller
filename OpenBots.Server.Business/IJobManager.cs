﻿using OpenBots.Server.Model;
using OpenBots.Server.Model.Core;
using OpenBots.Server.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.Business
{
    public interface IJobManager : IManager
    {
        JobViewModel GetJobView(Job job);
        PaginatedList<JobViewModel> GetAllJobsView(PaginatedList<Job> jobList);
        List<JobsLookupViewModel> GetJobAgentsLookup();
    }
}

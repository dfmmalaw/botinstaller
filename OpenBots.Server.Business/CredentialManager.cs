﻿using Microsoft.AspNetCore.Mvc;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model;
using OpenBots.Server.Security;
using OpenBots.Server.ViewModel;
using System;
using System.Collections.Generic;
using System.Reflection.PortableExecutable;
using System.Security.Cryptography;
using System.Text;

namespace OpenBots.Server.Business
{
    public class CredentialManager : BaseManager, ICredentialManager
    {
        private readonly ICredentialRepository repo;

        public CredentialManager(ICredentialRepository repo)
        {
            this.repo = repo;
        }

        public bool ValidateRetrievalDate(Credential credential)
        {
            if (DateTime.Now > credential.StartDate && DateTime.Now < credential.EndDate)
            {
                return true;
            }
            return false;
        }

        public CredentialViewModel GetCredentialView(Credential credential)
        {
            CredentialViewModel credentialViewModel = new CredentialViewModel();

            credentialViewModel.Name = credential.Name;
            credentialViewModel.Provider = credential.Provider;
            credentialViewModel.StartDate = credential.StartDate;
            credentialViewModel.EndDate = credential.EndDate;
            credentialViewModel.Domain = credential.Domain;
            credentialViewModel.UserName = credential.UserName;
            credentialViewModel.Certificate = credential.Certificate;
            credentialViewModel.Provider = credential.Provider;
            credentialViewModel.Id = credential.Id;
            credentialViewModel.IsDeleted = credential.IsDeleted;
            credentialViewModel.CreatedBy = credential.CreatedBy;
            credentialViewModel.CreatedOn = credential.CreatedOn;
            credentialViewModel.DeletedBy = credential.DeletedBy;
            credentialViewModel.DeleteOn = credential.DeleteOn;
            credentialViewModel.Timestamp = credential.Timestamp;
            credentialViewModel.UpdatedOn = credential.UpdatedOn;
            credentialViewModel.UpdatedBy = credential.UpdatedBy;

            return credentialViewModel;
        }
    }
}

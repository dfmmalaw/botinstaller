﻿using Microsoft.AspNetCore.Mvc;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model;
using OpenBots.Server.Model.Core;
using OpenBots.Server.ViewModel;
using System;
using System.Collections.Generic;
using System.Reflection.PortableExecutable;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace OpenBots.Server.Business
{
    public class JobManager : BaseManager, IJobManager
    {
        private readonly IJobRepository repo;
        private readonly IAgentRepository agentRepo;
        /*private readonly IProcessRepository processRepo;*/

        public JobManager(IJobRepository repo, IAgentRepository agentRepo)//IProcessRepository processRepo
        {
            this.repo = repo;
            this.agentRepo = agentRepo;
            /*this.processRepo = processRepo;*/
        }

        public JobViewModel GetJobView(Job job)
        {
            JobViewModel jobViewModel = new JobViewModel();

            jobViewModel.AgentId = job.AgentId;
            jobViewModel.StartTime = job.StartTime;
            jobViewModel.EndTime = job.EndTime;
            jobViewModel.EnqueueTime = job.EnqueueTime;
            jobViewModel.DequeueTime = job.DequeueTime;
            jobViewModel.ProcessId = job.ProcessId;
            jobViewModel.JobStatus = job.JobStatus;
            jobViewModel.Message = job.Message;
            jobViewModel.IsSuccessful = job.IsSuccessful;
            jobViewModel.Id = job.Id;
            jobViewModel.IsDeleted = job.IsDeleted;
            jobViewModel.CreatedBy = job.CreatedBy;
            jobViewModel.CreatedOn = job.CreatedOn;
            jobViewModel.DeletedBy = job.DeletedBy;
            jobViewModel.DeleteOn = job.DeleteOn;
            jobViewModel.Timestamp = job.Timestamp;
            jobViewModel.UpdatedOn = job.UpdatedOn;
            jobViewModel.UpdatedBy = job.UpdatedBy;

            jobViewModel.AgentName = agentRepo.GetOne(job.AgentId).Name;
            /*jobViewModel.ProcessName = processRepo.GetOne(job.ProcessId).Name;*/

            return jobViewModel;
        }

        public PaginatedList<JobViewModel> GetAllJobsView(PaginatedList<Job> jobList)
        {
            PaginatedList<JobViewModel> jobViewModelList = new PaginatedList<JobViewModel>();

            jobViewModelList.PageNumber = jobList.PageNumber;
            jobViewModelList.PageSize = jobList.PageSize;
            jobViewModelList.TotalCount = jobList.TotalCount;
            jobViewModelList.Completed = jobList.Completed;
            jobViewModelList.Started = jobList.Started;
            jobViewModelList.Impediments = jobList.Impediments;
            jobViewModelList.ParentId = jobList.ParentId;

            foreach (Job job in jobList.Items)
            {
                JobViewModel jobViewModel = new JobViewModel();

                jobViewModel.AgentId = job.AgentId;
                jobViewModel.StartTime = job.StartTime;
                jobViewModel.EndTime = job.EndTime;
                jobViewModel.EnqueueTime = job.EnqueueTime;
                jobViewModel.DequeueTime = job.DequeueTime;
                jobViewModel.ProcessId = job.ProcessId;
                jobViewModel.JobStatus = job.JobStatus;
                jobViewModel.Message = job.Message;
                jobViewModel.IsSuccessful = job.IsSuccessful;
                jobViewModel.Id = job.Id;
                jobViewModel.IsDeleted = job.IsDeleted;
                jobViewModel.CreatedBy = job.CreatedBy;
                jobViewModel.CreatedOn = job.CreatedOn;
                jobViewModel.DeletedBy = job.DeletedBy;
                jobViewModel.DeleteOn = job.DeleteOn;
                jobViewModel.Timestamp = job.Timestamp;
                jobViewModel.UpdatedOn = job.UpdatedOn;
                jobViewModel.UpdatedBy = job.UpdatedBy;

                jobViewModel.AgentName = agentRepo.GetOne(job.AgentId).Name;
                /*jobViewModel.ProcessName = processRepo.GetOne(job.ProcessId).Name;*/

                jobViewModelList.Add(jobViewModel);
            }

            return jobViewModelList;
        }

        public List<JobsLookupViewModel> GetJobAgentsLookup()
        {
            List<JobsLookupViewModel> jobsLookup = new List<JobsLookupViewModel>();
            var response = repo.Find(null, x => x.IsDeleted == false);
            
            if (response != null)
                foreach (Job job in response.Items)
                {
                    JobsLookupViewModel lookup = new JobsLookupViewModel();

                    lookup.AgentId = job.AgentId;                    
                    lookup.ProcessId = job.ProcessId;
                    lookup.AgentName = agentRepo.GetOne(job.AgentId).Name;
                    lookup.ProcessName = "ProcessName";
                    /*jobViewModel.ProcessName = processRepo.GetOne(job.ProcessId).Name;*/

                    jobsLookup.Add(lookup);
                }

            return jobsLookup;
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model;
using OpenBots.Server.ViewModel;
using System;
using System.Collections.Generic;
using System.Reflection.PortableExecutable;
using System.Text;

namespace OpenBots.Server.Business
{
    public class AgentManager : BaseManager, IAgentManager
    {
        private readonly IAgentRepository repo;

        public AgentManager(IAgentRepository repo)
        {
            this.repo = repo;
        }

        public bool ValidateAgent(string id)
        {
            Guid agentId = new Guid(id);
            var agent = repo.GetOne(agentId);

            if (agent != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public AgentModel GetAgentId(string machineName, string macAddress, string ipAddress)
        {
            return repo.GetId(machineName, macAddress, ipAddress);
        }
    }
}

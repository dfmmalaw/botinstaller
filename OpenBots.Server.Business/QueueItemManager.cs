﻿using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenBots.Server.Business
{
    public class QueueItemManager : BaseManager, IQueueItemManager
    {
        private readonly IQueueItemRepository repo;

        public QueueItemManager(IQueueItemRepository repo)
        {
            this.repo = repo;
        }

        public async Task<QueueItem> GetByID(Guid id)
        {
            return repo.GetOne(id);
        }

        public async Task<Guid> Enqueue(QueueItem input)
        {
            var response = repo.Add(input);
            Guid responseId = response.Id.Value;

            return responseId;
        }

        public async Task<QueueItemDequeueDto> Dequeue(string topic, string subtopic = "", string entityName = "", string status = "", bool includeError = false, string lockedby = "")
        {
            var item = repo.Find(null).Items               
               .WhereIf(!string.IsNullOrEmpty(topic), q => q.Name.Equals(topic))
               .WhereIf(!string.IsNullOrEmpty(subtopic), q => q.Subtopic.Equals(subtopic))
               .WhereIf(string.IsNullOrEmpty(subtopic), q => q.Subtopic == null)
               .WhereIf(!string.IsNullOrEmpty(entityName), q => q.EntityType.Equals(entityName))
               .WhereIf(!string.IsNullOrEmpty(status), q => q.EntityStatus.Equals(status))
               .WhereIf(!includeError, q => q.RetryCount == 0)
               .WhereIf(includeError, q => q.IsError == true)
               .Where(q => !q.IsLocked || q.LockedUntil < DateTime.UtcNow)
               .Where(q => !q.IsDequeued)
               .Where(q => q.IsDeleted == false)
               .Where(q => q.DontDequeueAfter == null || q.DontDequeueAfter <= DateTime.UtcNow)
               .Where(q => q.DontDequeueUntil == null || q.DontDequeueUntil >= DateTime.UtcNow)
               .OrderByDescending(q => q.Priority)
               .FirstOrDefault();

            QueueItemDequeueDto result = new QueueItemDequeueDto();
            if (item == null)
            {
                result.IsItemAvailable = false;
                return result;
            }

            item.IsLocked = true;
            item.LockedOn = DateTime.UtcNow;
            item.LockedUntil = DateTime.UtcNow.AddHours(1);

            Guid lockedById = Guid.Empty;
            Guid.TryParse(lockedby, out lockedById);
            item.LockedBy = lockedById;
            
            item.LockTransactionKey = Guid.NewGuid();


            item = repo.Update(item);
            var appQueueItem = (QueueItem)item;

            result.IsItemAvailable = true;
            if (item.RetryCount.HasValue)
                result.RetryAttempt = item.RetryCount.Value + 1;
            else
                result.RetryAttempt = 0;

            result.TransactionKey = item.LockTransactionKey;
            result.Item = appQueueItem;
            result.AppQueueItemId = item.Id;

            return result;
        }


        public async Task Commit(Guid queueItemId, Guid transactionKey)
        {

            var item = repo.GetOne(queueItemId);
            if (item?.IsLocked == true && item?.LockTransactionKey == transactionKey && item?.LockedUntil >= DateTime.UtcNow)
            {
                item.IsDequeued = true;
                item.Subtopic = "completed";
                repo.Update(item);
                repo.SoftDelete(queueItemId);
            }
            else
                throw new Exception("Transaction Key Mismatched or Expired. Cannot Commit.");
        }


        public async Task Rollback(Guid queueItemId, Guid transactionKey, string error)
        {
            var item = repo.GetOne(queueItemId);
            if (item.IsLocked && item.LockTransactionKey == transactionKey && item.LockedUntil >= DateTime.UtcNow)
            {
                item.IsLocked = false;
                item.LockTransactionKey = Guid.Empty;
                item.LockedUntil = null;
                item.RetryCount = item.RetryCount + 1;
                item.LastOccuredError = error;
                item.IsDequeued = false;
                item.IsError = true;
                if (item.RetryCount >= 3)
                    item.Subtopic = "error";
                repo.Update(item);
            }
            else
                throw new Exception("Transaction Key Mismatched or Expired. Cannot Rollback.");
        }

        public async Task Extend(Guid queueItemId, Guid transactionKey)
        {

            var item = repo.GetOne(queueItemId);
            if (item?.IsLocked == true && item?.LockTransactionKey == transactionKey && item?.LockedUntil >= DateTime.UtcNow)
            {
                item.LockedUntil = DateTime.UtcNow.AddHours(1);
                repo.Update(item);
            }
            else
                throw new Exception("Transaction Key Mismatched or Expired. Cannot Extend.");
        }

        
    }

    public static class WhereClause
    {
        public static IEnumerable<TSource> WhereIf<TSource>(this IEnumerable<TSource> source, bool condition, Func<TSource, bool> predicate)
        {
            if (condition)
                return source.Where(predicate);
            else
                return source;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OpenBots.Server.Business
{
    public interface IEmailManager
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using OpenBots.Server.Model;
using OpenBots.Server.Model.Core;
using OpenBots.Server.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.Business
{
    public interface IAgentManager : IManager
    {
        bool ValidateAgent(string id);
        AgentModel GetAgentId(string machineName, string macAddress, string ipAddress);
            
    }
}

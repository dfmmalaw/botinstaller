﻿using OpenBots.Server.Model.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.Business
{
    public class BaseManager : IManager
    {
        protected UserSecurityContext SecurityContext;

        public virtual void SetContext(UserSecurityContext userSecurityContext)
        {
            SecurityContext = userSecurityContext;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.Security
{
    public class Constants
    {
        //Connection string
        public const string ConnectionStringDefault = "DefaultConnection";
        public const string SendGridKey = "SendGrid";
        public const string PRODUCT = "OpenBots Server";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.Security.ViewModel
{
    public class RefreshModel
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}

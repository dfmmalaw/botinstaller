using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Deployment.WindowsInstaller;
using System.Diagnostics;


namespace InstallerCustomAction
{
    public class CustomActions
    {
        [CustomAction]
        public static ActionResult ConfigureDatabaseValues(Session session)
        {
            try
            {
                Debugger.Launch();

                session.Log("Begin Configuring database values in appsettings.json");

                string connectionString = session["CONNECTIONSTRING"];
                string databaseEngine = session["CHOICE_WIN_SQL"];


                //replace the values in appsettings.json for DB Engine and connection string based on user input
                string dbEngineKey = "DbOption:UseSqlServer";
                bool dbEngineValue = databaseEngine == "1" ? false : true;
                AddOrUpdateAppSetting(dbEngineKey, dbEngineValue);

                string connectionStringKey = "ConnectionStrings:Sql";
                string connectionStringValue = connectionString;
                AddOrUpdateAppSetting(connectionStringKey, connectionStringValue);


                session.Log("End appsettings.json configuration");

                return ActionResult.Success;
            }
            catch (Exception)
            {
                return ActionResult.Failure;
            }            
        }

        private static void AddOrUpdateAppSetting<T>(string key, T value)
        {
            try
            {
                //var filePath = Path.Combine(AppContext.BaseDirectory, "appSettings.json");
                //var filePath = Environment.GetEnvironmentVariable("TEMP") + Path.DirectorySeparatorChar + "appsettings.json";
                //var filePath = @"C:\Program Files(x86)\OpenBotServer\appsettings.json";
                var filePaths = Directory.GetFiles(@"c:\Program Files (x86)\OpenBotServer\", "*.json");
                var filePath = filePaths[1];
                string json = File.ReadAllText(filePath);
                dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);

                var sectionPath = key.Split(':')[0];
                if (!string.IsNullOrEmpty(sectionPath))
                {
                    var keyPath = key.Split(':')[1];
                    jsonObj[sectionPath][keyPath] = value;
                }
                else
                {
                    jsonObj[sectionPath] = value; // if no sectionpath just set the value
                }
                string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj, Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(filePath, output);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

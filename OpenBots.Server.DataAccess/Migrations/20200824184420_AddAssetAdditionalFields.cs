﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OpenBots.Server.DataAccess.Migrations
{
    public partial class AddAssetAdditionalFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ValueJson",
                table: "Assets");

            migrationBuilder.AddColumn<Guid>(
                name: "BinaryObject",
                table: "Assets",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Json",
                table: "Assets",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "NumberValue",
                table: "Assets",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TextValue",
                table: "Assets",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BinaryObject",
                table: "Assets");

            migrationBuilder.DropColumn(
                name: "Json",
                table: "Assets");

            migrationBuilder.DropColumn(
                name: "NumberValue",
                table: "Assets");

            migrationBuilder.DropColumn(
                name: "TextValue",
                table: "Assets");

            migrationBuilder.AddColumn<string>(
                name: "ValueJson",
                table: "Assets",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}

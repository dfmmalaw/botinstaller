﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OpenBots.Server.DataAccess.Migrations
{
    public partial class InitialSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            #region Terms and Conditions

            migrationBuilder.InsertData(
               table: "UserAgreements",
               columns: new string[] { "Id","Version", "Title", "ContentStaticUrl", "EffectiveOnUTC", "ExpiresOnUTC",
                                        "IsDeleted", "CreatedBy", "CreatedOn", "DeletedBy", "DeleteOn",
               },
                values: new object[] { Guid.NewGuid(), "1", "Terms and Conditions", "https://openbotsserver-dev.azurewebsites.net/Email/Terms.html", DateTime.Parse("01-01-2020"), DateTime.Parse("01-01-2022"),
                                         false, "", DateTime.UtcNow, "", "",
                });

            #endregion

            migrationBuilder.CreateTable(
                name: "Agents",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 100, nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    DeletedBy = table.Column<string>(maxLength: 100, nullable: true),
                    DeleteOn = table.Column<DateTime>(nullable: true),
                    Timestamp = table.Column<byte[]>(rowVersion: true, nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 100, nullable: true),
                    Name = table.Column<string>(nullable: true),
                    MachineName = table.Column<string>(nullable: true),
                    MacAddresses = table.Column<string>(nullable: true),
                    IPAddresses = table.Column<string>(nullable: true),
                    IsEnabled = table.Column<bool>(nullable: false),
                    LastReportedOn = table.Column<DateTime>(nullable: false),
                    LastReportedStatus = table.Column<string>(nullable: true),
                    LastReportedWork = table.Column<string>(nullable: true),
                    LastReportedMessage = table.Column<string>(nullable: true),
                    IsHealthy = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Agents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BinaryObjects",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 100, nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    DeletedBy = table.Column<string>(maxLength: 100, nullable: true),
                    DeleteOn = table.Column<DateTime>(nullable: true),
                    Timestamp = table.Column<byte[]>(rowVersion: true, nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 100, nullable: true),
                    Name = table.Column<string>(nullable: true),
                    OrganizationId = table.Column<Guid>(nullable: true),
                    ContentType = table.Column<string>(nullable: true),
                    CorelationEntityId = table.Column<Guid>(nullable: true),
                    CorelationEntity = table.Column<string>(nullable: true),
                    StoragePath = table.Column<string>(nullable: true),
                    StorageProvider = table.Column<string>(nullable: true),
                    SizeInBytes = table.Column<long>(nullable: true),
                    HashCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BinaryObjects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QueueItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 100, nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    DeletedBy = table.Column<string>(maxLength: 100, nullable: true),
                    DeleteOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 100, nullable: true),
                    OrganizationId = table.Column<Guid>(nullable: true),
                    ProcessID = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Subtopic = table.Column<string>(maxLength: 100, nullable: true),
                    Event = table.Column<string>(maxLength: 100, nullable: true),
                    Source = table.Column<string>(maxLength: 100, nullable: true),
                    Priority = table.Column<int>(nullable: true),
                    QueueItemType = table.Column<string>(maxLength: 20, nullable: true),
                    EntityType = table.Column<string>(maxLength: 255, nullable: true),
                    EntityStatus = table.Column<string>(maxLength: 255, nullable: true),
                    DataJSON = table.Column<string>(nullable: true),
                    DataText = table.Column<string>(nullable: true),
                    DontDequeueUntil = table.Column<DateTime>(nullable: true),
                    DontDequeueAfter = table.Column<DateTime>(nullable: true),
                    IsDequeued = table.Column<bool>(nullable: false),
                    IsLocked = table.Column<bool>(nullable: false),
                    LockedOn = table.Column<DateTime>(nullable: true),
                    LockedUntil = table.Column<DateTime>(nullable: true),
                    LockedBy = table.Column<string>(nullable: true),
                    LockTransactionKey = table.Column<Guid>(nullable: false),
                    RetryCount = table.Column<int>(nullable: true),
                    LastOccuredError = table.Column<string>(nullable: true),
                    Timestamp = table.Column<byte[]>(rowVersion: true, nullable: true),
                    IsError = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QueueItems", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Agents");

            migrationBuilder.DropTable(
                name: "BinaryObjects");

            migrationBuilder.DropTable(
                name: "QueueItems");
        }
    }
}

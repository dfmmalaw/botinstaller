﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OpenBots.Server.DataAccess.Migrations
{
    public partial class RenameAssetFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BinaryObject",
                table: "Assets");

            migrationBuilder.DropColumn(
                name: "Json",
                table: "Assets");

            migrationBuilder.AddColumn<Guid>(
                name: "BinaryObjectID",
                table: "Assets",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JsonValue",
                table: "Assets",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BinaryObjectID",
                table: "Assets");

            migrationBuilder.DropColumn(
                name: "JsonValue",
                table: "Assets");

            migrationBuilder.AddColumn<Guid>(
                name: "BinaryObject",
                table: "Assets",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Json",
                table: "Assets",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

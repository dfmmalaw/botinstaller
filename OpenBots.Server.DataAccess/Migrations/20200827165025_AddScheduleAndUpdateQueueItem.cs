﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OpenBots.Server.DataAccess.Migrations
{
    public partial class AddScheduleAndUpdateQueueItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "LockedBy",
                table: "QueueItems",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Data",
                table: "QueueItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExceptionMessage",
                table: "QueueItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RawData",
                table: "QueueItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "QueueItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StatusMessage",
                table: "QueueItems",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Schedules",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 100, nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    DeletedBy = table.Column<string>(maxLength: 100, nullable: true),
                    DeleteOn = table.Column<DateTime>(nullable: true),
                    Timestamp = table.Column<byte[]>(rowVersion: true, nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 100, nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    AgentName = table.Column<string>(nullable: true),
                    CRONExpression = table.Column<string>(nullable: true),
                    LastExecution = table.Column<DateTime>(nullable: true),
                    NextExecution = table.Column<DateTime>(nullable: true),
                    IsDisabled = table.Column<bool>(nullable: true),
                    ProjectId = table.Column<Guid>(nullable: true),
                    TriggerName = table.Column<string>(nullable: true),
                    Recurrence = table.Column<bool>(nullable: true),
                    StartingType = table.Column<string>(nullable: true),
                    StartJobOn = table.Column<DateTime>(nullable: true),
                    RecurrenceUnit = table.Column<DateTime>(nullable: true),
                    JobRecurEveryUnit = table.Column<DateTime>(nullable: true),
                    EndJobOn = table.Column<DateTime>(nullable: true),
                    EndJobAtOccurence = table.Column<DateTime>(nullable: true),
                    NoJobEndDate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    ExpiryDate = table.Column<DateTime>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schedules", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Schedules");

            migrationBuilder.DropColumn(
                name: "Data",
                table: "QueueItems");

            migrationBuilder.DropColumn(
                name: "ExceptionMessage",
                table: "QueueItems");

            migrationBuilder.DropColumn(
                name: "RawData",
                table: "QueueItems");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "QueueItems");

            migrationBuilder.DropColumn(
                name: "StatusMessage",
                table: "QueueItems");

            migrationBuilder.AlterColumn<string>(
                name: "LockedBy",
                table: "QueueItems",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(Guid),
                oldNullable: true);
        }
    }
}

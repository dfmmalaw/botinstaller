﻿using System;
using System.Runtime.Serialization;
using Microsoft.EntityFrameworkCore;
#nullable enable

namespace OpenBots.Server.DataAccess.Exceptions
{
    [Serializable]
    public class EntityConcurrencyException : EntityOperationException
    {
       

        public EntityConcurrencyException()
        {
        }

        public EntityConcurrencyException(DbUpdateConcurrencyException ex) : base(ex)
        {
        }

        public EntityConcurrencyException(string? message) : base(message)
        {
        }

        public EntityConcurrencyException(Exception innerException) : base(innerException)
        {
        }

        public EntityConcurrencyException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected EntityConcurrencyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

     
    }
}
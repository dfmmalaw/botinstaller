﻿using System;
using System.Runtime.Serialization;
using OpenBots.Server.Model.Core;
#nullable enable

namespace OpenBots.Server.DataAccess.Exceptions
{
    [Serializable]
    public class UnauthorizedOperationException : EntityOperationException
    {
        public EntityOperationType Operation { get; private set; }

        public UnauthorizedOperationException()
        {
            Operation = EntityOperationType.Unknown;
        }

        public UnauthorizedOperationException(EntityOperationType add)
        {
            this.Operation = add;
        }

        public UnauthorizedOperationException(string? message) : base(message)
        {
            Operation = EntityOperationType.Unknown;
        }

        public UnauthorizedOperationException(string? message, Exception? innerException) : base(message, innerException)
        {
            Operation = EntityOperationType.Unknown;
        }

        protected UnauthorizedOperationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Operation = EntityOperationType.Unknown;
        }
    }
}
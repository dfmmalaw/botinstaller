﻿using System;
using System.Runtime.Serialization;
#nullable enable

namespace OpenBots.Server.DataAccess.Exceptions
{
    [Serializable]
    public class EntityDoesNotExistException : EntityOperationException
    {
        public EntityDoesNotExistException()
        {
        }

        public EntityDoesNotExistException(string? message) : base(message)
        {
        }

        public EntityDoesNotExistException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected EntityDoesNotExistException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
﻿using System;
using System.Runtime.Serialization;
using OpenBots.Server.Model.Core;
#nullable enable

namespace OpenBots.Server.DataAccess.Exceptions
{
    [Serializable]
    public class EntityValidationException : EntityOperationException
    {
        public ValidationResults Validation { get; private set; }

        public EntityValidationException()
        {
            Validation = new ValidationResults();
        }

        public EntityValidationException(ValidationResults validation)
        {
            this.Validation = validation;
        }

        public EntityValidationException(string? message) : base(message)
        {
            Validation = new ValidationResults();
        }

        public EntityValidationException(string? message, Exception? innerException) : base(message, innerException)
        {
            Validation = new ValidationResults();
        }

        protected EntityValidationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Validation = new ValidationResults();
        }

     
    }
}
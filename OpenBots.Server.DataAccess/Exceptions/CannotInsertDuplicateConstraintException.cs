﻿#nullable enable
using System;
using System.Runtime.Serialization;
using Microsoft.EntityFrameworkCore;

namespace OpenBots.Server.DataAccess.Exceptions
{
    [Serializable]
    public class CannotInsertDuplicateConstraintException : Exception
    {
      
        public string? EntityName { get; internal set; }
        public string? PropertyName { get; internal set; }
        public string? Value { get; internal set; }

        public CannotInsertDuplicateConstraintException()
        {
        }

        public CannotInsertDuplicateConstraintException(string? message) : base(message)
        {
        }

        public CannotInsertDuplicateConstraintException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        public CannotInsertDuplicateConstraintException(Exception ex, string tableName, string constraintName, string valueName) : base("", ex)
        {
           
            this.EntityName = tableName;
            this.PropertyName = constraintName;
            this.Value = valueName;
        }

        protected CannotInsertDuplicateConstraintException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
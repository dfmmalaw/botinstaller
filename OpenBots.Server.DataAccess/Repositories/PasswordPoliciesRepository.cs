﻿using OpenBots.Server.Core;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Model.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class PasswordPolicyRepository : EntityRepository<PasswordPolicy>, IPasswordPolicyRepository
    {
        public PasswordPolicyRepository(StorageContext context, 
            ILogger<PasswordPolicy> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
            
        }

        protected override DbSet<PasswordPolicy> DbTable()
        {
            return DbContext.PasswordPolicies;
        }
    }
}

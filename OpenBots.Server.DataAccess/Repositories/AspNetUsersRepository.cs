﻿using OpenBots.Server.Core;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Model.Report;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class AspNetUsersRepository : TenantEntityRepository<AspNetUsers>, IAspNetUsersRepository
    {
        public AspNetUsersRepository(StorageContext context,  ILogger<AspNetUsers> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override DbSet<AspNetUsers> DbTable()
        {
            return DbContext.AspNetUsers;
        }
    }
}

﻿using OpenBots.Server.Core;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Model.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class UserConsentRepository : EntityRepository<UserConsent>, IUserConsentRepository
    {
        public UserConsentRepository(StorageContext context,  ILogger<UserConsent> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override DbSet<UserConsent> DbTable()
        {
            return dbContext.UserConsents;
        }

       

    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBots.Server.Core;
using OpenBots.Server.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class QueueItemRepository : EntityRepository<QueueItem>, IQueueItemRepository
    {
        public QueueItemRepository(StorageContext context, ILogger<QueueItem> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override DbSet<QueueItem> DbTable()
        {
            return dbContext.QueueItems;
        }
    }
}

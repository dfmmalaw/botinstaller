﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBots.Server.Core;
using OpenBots.Server.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class CredentialRepository : EntityRepository<Credential>, ICredentialRepository
    {
        public CredentialRepository(StorageContext context, ILogger<Credential> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {

        }

        protected override DbSet<Credential> DbTable()
        {
            return dbContext.Credentials;
        }
    }
}

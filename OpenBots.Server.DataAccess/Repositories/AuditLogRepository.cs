﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBots.Server.Core;
using OpenBots.Server.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    /// <summary>
    /// Audit Log Repository
    /// </summary>
    public class AuditLogRepository: EntityRepository<AuditLog>, IAuditLogRepository
    {
        /// <summary>
        /// Constructor for AuditLogRepository
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="entityEventSink"></param>
        public AuditLogRepository(StorageContext context, ILogger<AuditLog> logger, IEntityOperationEventSink entityEventSink) :base(context, logger, entityEventSink)
        {
        }

        /// <summary>
        /// Retrieves audit logs.
        /// </summary>
        /// <returns></returns>
        protected override DbSet<AuditLog> DbTable()
        {
            return dbContext.AuditLogs;
        }
    }
}

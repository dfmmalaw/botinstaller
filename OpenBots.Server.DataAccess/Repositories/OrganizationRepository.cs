﻿using OpenBots.Server.Core;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Model.Membership;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class OrganizationRepository : TenantEntityRepository<Organization>, IOrganizationRepository
    {
        public OrganizationRepository(StorageContext context, ILogger<Organization> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override Microsoft.EntityFrameworkCore.DbSet<Organization> DbTable()
        {
            return base.DbContext.Organizations;
        }

        public string GenerateBusinessProcessKey(Guid organizationId)
        {
            var param = new SqlParameter("@OrganizationId", organizationId);
            var org = base.DbContext.Organizations.FromSqlRaw("GetNextProcessNumber @OrganizationId", param).ToListAsync().Result;
            if (org != null && org.Count > 0) {
                string ProcessKeyPrefix = (!string.IsNullOrEmpty(org[0].ProcessKeyPrefix)) ? org[0].ProcessKeyPrefix : string.Empty;
                int ProcessKeyNumber = (org[0].ProcessKeyNumber != null && org[0].ProcessKeyNumber.HasValue) ? org[0].ProcessKeyNumber.Value : 0;
                string ProcessKeyNumberString = ProcessKeyNumber.ToString();
                if(ProcessKeyNumber < 1000)
                {
                    ProcessKeyNumberString = ProcessKeyNumber.ToString().PadLeft(3, '0');
                }
                return string.Format("{0}{1}", ProcessKeyPrefix, ProcessKeyNumberString);
            }
            return null;
        }
    }
}

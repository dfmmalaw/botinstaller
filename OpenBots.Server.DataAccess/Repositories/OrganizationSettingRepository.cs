﻿using OpenBots.Server.Core;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Model.Membership;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{

    public class OrganizationSettingRepository : TenantEntityRepository<OrganizationSetting>, IOrganizationSettingRepository
    {
        public OrganizationSettingRepository(StorageContext context,  ILogger<OrganizationSetting> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override DbSet<OrganizationSetting> DbTable()
        {
            return DbContext.OrganizationSettings;
        }
        protected override Func<OrganizationSetting, bool> ParentFilter(Guid parentId)
        {
            return (o => o.OrganizationId == parentId);
        }
    }
}

﻿using OpenBots.Server.Core;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class LookupValueRepository : EntityRepository<LookupValue>, ILookupValueRepository
    {
        public LookupValueRepository(StorageContext context, ILogger<LookupValue> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override DbSet<LookupValue> DbTable()
        {
            return DbContext.LookupValues;
        }
    }
}

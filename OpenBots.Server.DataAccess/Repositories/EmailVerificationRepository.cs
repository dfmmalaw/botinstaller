﻿using OpenBots.Server.Core;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{

    public class EmailVerificationRepository : EntityRepository<EmailVerification>, IEmailVerificationRepository
    {
        public EmailVerificationRepository(StorageContext context, ILogger<EmailVerification> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

    
        protected override DbSet<EmailVerification> DbTable()
        {
            return DbContext.EmailVerifications;
        }

        protected override Func<EmailVerification, bool> ParentFilter(Guid parentId)
        {
            return (o => o.PersonId == parentId);
        }
    }
}

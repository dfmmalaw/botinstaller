﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBots.Server.Core;
using OpenBots.Server.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class QueueRepository : EntityRepository<Queue>, IQueueRepository
    {
        public QueueRepository(StorageContext context, ILogger<Queue> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override DbSet<Queue> DbTable()
        {
            return dbContext.Queues;
        }
    }
}

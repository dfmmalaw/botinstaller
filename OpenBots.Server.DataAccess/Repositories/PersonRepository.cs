﻿using OpenBots.Server.Core;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class PersonRepository : TenantEntityRepository<Person>, IPersonRepository
    {
        public PersonRepository(StorageContext context,  ILogger<Person> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override DbSet<Person> DbTable()
        {
            return DbContext.People;
        }
      
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBots.Server.Core;
using OpenBots.Server.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class BinaryObjectRepository : EntityRepository<BinaryObject>, IBinaryObjectRepository
    {
        public BinaryObjectRepository(StorageContext context, ILogger<BinaryObject> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override DbSet<BinaryObject> DbTable()
        {
            return dbContext.BinaryObjects;
        }
    }
}

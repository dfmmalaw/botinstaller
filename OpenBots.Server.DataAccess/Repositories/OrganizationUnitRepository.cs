﻿using OpenBots.Server.Core;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Model.Membership;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class OrganizationUnitRepository : TenantEntityRepository<OrganizationUnit> , IOrganizationUnitRepository
    {
        public OrganizationUnitRepository(StorageContext context, ILogger<OrganizationUnit> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

     
        protected override DbSet<OrganizationUnit> DbTable()
        {
            return DbContext.OrganizationUnits;
        }

        protected override Func<OrganizationUnit, bool> ParentFilter(Guid parentId)
        {
            return (o => o.OrganizationId.Equals(parentId));
        }
    }
}

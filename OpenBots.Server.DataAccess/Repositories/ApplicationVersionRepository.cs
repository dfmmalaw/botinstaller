﻿using OpenBots.Server.Core;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class ApplicationVersionRepository : EntityRepository<ApplicationVersion>, IApplicationVersionRepository
    {
        public ApplicationVersionRepository(StorageContext context, ILogger<ApplicationVersion> logger, IEntityOperationEventSink entityEventSink) : base(context,  logger, entityEventSink)
        {
        }

        protected override Microsoft.EntityFrameworkCore.DbSet<ApplicationVersion> DbTable()
        {
            return base.DbContext.AppVersion;
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBots.Server.Core;
using OpenBots.Server.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class AssetRepository : EntityRepository<Asset>, IAssetRepository
    {
        public AssetRepository(StorageContext context, ILogger<Asset> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override DbSet<Asset> DbTable()
        {
            return dbContext.Assets;
        }
    }
}

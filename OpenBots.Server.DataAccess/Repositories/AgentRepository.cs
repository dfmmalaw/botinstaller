﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBots.Server.Core;
using OpenBots.Server.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class AgentRepository : EntityRepository<AgentModel>, IAgentRepository
    {
        public AgentRepository(StorageContext context, ILogger<AgentModel> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override DbSet<AgentModel> DbTable()
        {
            return dbContext.Agents;
        }

        public AgentModel GetId(string machineName, string macAddress, string ipAddress)
        {
            var agent = DbTable().Where(AuthorizeRead()).AsQueryable().Where(e => e.MachineName.Equals(machineName) 
                                                                                && e.MacAddresses.Equals(macAddress) 
                                                                                && e.IPAddresses.Equals(ipAddress)).FirstOrDefault();
            return agent;
        }
    }
}

﻿using OpenBots.Server.Core;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Model.Membership;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{

    public class OrganizationUnitMemberRepository : TenantEntityRepository<OrganizationUnitMember>, IOrganizationUnitMemberRepository
    {
        public OrganizationUnitMemberRepository(StorageContext context,  ILogger<OrganizationUnitMember> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override DbSet<OrganizationUnitMember> DbTable()
        {
            return DbContext.OrganizationUnitMembers;
        }


        protected override Func<OrganizationUnitMember, bool> ParentFilter(Guid parentId)
        {
            return (o => o.OrganizationId == parentId);
        }

    }
}

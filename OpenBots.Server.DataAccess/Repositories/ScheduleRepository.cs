﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBots.Server.Core;
using OpenBots.Server.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class ScheduleRepository : EntityRepository<Schedule>, IScheduleRepository
    {
        public ScheduleRepository(StorageContext context, ILogger<Schedule> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override DbSet<Schedule> DbTable()
        {
            return dbContext.Schedules;
        }

    }
}

﻿using OpenBots.Server.Core;
using OpenBots.Server.DataAccess.Exceptions;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using OpenBots.Server.Model;
using Newtonsoft.Json;

namespace OpenBots.Server.DataAccess.Repositories
{
    /// <summary>
    /// Entity Repository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class EntityRepository<T> : ReadOnlyEntityRepository<T>,
        IEntityRepository<T> where T : class, IEntity, new()
    {
        const string DUPLICATE_KEY_ERROR_MESSSAGE = @"Cannot insert duplicate key row in object '(.*)' with unique index '(.*)'. The duplicate key value is (.*).The statement has been terminated.";

        private readonly IEntityOperationEventSink eventSink;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityRepository{T}"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="logger">The logger.</param>
        public EntityRepository(StorageContext context, ILogger<T> logger, IEntityOperationEventSink entityEventSink)
            : base(context, logger)
        {
            eventSink = entityEventSink;
        }

        /// <summary>
        /// Authorizes the operation.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="operation">The operation.</param>
        /// <returns></returns>
        protected virtual bool AuthorizeOperation(T entity, EntityOperationType operation)
        {
            return true;
        }

        /// <summary>
        /// Add entity to appropriate data table
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        /// <exception cref="EntityValidationException"></exception>
        /// <exception cref="EntityAlreadyExistsException"></exception>
        /// <exception cref="EntityOperationException"></exception>
        /// <exception cref="UnauthorizedOperationException"></exception>
        public virtual T Add(T entity)
        {
            if (entity == null)
                throw new EntityOperationException(new ArgumentNullException(nameof(entity)));

            if (AuthorizeOperation(entity, EntityOperationType.Add))
            {
                ValidationResults validation = Validate(entity);
                if (!validation.IsValid)
                    throw new EntityValidationException(validation);

                if (Exists(entity.Id.Value))
                    throw new EntityAlreadyExistsException();

                //get null value from database because it hasn't been added yet
                var originalValues = DbContext.Entry(entity).GetDatabaseValues();

                var savedEntity = DbContext.Add(entity);
                try
                {
                    var change = TrackChange(entity, EntityOperationType.Add, DbContext.Entry(entity).CurrentValues, originalValues);
                    DbContext.SaveChanges();
                    eventSink.RaiseOperationCompletedEvent(change);
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException ex)
                {
                    if (ex.InnerException != null && ex.InnerException.GetType() == typeof(Microsoft.Data.SqlClient.SqlException))
                    {
                        HandleDuplicateConstraint(ex.InnerException as Microsoft.Data.SqlClient.SqlException);
                    }
                    throw new EntityOperationException(ex);

                }
                catch (Exception ex)
                {
                    throw new EntityOperationException(ex);
                }

                return (T)savedEntity.Entity;
            }
            throw new UnauthorizedOperationException(EntityOperationType.Add);
        }

        /// <summary>
        /// Duplicate constraint error handling
        /// </summary>
        /// <param name="ex"></param>
        /// <exception cref="CannotInsertDuplicateConstraintException"></exception>
        private static void HandleDuplicateConstraint(Microsoft.Data.SqlClient.SqlException ex)
        {

            Regex rx = new Regex(DUPLICATE_KEY_ERROR_MESSSAGE,
                RegexOptions.CultureInvariant |
                RegexOptions.IgnoreCase);
            Match match = rx.Match(ex.Message.ToUpperInvariant().Replace("\r", "", StringComparison.InvariantCultureIgnoreCase).Replace("\n", "", StringComparison.InvariantCultureIgnoreCase));
            if (match.Success && match.Groups.Count == 4)
            {
                string tableName = match.Groups[1].Value;

                if (!string.IsNullOrEmpty(tableName) && tableName.Contains(".", StringComparison.InvariantCultureIgnoreCase))
                    tableName = tableName.Split('.').LastOrDefault();

                string constraintName = match.Groups[2].Value;
                if (!string.IsNullOrEmpty(constraintName) && constraintName.Contains("_", StringComparison.InvariantCultureIgnoreCase))
                    constraintName = constraintName.Split('_').LastOrDefault();

                string valueName = match.Groups[3].Value;

                throw new CannotInsertDuplicateConstraintException(ex, tableName, constraintName, valueName);

            }

            throw new CannotInsertDuplicateConstraintException(ex, "", "", "");
        }

        /// <summary>Deletes the specified identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <exception cref="EntityDoesNotExistException"></exception>
        /// <exception cref="UnauthorizedOperationException"></exception>
        /// <exception cref="EntityOperationException"></exception>
        public virtual void Delete(Guid id)
        {
            if (!Exists(id))
                throw new EntityDoesNotExistException();

            T entity = GetOne(id);

            if (!AuthorizeOperation(entity, EntityOperationType.Delete))
            {
                throw new UnauthorizedOperationException(EntityOperationType.Add);
            }
            var originalValues = DbContext.Entry(entity).GetDatabaseValues();

            DbContext.Remove(entity);
            try
            {
                var change = TrackChange(entity, EntityOperationType.HardDelete, DbContext.Entry(entity).CurrentValues, originalValues);
                DbContext.SaveChanges();
                eventSink.RaiseOperationCompletedEvent(change);
            }
            catch (Exception ex)
            {
                throw new EntityOperationException(ex);
            }
        }

        /// <summary>
        /// Softs the delete.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <exception cref="EntityDoesNotExistException"></exception>
        /// <exception cref="UnauthorizedOperationException"></exception>
        /// <exception cref="EntityOperationException"></exception>
        public virtual void SoftDelete(Guid id, string strDeletedBy = "")
        {
            if (!Exists(id))
                throw new EntityDoesNotExistException();

            T entity = GetOne(id);

            if (!AuthorizeOperation(entity, EntityOperationType.Delete))
            {
                throw new UnauthorizedOperationException(EntityOperationType.Add);
            }

            var originalValues = DbContext.Entry(entity).GetDatabaseValues();

            entity.IsDeleted = true;
            entity.DeleteOn = DateTime.UtcNow;
            entity.DeletedBy = strDeletedBy;
            DbContext.Update(entity);
            try
            {
                var change = TrackChange(entity, EntityOperationType.Delete, DbContext.Entry(entity).CurrentValues, originalValues);
                DbContext.SaveChanges();
                eventSink.RaiseOperationCompletedEvent(change);
            }
            catch (Exception ex)
            {
                throw new EntityOperationException(ex);
            }
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="originalTimestamp">Timestamp value of original </param>
        /// <returns></returns>
        /// <exception cref="EntityValidationException"></exception>
        /// <exception cref="EntityDoesNotExistException"></exception>
        /// <exception cref="EntityConcurrencyException"></exception>
        /// <exception cref="EntityOperationException"></exception>
        /// <exception cref="UnauthorizedOperationException"></exception>
        public virtual T Update(T entity, byte[] originalTimestamp = null)
        {
            if (entity == null)
                throw new EntityOperationException(new ArgumentNullException(nameof(entity)));

            if (AuthorizeOperation(entity, EntityOperationType.Update))
            {
                ValidationResults validation = Validate(entity);
                if (!validation.IsValid)
                    throw new EntityValidationException(validation);

                if (!Exists(entity.Id.Value))
                    throw new EntityDoesNotExistException();

                try
                {
                    var originalValues = DbContext.Entry(entity).GetDatabaseValues();

                    if (originalTimestamp != null)
                        DbContext.Entry(entity).Property("Timestamp").OriginalValue = originalTimestamp;

                    DbContext.Entry(entity).Property("UpdatedOn").CurrentValue = DateTime.Now;
                    DbContext.Entry(entity).Property("UpdatedBy").CurrentValue = entity.UpdatedBy;

                    DbContext.Entry(entity).State = EntityState.Modified;
                    DbContext.SaveChanges();  //may need to move below trackchange?
                    var change = TrackChange(entity, EntityOperationType.Update, DbContext.Entry(entity).CurrentValues, originalValues);
                    eventSink.RaiseOperationCompletedEvent(change);
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException ex)
                {
                    if (ex.InnerException != null && ex.InnerException.GetType() == typeof(Microsoft.Data.SqlClient.SqlException))
                    {
                        HandleDuplicateConstraint(ex.InnerException as Microsoft.Data.SqlClient.SqlException);
                    }
                    throw new EntityOperationException(ex);

                }
                catch (Exception ex)
                {
                    throw new EntityOperationException(ex);
                }

                return entity;
            }
            throw new UnauthorizedOperationException(EntityOperationType.Add);
        }

     
        /// <summary>
        /// This method creates a changeset style model for Changes being made to the entity.
        /// This can be used to create audit logs or event queues in conjunction with IEntityOperationEventSink
        /// </summary>
        /// <param name="timestamp"></param>
        /// <param name="entity"></param>
        /// <param name="operation"></param>
        /// <param name="currentValues"></param>
        /// <param name="originalValues"></param>
        /// <returns></returns>

        //protected EntityChange TrackChange(Guid? Id, object entity, EntityOperationType operation = EntityOperationType.Unknown, PropertyValues currentValues = null, PropertyValues originalValues = null)
        protected string TrackChange(T entity, EntityOperationType operation = EntityOperationType.Unknown, PropertyValues currentValues = null, PropertyValues originalValues = null, byte[] timestamp = null)
        {
            //create new AuditLog object
            AuditLog change = new AuditLog();

            try 
            {
                //define properties in object
                change.Id = Guid.NewGuid();

                if (entity.IsDeleted == null)
                    entity.IsDeleted = false;

                change.IsDeleted = entity.IsDeleted;
                change.CreatedBy = entity.CreatedBy;
                change.CreatedOn = entity.CreatedOn;
                change.DeletedBy = entity.DeletedBy;
                change.DeleteOn = entity.DeleteOn;

                if (timestamp == null)
                    timestamp = new byte[1];

                change.Timestamp = timestamp;
                change.UpdatedOn = DateTime.UtcNow;
                //user id
                change.UpdatedBy = UserContext?.PersonId.ToString();
                //name of service that is being changed
                change.ServiceName = entity.ToString();
                //name of how entity is being changed (Add, Update, Delete)
                change.MethodName = operation.ToString();
                //ask about what to do for parameters and exceptions thrown when changing entity
                change.ParametersJson = ""; //TODO: update to show parameters of method
                change.ExceptionJson = ""; //TODO: update to show any exceptions that arise
                change.ChangedFromJson = (originalValues == null) ? "No original value." : JsonConvert.SerializeObject(originalValues.ToObject());
                change.ChangedToJson = (currentValues == null) ? "No current value." : JsonConvert.SerializeObject(currentValues.ToObject());

            }
            catch (Exception ex)
            {
                throw new EntityOperationException(ex);
            }

            //add object to AuditLog data table
            DbContext.Add(change);
            DbContext.SaveChanges();

            //return audit log object as a string
            return change.ToString();
        }


        /// <summary>
        /// Validates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public virtual ValidationResults Validate(T entity)
        {
            ValidationResults results = new ValidationResults();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(entity, serviceProvider: null, items: null);

            var isValid = Validator.TryValidateObject(entity, context, results);

            results.IsValid = isValid;

            return results;
        }
    }
}

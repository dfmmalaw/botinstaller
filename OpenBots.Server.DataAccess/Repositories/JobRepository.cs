﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBots.Server.Core;
using OpenBots.Server.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenBots.Server.DataAccess.Repositories
{
    public class JobRepository : EntityRepository<Job>, IJobRepository
    {
        public JobRepository(StorageContext context, ILogger<Job> logger, IEntityOperationEventSink entityEventSink) : base(context, logger, entityEventSink)
        {
        }

        protected override DbSet<Job> DbTable()
        {
            return dbContext.Jobs;
        }
    }
}

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using OpenBots.Server.Business;
using OpenBots.Server.Core;
using OpenBots.Server.DataAccess;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model;
using System;
using System.Threading.Tasks;
using Xunit;

namespace XUnitTests
{
    public class AgentManagerTests
    {
        [Fact]
        public async Task ValidatesUserId()
        {
            // arrange
            var options = new DbContextOptionsBuilder<StorageContext>()
                .UseInMemoryDatabase(databaseName: "ValidatesId")
                .Options;

            var context = new StorageContext(options);
            var dummyAgent = new AgentModel
            {
                Id = new Guid("10ea9a48-7365-4b86-8897-e1d5969137e6"),
                MachineName = "TestingMachine",
                MacAddresses = "00:00:00:a1:2b:cc",
                IPAddresses = "192.165.1.91"
            };

            Seed(context, dummyAgent);

            var logger = Mock.Of<ILogger<AgentModel>>();
            var entityEventSink = Mock.Of<IEntityOperationEventSink>();

            var repo = new AgentRepository(context,logger, entityEventSink);
            var manager = new AgentManager(repo);

            // act
            var validId = manager.ValidateAgent("10ea9a48-7365-4b86-8897-e1d5969137e6");
            var invalidId = manager.ValidateAgent("10ea9a48-1111-4b86-8897-e1d5969137e6");

            // assert
            Assert.True(validId);
            Assert.False(invalidId);
        }

        [Fact]
        public async Task GetAgentId()
        {
            // arrange
            var options = new DbContextOptionsBuilder<StorageContext>()
                .UseInMemoryDatabase(databaseName: "ValidatesId")
                .Options;

            var context = new StorageContext(options);

            var dummyAgent = new AgentModel
            {
                Id = new Guid("20ea9a48-7365-4b86-8897-e1d5969137e6"),
                MachineName = "TestingMachine",
                MacAddresses = "00:00:00:a1:2b:cc",
                IPAddresses = "192.165.1.91"
            };

            Seed(context, dummyAgent);

            var logger = Mock.Of<ILogger<AgentModel>>();
            var entityEventSink = Mock.Of<IEntityOperationEventSink>();

            var repo = new AgentRepository(context, logger, entityEventSink);
            var manager = new AgentManager(repo);

            // act
            var validAgent = manager.GetAgentId("TestingMachine", "00:00:00:a1:2b:cc", "192.165.1.91");
            var invalidAgent = manager.GetAgentId("FakeMachine", "12:34:56:a7:2b:cc", "192.165.0.91");

            // assert
            Assert.Equal("20ea9a48-7365-4b86-8897-e1d5969137e6", validAgent.Id.ToString());
            Assert.Null(invalidAgent);
        }


        private void Seed(StorageContext context,AgentModel model)
        {
            var items = new[]
            {
                model
            };

            context.Agents.AddRange(items);
            context.SaveChanges();
        }
    }
}

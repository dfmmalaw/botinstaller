﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Security;
using OpenBots.Server.ViewModel;
using OpenBots.Server.WebAPI.Controllers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OpenBots.Server.Web
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class JobsController : EntityController<Job>
    {
        IJobManager jobManager;
        public JobsController(
            IJobRepository repository,
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IJobManager jobManager,
            IHttpContextAccessor httpContextAccessor) : base(repository, userManager, httpContextAccessor, membershipManager)
        {
            this.jobManager = jobManager;
            this.jobManager.SetContext(base.SecurityContext);
        }

        /// <summary>
        /// Provides a list of all Jobs
        /// </summary>
        /// <response code="200">OK,a Paginated list of all Jobs</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>Paginated list of all Jobs </returns>
        [HttpGet]
        [ProducesResponseType(typeof(PaginatedList<Job>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<Job> Get(
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0
            )
        {
            return base.GetMany();
        }

        /// <summary>
        /// Provides a viewmodel list of all Jobs
        /// </summary>
        /// <response code="200">OK,a Paginated list of all Jobs</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>Paginated list of all Jobs </returns>
        [HttpGet("view")]
        [ProducesResponseType(typeof(PaginatedList<JobViewModel>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<JobViewModel> View(
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0
            )
        {
            var paginatedJobs = base.GetMany();
            return jobManager.GetAllJobsView(paginatedJobs);
        }

        /// <summary>
        /// Provides a lookup list of all Jobs agents and processes
        /// </summary>
        /// <response code="200">OK,a list of all Jobs lookup</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>Paginated list of all Jobs lookup</returns>
        [HttpGet("JobAgentsLookup")]
        [ProducesResponseType(typeof(List<JobsLookupViewModel>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public List<JobsLookupViewModel> GetJobAgentsLookup()
        {
            return jobManager.GetJobAgentsLookup();
        }

        /// <summary>
        /// Provides a Job's details for a particular Job Id.
        /// </summary>
        /// <param name="id">Job id</param>
        /// <response code="200">OK, If a Job exists with the given Id.</response>
        /// <response code="304">Not modified</response>
        /// <response code="400">BadRequest,If Job ID is not in the proper format or proper Guid.</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">NotFound, when no Job exists for the given Job ID</response>
        /// <returns>Job details for the given ID</returns>
        [HttpGet("{id}", Name = "GetJob")]
        [ProducesResponseType(typeof(PaginatedList<Job>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                return await base.GetEntity(id);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Provides a Job's details for a particular Job Id.
        /// </summary>
        /// <param name="id">Job id</param>
        /// <response code="200">OK, If a Job exists with the given Id.</response>
        /// <response code="304">Not modified</response>
        /// <response code="400">BadRequest,If Job ID is not in the proper format or proper Guid.</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">NotFound, when no Job exists for the given Job ID</response>
        /// <returns>Job details for the given ID</returns>
        [HttpGet("view/{id}")]
        [ProducesResponseType(typeof(PaginatedList<Job>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> View(string id)
        {
            try
            {
                Job job = new Job();
                job = readRepository.GetOne(new Guid(id));
                if (job == null)
                {
                    ModelState.AddModelError("GetData", "Record does not exist or you do not have authorized access.");
                    return BadRequest(ModelState);
                }

                string timeStamp = "\"" + Convert.ToBase64String(job.Timestamp) + "\"";
                if (Request.Headers.ContainsKey("if-none-match"))
                {
                    string etag = Request.Headers["if-none-match"];

                    if (!string.IsNullOrEmpty(etag) && etag.Equals(timeStamp, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return StatusCode(304); // Not Modified if ETag is same as Current Timestamp
                    }
                }
                try
                {
                    Response.Headers.Add("ETag", timeStamp);
                }
                catch
                {
                    return Ok(jobManager.GetJobView(job));
                }

                return Ok(jobManager.GetJobView(job));
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Adds a new Job to the existing Jobs
        /// </summary>
        /// <remarks>
        /// Adds the Job with unique Job Id to the existing Jobs
        /// </remarks>
        /// <param name="value"></param>
        /// <response code="200">OK,new Job created and returned</response>
        /// <response code="400">BadRequest,When the Job value is not in proper format</response>
        /// <response code="403">Forbidden, unauthorized access</response>
        ///<response code="409">Conflict,concurrency error</response> 
        /// <response code="422">UnprocessabileEntity,when a duplicate record is being entered.</response>
        /// <returns> newly created unique Job Id with route name </returns>
        [HttpPost]
        [ProducesResponseType(typeof(Job), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post([FromBody] Job request)
        {
            if (request == null)
            {
                ModelState.AddModelError("Save", "No data passed");
                return BadRequest(ModelState);
            }

            Guid entityId = Guid.NewGuid();
            if (request.Id == null || request.Id.HasValue || request.Id.Equals(Guid.Empty))
                request.Id = entityId;

            try
            {
                return await base.PostEntity(request);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Updates an Job 
        /// </summary>
        /// <remarks>
        /// Provides an action to update a Job, when Job id and the new details of Job are given
        /// </remarks>
        /// <param name="id">Job Id,produces Bad request if Id is null or Id's don't match</param>
        /// <param name="value">Job details to be updated</param>
        /// <response code="200">OK, If the Job details for the given Job Id has been updated.</response>
        /// <response code="400">BadRequest,if the Job Id is null or Id's don't match</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>OK response with the updated value</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string id, [FromBody] Job request)
        {
            try
            {
                Guid entityId = new Guid(id);

                var existingJob = repository.GetOne(entityId);
                if (existingJob == null) return NotFound();

                existingJob.AgentId = request.AgentId;
                existingJob.StartTime = request.StartTime;
                existingJob.EndTime = request.EndTime;
                existingJob.EnqueueTime = request.EnqueueTime;
                existingJob.DequeueTime = request.DequeueTime;
                existingJob.ProcessId = request.ProcessId;
                existingJob.JobStatus = request.JobStatus;
                existingJob.Message = request.Message;
                existingJob.IsSuccessful = request.IsSuccessful;

                return await base.PutEntity(id, existingJob);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Job", ex.Message);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Deletes a Job with a specified id from the Job.
        /// </summary>
        /// <param name="id">Job ID to be deleted- throws BadRequest if null or empty Guid/</param>
        /// <response code="200">OK,when Job is softdeleted,( isDeleted flag is set to true in DB) </response>
        /// <response code="400">BadRequest,If Job Id is null or empty Guid</response>
        /// <response code="403">Forbidden </response>
        /// <returns>OK response with deleted value </returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(string id)
        {
            return await base.DeleteEntity(id);
        }

        /// <summary>
        /// Updates partial details of Job.
        /// </summary>
        /// <param name="id">Job identifier</param>
        /// <param name="value">Value of the Job to be updated.</param>
        /// <response code="200">OK,If update of Job is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Job values has been updated</returns>

        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id,
            [FromBody] JsonPatchDocument<Job> request)
        {
            return await base.PatchEntity(id, request);
        }
    }
}

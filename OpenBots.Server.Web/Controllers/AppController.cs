﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace OpenBots.Server.WebAPI.Controllers
{
    [Route("Application")]
    [ApiController]
    [AllowAnonymous]
    public class AppController : EntityController<ApplicationVersion>
    {
        public AppController(
            IApplicationVersionRepository repository,
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor) : base(repository, userManager, httpContextAccessor, membershipManager)
        {

          
        }

        protected static string APPNAME = "OpenBots.Server";

        
        [HttpGet("{application}/Version")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Version(string application= "OpenBots.Server")
        {
            try
            {
                var app = repository.Find(null, p => p.Name.Equals(application, StringComparison.OrdinalIgnoreCase))?.Items?.FirstOrDefault();
                if (app == null)
                {
                    ModelState.AddModelError("", "No application found");
                    return BadRequest(ModelState);
                }
                var appVersion = string.Format("{0}.{1}.{2}", app.Major, app.Minor, app.Patch);
                return Ok(appVersion);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        [HttpPut("{application}/Version/Patch/Release")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> PatchRelease(string application= "OpenBots.Server", [FromQuery()] string key = "")
        {
            if (string.IsNullOrEmpty(key) || !key.Equals("TUREACBITR", StringComparison.InvariantCultureIgnoreCase))
                return Unauthorized();
        

            try
            {
                
                var app = repository.Find(null, p => p.Name.Equals(application, StringComparison.OrdinalIgnoreCase))?.Items?.FirstOrDefault();
                if (app == null)
                    return NotFound();

                app.Patch = app.Patch + 1;
                repository.Update(app);
                var appVersion = string.Format("{0}.{1}.{2}", app.Major, app.Minor, app.Patch);
                return Ok(appVersion);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }

        }


        [HttpPut("{application}/Version/Minor/Release")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> MinorRelease(string application = "OpenBots.Server", [FromQuery()] string key = "")
        {
            if (string.IsNullOrEmpty(key) || !key.Equals("TUREACBITR"))
                return Unauthorized();

            try
            {
                var app = repository.Find(null, p => p.Name.Equals(application, StringComparison.OrdinalIgnoreCase))?.Items?.FirstOrDefault();
                if (app == null)
                    return NotFound();

                app.Minor = app.Minor + 1;
                repository.Update(app);
                var appVersion = string.Format("{0}.{1}.{2}", app.Major, app.Minor, app.Patch);
                return Ok(appVersion);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }

        }


    }
}
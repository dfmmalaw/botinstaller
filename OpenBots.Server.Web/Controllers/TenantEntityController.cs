﻿using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Security;
using Microsoft.AspNetCore.Http;

namespace OpenBots.Server.WebAPI.Controllers
{
    public abstract class TenantEntityController<T> : EntityController<T>
         where T : class, IEntity, ITenanted, new()
    {
        protected TenantEntityController(ITenantEntityRepository<T> repository,
          ApplicationIdentityUserManager userManager,
          IHttpContextAccessor httpContextAccessor,
          IMembershipManager membershipManager
          ) : base(repository,
           
           userManager,
           httpContextAccessor,
           membershipManager)
        {
           
        }
    }
}

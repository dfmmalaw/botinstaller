﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Security;
using OpenBots.Server.ViewModel.AgentViewModels;
using OpenBots.Server.WebAPI.Controllers;
using System;
using System.Reflection.PortableExecutable;
using System.Threading.Tasks;

namespace OpenBots.Server.Web.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class AgentsController : EntityController<AgentModel>
    {
        IAgentManager agentManager;
        private IHttpContextAccessor _accessor;
        public AgentsController(
            IAgentRepository repository,
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IAgentManager agentManager,
            IHttpContextAccessor accessor,
            IHttpContextAccessor httpContextAccessor) : base(repository, userManager, httpContextAccessor, membershipManager)
        {
            this.agentManager = agentManager;
            this.agentManager.SetContext(base.SecurityContext);
            _accessor = accessor;
        }

        /// <summary>
        /// Provides a list of all Agents
        /// </summary>
        /// <response code="200">OK,a Paginated list of all Agents</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// /// <returns>Paginated list of all Agents </returns>
        [HttpGet]
        //        [ProducesResponseType(typeof(Agent), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(PaginatedList<AgentModel>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<AgentModel> Get(
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0
            )
        {
            return base.GetMany();
        }

        /// <summary>
        /// Provides an Agent details for a particular Agent Id.
        /// </summary>
        /// <param name="id">Agent id</param>
        /// <response code="200">OK, If an Agent exists with the given Id.</response>
        /// <response code="304">Not modified</response>
        /// <response code="400">BadRequest,If Agent id is not in proper format or proper Guid.</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">NotFound, when no Agent exists for the given Agent id</response>
        /// <returns>Agent details for the given Id</returns>
        [HttpGet("{id}", Name = "GetAgentModel")]
        [ProducesResponseType(typeof(PaginatedList<AgentModel>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                return await base.GetEntity(id);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Provides an Agent id if the provided machine matches an Agent
        /// </summary>
        /// <response code="200">OK,AgentId</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>AgentId that matches provided machine details </returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("connect")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Connect([FromQuery] ConnectAgentViewModel request = null)
        {
            try
            {
                var requestIp = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                var result = this.agentManager.GetAgentId(request.MachineName, request.MacAddresses, requestIp);

                if (result == null)
                {
                    return NotFound("Agent not found");
                }
                return new OkObjectResult(result.Id);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Connect", ex.Message);
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Adds a new Agent to the existing Agents
        /// </summary>
        /// <remarks>
        /// Adds the Agent with unique Agent Id to the existing Agents
        /// </remarks>
        /// <param name="value"></param>
        /// <response code="200">OK,new Agent created and returned</response>
        /// <response code="400">BadRequest,When the Agent value is not in proper format</response>
        /// <response code="403">Forbidden, unauthorized access</response>
        ///<response code="409">Conflict,concurrency error</response> 
        /// <response code="422">UnprocessabileEntity,when a duplicate record is being entered.</response>
        /// <returns> newly created unique Agent Id with route name </returns>
        [HttpPost]
        [ProducesResponseType(typeof(AgentModel), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post([FromBody] AgentModel request)
        {
            try
            {
                return await base.PostEntity(request);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }


        /// <summary>
        /// Updates an Agent 
        /// </summary>
        /// <remarks>
        /// Provides an action to update an Agent, when Agent id and the new details of Agent are given
        /// </remarks>
        /// <param name="id">Agent Id,produces Bad request if Id is null or Id's don't match</param>
        /// <param name="value">Agent details to be updated</param>
        /// <response code="200">OK, If the Agent details for the given Agent Id has been updated.</response>
        /// <response code="400">BadRequest,if the Agent Id is null or Id's don't match</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>OK response with the updated value</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string id, [FromBody] AgentModel request)
        {
            try
            {
                Guid entityId = new Guid(id);
                
                var existingAgent = repository.GetOne(entityId);
                if (existingAgent == null) return NotFound();

                existingAgent.Name = request.Name;
                existingAgent.MachineName = request.MachineName;
                existingAgent.MacAddresses = request.MacAddresses;
                existingAgent.IPAddresses = request.IPAddresses;
                existingAgent.IsEnabled = request.IsEnabled;
                existingAgent.LastReportedOn = request.LastReportedOn;
                existingAgent.LastReportedStatus = request.LastReportedStatus;
                existingAgent.LastReportedWork = request.LastReportedWork;
                existingAgent.LastReportedMessage = request.LastReportedMessage;
                existingAgent.IsHealthy = request.IsHealthy;
                
                return await base.PutEntity(id, existingAgent);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Agent", ex.Message);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Deletes an Agent with a specified id from the Agents.
        /// </summary>
        /// <param name="id">Agent ID to be deleted- throws BadRequest if null or empty Guid/</param>
        /// <response code="200">OK,when Agent is softdeleted,( isDeleted flag is set to true in DB) </response>
        /// <response code="400">BadRequest,If Agent Id is null or empty Guid</response>
        /// <response code="403">Forbidden </response>
        /// <returns>OK response with deleted value </returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(string id)
        {
            return await base.DeleteEntity(id);
        }

        /// <summary>
        /// Updates partial details of Agent.
        /// </summary>
        /// <param name="id">Agent identifier</param>
        /// <param name="value">Value of the Agent to be updated.</param>
        /// <response code="200">OK,If update of Agent is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Agent values has been updated</returns>

        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id,
            [FromBody] JsonPatchDocument<AgentModel> request)
        {
            return await base.PatchEntity(id, request);
        }

        /// <summary>
        /// Performs a Heatbeat on Agent id.
        /// </summary>
        /// <param name="id">Agent identifier</param>
        /// <param name="value">Value of the Agent to be updated.</param>
        /// <response code="200">OK,If update of Agent is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the heartbeat Agent values have been updated</returns>
        [AllowAnonymous]
        [HttpPatch("{id}/heartbeat")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Heartbeat(string id,
            [FromBody] JsonPatchDocument<AgentModel> request)
        {
            if (request == null)
            {
                ModelState.AddModelError("Save", "No data passed");
                return BadRequest(ModelState);
            }

            try
            {
                var validAgent = this.agentManager.ValidateAgent(id);

                if (validAgent)
                {
                    return await base.PatchEntity(id, request);
                }
                else
                {
                    ModelState.AddModelError("Invalid Agent ID", "The Agent ID provided does not match any existing Agents");
                    return BadRequest(ModelState);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Heartbeat", ex.Message);
                return ex.GetActionResult();
            }
        }
    }
}

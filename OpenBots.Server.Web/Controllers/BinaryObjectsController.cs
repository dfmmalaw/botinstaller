﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Security;
using OpenBots.Server.WebAPI.Controllers;
using System;
using System.Threading.Tasks;

namespace OpenBots.Server.Web.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class BinaryObjectsController : EntityController<BinaryObject>
    {
        public BinaryObjectsController(
            IBinaryObjectRepository repository,
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor) : base(repository, userManager, httpContextAccessor, membershipManager)
        {
        }

        /// <summary>
        /// Provides a list of all BinaryObjects
        /// </summary>
        /// <response code="200">OK,a Paginated list of all BinaryObjects</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// /// <returns>Paginated list of all BinaryObjects </returns>
        [HttpGet]
        //        [ProducesResponseType(typeof(BinaryObject), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(PaginatedList<BinaryObject>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<BinaryObject> Get(
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0
            )
        {
            return base.GetMany();
        }

        /// <summary>
        /// Provides an BinaryObject details for a particular BinaryObject Id.
        /// </summary>
        /// <param name="id">BinaryObject id</param>
        /// <response code="200">OK, If an BinaryObject exists with the given Id.</response>
        /// <response code="304">Not modified</response>
        /// <response code="400">BadRequest,If BinaryObject id is not in proper format or proper Guid.</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">NotFound, when no BinaryObject exists for the given BinaryObject id</response>
        /// <returns>BinaryObject details for the given Id</returns>
        [HttpGet("{id}", Name = "GetBinaryObject")]
        [ProducesResponseType(typeof(PaginatedList<BinaryObject>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                return await base.GetEntity(id);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Adds a new BinaryObject to the existing BinaryObjects
        /// </summary>
        /// <remarks>
        /// Adds the BinaryObject with unique BinaryObject Id to the existing BinaryObjects
        /// </remarks>
        /// <param name="value"></param>
        /// <response code="200">OK,new BinaryObject created and returned</response>
        /// <response code="400">BadRequest,When the BinaryObject value is not in proper format</response>
        /// <response code="403">Forbidden, unauthorized access</response>
        ///<response code="409">Conflict,concurrency error</response> 
        /// <response code="422">UnprocessabileEntity,when a duplicate record is being entered.</response>
        /// <returns> newly created unique BinaryObject Id with route name </returns>
        [HttpPost]
        [ProducesResponseType(typeof(BinaryObject), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post([FromBody] BinaryObject request)
        {
            if (request == null)
            {
                ModelState.AddModelError("Save", "No data passed");
                return BadRequest(ModelState);
            }

            Guid entityId = Guid.NewGuid();
            if (request.Id == null || request.Id.HasValue || request.Id.Equals(Guid.Empty))
                request.Id = entityId;

            try
            {
                return await base.PostEntity(request);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }


        /// <summary>
        /// Updates an BinaryObject 
        /// </summary>
        /// <remarks>
        /// Provides an action to update an BinaryObject, when BinaryObject id and the new details of BinaryObject are given
        /// </remarks>
        /// <param name="id">BinaryObject Id,produces Bad request if Id is null or Id's don't match</param>
        /// <param name="value">BinaryObject details to be updated</param>
        /// <response code="200">OK, If the BinaryObject details for the given BinaryObject Id has been updated.</response>
        /// <response code="400">BadRequest,if the BinaryObject Id is null or Id's don't match</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>OK response with the updated value</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string id, [FromBody] BinaryObject request)
        {
            try
            {
                Guid entityId = new Guid(id);

                var existingBinaryObject = repository.GetOne(entityId);
                if (existingBinaryObject == null) return NotFound();

                existingBinaryObject.Name = request.Name;
                existingBinaryObject.OrganizationId = request.OrganizationId;
                existingBinaryObject.ContentType = request.ContentType;
                existingBinaryObject.CorelationEntityId = request.CorelationEntityId;
                existingBinaryObject.CorelationEntity = request.CorelationEntity;
                existingBinaryObject.StoragePath = request.StoragePath;
                existingBinaryObject.StorageProvider = request.StorageProvider;
                existingBinaryObject.SizeInBytes = request.SizeInBytes;
                existingBinaryObject.HashCode = request.HashCode;
                
                return await base.PutEntity(id, existingBinaryObject);
                                     
            }
            catch (Exception ex)     
            {
                ModelState.AddModelError("BinaryObject", ex.Message);
                return BadRequest(ModelState);
            }
            
        }

        /// <summary>
        /// Deletes an BinaryObject with a specified id from the BinaryObjects.
        /// </summary>
        /// <param name="id">BinaryObject ID to be deleted- throws BadRequest if null or empty Guid/</param>
        /// <response code="200">OK,when BinaryObject is softdeleted,( isDeleted flag is set to true in DB) </response>
        /// <response code="400">BadRequest,If BinaryObject Id is null or empty Guid</response>
        /// <response code="403">Forbidden </response>
        /// <returns>OK response with deleted value </returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(string id)
        {
            return await base.DeleteEntity(id);
        }

        /// <summary>
        /// Updates partial details of BinaryObject.
        /// </summary>
        /// <param name="id">BinaryObject identifier</param>
        /// <param name="value">Value of the BinaryObject to be updated.</param>
        /// <response code="200">OK,If update of BinaryObject is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial BinaryObject values has been updated</returns>

        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id,
            [FromBody] JsonPatchDocument<BinaryObject> request)
        {
            return await base.PatchEntity(id, request);
        }
    }
}

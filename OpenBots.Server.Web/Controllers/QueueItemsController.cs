﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Security;
using OpenBots.Server.Web.Hubs;
using OpenBots.Server.WebAPI.Controllers;
using System;
using System.Threading.Tasks;

namespace OpenBots.Server.Web.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class QueueItemsController : EntityController<QueueItem>
    {
        readonly IQueueItemManager manager;
        private IHubContext<NotificationHub> _hub;

        public QueueItemsController(
            IQueueItemRepository repository,
            IQueueItemManager manager,
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IHubContext<NotificationHub> hub,
            IHttpContextAccessor httpContextAccessor) : base(repository, userManager, httpContextAccessor, membershipManager)
        {
            this.manager = manager;
            _hub = hub;
        }

        /// <summary>
        /// Provides a list of all QueueItems
        /// </summary>
        /// <response code="200">OK,a Paginated list of all QueueItems</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// /// <returns>Paginated list of all QueueItems </returns>
        [HttpGet]
        //        [ProducesResponseType(typeof(QueueItem), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(PaginatedList<QueueItem>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<QueueItem> Get(
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0
            )
        {
            return base.GetMany();
        }

        /// <summary>
        /// Provides an QueueItem details for a particular QueueItem Id.
        /// </summary>
        /// <param name="id">QueueItem id</param>
        /// <response code="200">OK, If an QueueItem exists with the given Id.</response>
        /// <response code="304">Not modified</response>
        /// <response code="400">BadRequest,If QueueItem id is not in proper format or proper Guid.</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">NotFound, when no QueueItem exists for the given QueueItem id</response>
        /// <returns>QueueItem details for the given Id</returns>
        [HttpGet("{id}", Name = "GetQueueItem")]
        [ProducesResponseType(typeof(PaginatedList<QueueItem>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                return await base.GetEntity(id);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Adds a new QueueItem to the existing QueueItems
        /// </summary>
        /// <remarks>
        /// Adds the QueueItem with unique QueueItem Id to the existing QueueItems
        /// </remarks>
        /// <param name="value"></param>
        /// <response code="200">OK,new QueueItem created and returned</response>
        /// <response code="400">BadRequest,When the QueueItem value is not in proper format</response>
        /// <response code="403">Forbidden, unauthorized access</response>
        ///<response code="409">Conflict,concurrency error</response> 
        /// <response code="422">UnprocessabileEntity,when a duplicate record is being entered.</response>
        /// <returns> newly created unique QueueItem Id with route name </returns>
        [HttpPost]
        [ProducesResponseType(typeof(QueueItem), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post([FromBody] QueueItem request)
        {
            if (request == null)
            {
                ModelState.AddModelError("Save", "No data passed");
                return BadRequest(ModelState);
            }

            Guid entityId = Guid.NewGuid();
            if (request.Id == null || request.Id.HasValue || request.Id.Equals(Guid.Empty))
                request.Id = entityId;

            try
            {
                var response = await base.PostEntity(request);
                //Send SignalR notification to all connected clients 
                await _hub.Clients.All.SendAsync("sendnotification", "New QueueItems added.");

                return response;
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }


        /// <summary>
        /// Updates an QueueItem 
        /// </summary>
        /// <remarks>
        /// Provides an action to update an QueueItem, when QueueItem id and the new details of QueueItem are given
        /// </remarks>
        /// <param name="id">QueueItem Id,produces Bad request if Id is null or Id's don't match</param>
        /// <param name="value">QueueItem details to be updated</param>
        /// <response code="200">OK, If the QueueItem details for the given QueueItem Id has been updated.</response>
        /// <response code="400">BadRequest,if the QueueItem Id is null or Id's don't match</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>OK response with the updated value</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string id, [FromBody] QueueItem request)
        {
            try
            {
                Guid entityId = new Guid(id);

                var existingQueueItem = repository.GetOne(entityId);
                if (existingQueueItem == null) return NotFound();

                existingQueueItem.Name = request.Name;
                existingQueueItem.Subtopic = request.Subtopic;
                existingQueueItem.Event = request.Event;
                existingQueueItem.Source = request.Source;
                existingQueueItem.Priority = request.Priority;
                existingQueueItem.QueueItemType = request.QueueItemType;
                existingQueueItem.EntityType = request.EntityType;
                existingQueueItem.EntityStatus = request.EntityStatus;
                existingQueueItem.DataJSON = request.DataJSON;
                existingQueueItem.DataText = request.DataText;
                existingQueueItem.QueueID = request.QueueID;
                //existingQueueItem.DontDequeueUntil = request.DontDequeueUntil;
                //existingQueueItem.DontDequeueAfter = request.DontDequeueAfter;
                //existingQueueItem.IsDequeued = request.IsDequeued;
                //existingQueueItem.IsLocked = request.IsLocked;
                //existingQueueItem.LockedOn = request.LockedOn;
                //existingQueueItem.LockedUntil = request.LockedUntil;
                //existingQueueItem.LockedBy = request.LockedBy;
                //existingQueueItem.LockTransactionKey = request.LockTransactionKey;
                //existingQueueItem.RetryCount = request.RetryCount;
                //existingQueueItem.LastOccuredError = request.LastOccuredError;
                //existingQueueItem.IsError = request.IsError;

                var response = await base.PutEntity(id, existingQueueItem);
                await _hub.Clients.All.SendAsync("sendnotification", string.Format("QueueItem {0} updated.", existingQueueItem.Name));

                return response;
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("QueueItem", ex.Message);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Deletes an QueueItem with a specified id from the QueueItems.
        /// </summary>
        /// <param name="id">QueueItem ID to be deleted- throws BadRequest if null or empty Guid/</param>
        /// <response code="200">OK,when QueueItem is softdeleted,( isDeleted flag is set to true in DB) </response>
        /// <response code="400">BadRequest,If QueueItem Id is null or empty Guid</response>
        /// <response code="403">Forbidden </response>
        /// <returns>OK response with deleted value </returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(string id)
        {
            var response = await base.DeleteEntity(id);
            _hub.Clients.All.SendAsync("sendnotification", "QueueItem deleted.");

            return response;
        }

        /// <summary>
        /// Updates partial details of QueueItem.
        /// </summary>
        /// <param name="id">QueueItem identifier</param>
        /// <param name="value">Value of the QueueItem to be updated.</param>
        /// <response code="200">OK,If update of QueueItem is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial QueueItem values has been updated</returns>

        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id,
            [FromBody] JsonPatchDocument<QueueItem> request)
        {
            return await base.PatchEntity(id, request);
        }

        /// <summary>
        /// Enqueue QueueItem.
        /// </summary>
        /// <param name="value">Value of the QueueItem to be added.</param>
        /// <response code="200">OK,Guid of QueueItem. </response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>        
        [HttpPost("Enqueue")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Enqueue([FromBody] QueueItem value)
        {
            try
            {
                Guid lockedById = Guid.Empty;
                string lockedby = applicationUser?.Id; 
                Guid.TryParse(lockedby, out lockedById);
                value.LockedBy = lockedById;

                var response = await manager.Enqueue(value);
                return Ok(response);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Enqueue", ex.Message);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Dequeue QueueItem.
        /// </summary>
        /// <param name="topic">topic to be added.</param>
        /// <param name="subtopic">subtopic to be added.</param>
        /// <param name="entityName">entityName to be added.</param>
        /// <param name="status">status to be added.</param>
        /// <param name="includeError">includeError to be added.</param>
        /// <response code="200">OK,QueueItem. </response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>        
        [HttpPost("Dequeue")]
        [ProducesResponseType(typeof(QueueItemDequeueDto), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Dequeue(string topic, string subtopic = "", string entityName = "", string status = "", bool includeError = false)
        {
            try
            {
                string lockedby = applicationUser?.Id;
                var response = await manager.Dequeue(topic, subtopic, entityName, status, includeError, lockedby);
                return Ok(response);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Enqueue", ex.Message);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Commit QueueItem.
        /// </summary>
        /// <param name="id">QueueItem Id to be commited.</param>
        /// <param name="transactionKey">transactionKeyId to be verified.</param>
        /// <response code="200">OK</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>        
        [HttpPost("Commit/{id}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Commit(string id, string transactionKey)
        {
            try
            {
                Guid queueItemId = Guid.Parse(id);
                Guid transactionKeyId = Guid.Parse(transactionKey);

                await manager.Commit(queueItemId, transactionKeyId);
                return Ok("Transaction commit successfully.");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Commit", ex.Message);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Rollback QueueItem.
        /// </summary>
        /// <param name="id">QueueItem Id to be commited.</param>
        /// <param name="transactionKey">transactionKeyId to be verified.</param>
        /// <param name="error">error to be stored.</param>
        /// <response code="200">OK</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>        
        [HttpPost("Rollback/{id}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Rollback(string id, string transactionKey, string error)
        {
            try
            {
                Guid queueItemId = Guid.Parse(id);
                Guid transactionKeyId = Guid.Parse(transactionKey);

                await manager.Rollback(queueItemId, transactionKeyId, error);
                return Ok("Transaction rollback successfully.");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Rollback", ex.Message);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Extend QueueItem.
        /// </summary>
        /// <param name="id">QueueItem Id to be commited.</param>
        /// <param name="transactionKey">transactionKeyId to be verified.</param>
        /// <response code="200">OK</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>        
        [HttpPost("Extend/{id}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Extend(string id, string transactionKey)
        {
            try
            {
                Guid queueItemId = Guid.Parse(id);
                Guid transactionKeyId = Guid.Parse(transactionKey);
             
                var response = manager.Extend(queueItemId, transactionKeyId);
                return Ok("Transaction time extend successfully.");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Extend", ex.Message);
                return BadRequest(ModelState);
            }
        }
    }
}

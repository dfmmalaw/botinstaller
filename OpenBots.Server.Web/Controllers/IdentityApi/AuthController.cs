﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using OpenBots.Server.Business;
using OpenBots.Server.Core;
using OpenBots.Server.DataAccess.Exceptions;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Security;
using OpenBots.Server.Security.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using OpenBots.Server.Web;

namespace OpenBots.Server.WebAPI.Controllers.IdentityApi
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [AllowAnonymous] //This controller will be used for token generation
    public class AuthController : ApplicationBaseController
    {
        readonly ApplicationIdentityUserManager userManager;
        readonly SignInManager<ApplicationUser> signInManager;
        readonly IConfiguration configuration;
        readonly ILogger<AuthController> logger;
        readonly IMembershipManager membershipManager;
        readonly IPersonRepository personRepository;
        readonly IEmailManager emailSender;
        readonly IPersonEmailRepository personEmailRepository;
        readonly IEmailVerificationRepository emailVerificationRepository;
        readonly IPasswordPolicyRepository passwordPolicyRepository;
        readonly IOrganizationManager organizationManager;
        readonly IAccessRequestsManager accessRequestManager;
        readonly IOrganizationMemberRepository organizationMemberRepository;
        readonly ITermsConditionsManager termsConditionsManager;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="signInManager"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="claimsFactory"></param>
        /// <param name="signInManagerlogger"></param>
        /// <param name="configuration"></param>
        /// <param name="logger"></param>
        /// <param name="membershipManager"></param>
        /// <param name="personRepository"></param>
        /// <param name="personEmailRepository"></param>
        /// <param name="emailVerificationRepository"></param>
        /// <param name="emailSender"></param>
        public AuthController(
           ApplicationIdentityUserManager userManager,
           SignInManager<ApplicationUser> signInManager,
           IHttpContextAccessor httpContextAccessor,
           IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory,
           ILogger<SignInManager<ApplicationUser>> signInManagerlogger,
           IConfiguration configuration,
           ILogger<AuthController> logger,
           IMembershipManager membershipManager,
           IPersonRepository personRepository,
           IPersonEmailRepository personEmailRepository,
           IEmailVerificationRepository emailVerificationRepository,
           IPasswordPolicyRepository passwordPolicyRepository,
           IEmailManager emailSender,
           IOrganizationManager organizationManager,
           IAccessRequestsManager accessRequestManager,
           IOrganizationMemberRepository organizationMemberRepository,
           ITermsConditionsManager termsConditionsManager
            ) : base(httpContextAccessor, userManager, membershipManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
            this.logger = logger;
            this.membershipManager = membershipManager;
            this.personRepository = personRepository;
            this.emailSender = emailSender;
            this.personEmailRepository = personEmailRepository;
            this.emailVerificationRepository = emailVerificationRepository;
            this.passwordPolicyRepository = passwordPolicyRepository;
            this.organizationManager = organizationManager;
            this.accessRequestManager = accessRequestManager;
            this.organizationMemberRepository = organizationMemberRepository;
            this.termsConditionsManager = termsConditionsManager;
        }

        /// <summary>
        /// Login with username and password
        /// </summary>
        /// <param name="loginModel">Input model</param>
        /// <returns>JWT Token</returns>
        [HttpPost]
        [Route("token")]
        public async Task<IActionResult> CreateToken([FromBody] LoginModel loginModel)
        {
            logger.LogInformation(string.Format("Login email : {0}", loginModel.Email));
            if (ModelState.IsValid)
            {
                ApplicationUser user = null;
                //sign ID
                string signInUser = loginModel.Email;
                if (RegexUtilities.IsValidEmail(signInUser))
                {
                    //First check if emailId exists
                    user = await userManager.FindByEmailAsync(loginModel.Email).ConfigureAwait(true);
                }
                else //Not email ID then find by username.
                {
                    user = await userManager.FindByNameAsync(signInUser).ConfigureAwait(true);
                }

                if (user == null) return Unauthorized();
                signInUser = user?.UserName;

                var loginResult = await signInManager.PasswordSignInAsync(signInUser, loginModel.Password, isPersistent: false, lockoutOnFailure: false).ConfigureAwait(true);
                if (!loginResult.Succeeded)
                {
                    return Unauthorized();
                }

                user = await userManager.FindByNameAsync(signInUser).ConfigureAwait(true);
                string authenticationToken = GetToken(user);
                VerifyUserEmailAsync(user);

                string startsWith = "";
                int skip = 0;
                int take = 100;
                var personOrgs = membershipManager.Search(user.PersonId, startsWith, skip, take);
                bool isUserConsentRequired = VerifyUserAgreementConsentStatus(user.PersonId);
                var pendingAcessOrgs = membershipManager.PendingOrganizationAccess(user.PersonId);
                var newRefreshToken = GenerateRefreshToken();
                var authenticatedUser = new
                {
                    personId = user.PersonId,
                    email = user.Email,
                    userName = user.UserName,
                    token = authenticationToken,
                    refreshToken = newRefreshToken,
                    user.ForcedPasswordChange,
                    isUserConsentRequired,
                    IsJoinOrgRequestPending = (pendingAcessOrgs?.Items?.Count > 0) ? true : false,
                    myOrganizations = personOrgs?.Items,
                };

                // not sure (delete this after confirming that the refresh token is being saved)
                var checkSave = await userManager.SetAuthenticationTokenAsync(user, userManager.Options.Tokens.AuthenticatorTokenProvider, "refresh", newRefreshToken).ConfigureAwait(false);

                return Ok(authenticatedUser);
            }
            return BadRequest(ModelState);

        }

        private async void VerifyUserEmailAsync(ApplicationUser user)
        {
            //Verify email address is confirmed with identity 
            var emailConfirmed = userManager.IsEmailConfirmedAsync(user).Result;
            if (!emailConfirmed)
            {
                string emailVerificationToken = userManager.GenerateEmailConfirmationTokenAsync(user).Result;
                var result = userManager.ConfirmEmailAsync(user, emailVerificationToken).Result;

                if (result.Succeeded)
                {
                    //
                    //emailverification 
                    var emailVerification = emailVerificationRepository.Find(null, p => p.PersonId == user.PersonId && p.IsVerified != true)?.Items?.FirstOrDefault();
                    if (emailVerification != null)
                    {
                        var verifiedEmailAddress = personEmailRepository.Find(null, p => p.Address.Equals(emailVerification.Address, StringComparison.OrdinalIgnoreCase))?.Items?.FirstOrDefault();
                        if (verifiedEmailAddress == null)
                        {
                            var personEmail = new PersonEmail()
                            {
                                EmailVerificationId = emailVerification.Id,
                                IsPrimaryEmail = true,
                                PersonId = emailVerification.PersonId,
                                Address = emailVerification.Address
                            };
                            personEmailRepository.Add(personEmail);
                        }

                        //Verification completed
                        emailVerification.IsVerified = true;
                        emailVerificationRepository.Update(emailVerification);
                    }
                }
            }
        }

        /// <summary>
        /// Signup /  Register new user
        /// </summary>
        /// <param name="signupModel">Signup model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Register")]
        [ProducesResponseType(typeof(ServiceBadRequest), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Register(SignUpViewModel signupModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (signupModel.CreateNewOrganization)
            {
                var checkOrganization = organizationManager.GetDefaultOrganization();
                if (checkOrganization != null)
                {
                    ModelState.AddModelError("", "Default organization exists, you can not create new organization.");
                    return BadRequest(ModelState);
                }
            }
            
            var emailAddress = personEmailRepository.Find(null, p => p.Address.Equals(signupModel.Email, StringComparison.OrdinalIgnoreCase));
            if (emailAddress != null && emailAddress.TotalCount > 0)
            {
                ModelState.AddModelError("","Email address already exists");
                return BadRequest(ModelState);
            }
            
            if (signupModel.CreateNewOrganization == true && string.IsNullOrWhiteSpace(signupModel.Organization))
            {
                ModelState.AddModelError("", "Organization Name is required");
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser()
            {
                Name = signupModel.Name,
                UserName = signupModel.Email,
                Email = signupModel.Email,
                Utm_Campaign = signupModel.Utm_Campaign,
                Utm_Content = signupModel.Utm_Content,
                Utm_Medium = signupModel.Utm_Medium,
                Utm_Source = signupModel.Utm_Source,
                Utm_Term = signupModel.Utm_Term,
                Plan = signupModel.Plan,
                ForcedPasswordChange = false //set this property to donot show password reset secreen for new user
            };

            RandomPassword randomPass = new RandomPassword();
            string passwordString = "";
            bool isPasswordProvided = false;

            if (string.IsNullOrWhiteSpace(signupModel.Password))
            {
                passwordString = randomPass.GenerateRandomPassword();
                isPasswordProvided = false;
            }
            else
            {
                passwordString = signupModel.Password;
                isPasswordProvided = true;
            }
            
            var loginResult = await userManager.CreateAsync(user, passwordString).ConfigureAwait(false);

            if (!loginResult.Succeeded)
            {
                return GetErrorResult(loginResult);
            }
            else
            {
                //Add person email
                var emailIds = new List<EmailVerification>();
                var personEmail = new EmailVerification()
                {
                    PersonId = Guid.Empty,
                    Address = signupModel.Email,
                    IsVerified = false
                };
                emailIds.Add(personEmail);

                Person newPerson = new Person()
                {
                    Company = signupModel.Organization,
                    Department = signupModel.Department,
                    Name = signupModel.Name,
                    EmailVerifications = emailIds
                };
                var person = personRepository.Add(newPerson);

                //Create new organization if count is zero
                if (signupModel.CreateNewOrganization)
                {
                    Model.Membership.Organization value = new Model.Membership.Organization();
                    value.Name = signupModel.Organization;
                    value.IsPublic = true;
                    value.Description = "System created organization";
                    value.ProcessKeyPrefix = "P"; //TODO : Hardcoding to default
                    var newOrganization = organizationManager.AddNewOrganization(value);

                    if (newOrganization != null && newOrganization.Id != null)
                    {
                        Model.Membership.OrganizationMember newOrgMember = new Model.Membership.OrganizationMember()
                        {
                            PersonId = person.Id,
                            OrganizationId = newOrganization.Id,
                            IsAutoApprovedByEmailAddress = true,
                            IsAdministrator = true
                        };

                        organizationMemberRepository.ForceIgnoreSecurity();
                        organizationMemberRepository.Add(newOrgMember);
                        organizationMemberRepository.ForceSecurity();
                    }
                }
                else
                {
                    var oldOrganization = organizationManager.GetDefaultOrganization();
                    if (oldOrganization != null)
                    {
                        //Add it to access requests
                        Model.Membership.AccessRequest accessRequest = new Model.Membership.AccessRequest()
                        {
                            OrganizationId = oldOrganization.Id,
                            PersonId = person.Id,
                            IsAccessRequested = true,
                            AccessRequestedOn = DateTime.UtcNow
                        };

                        accessRequestManager.AddAnonymousAccessRequest(accessRequest);
                    }
                }
                

                string code = await userManager.GenerateEmailConfirmationTokenAsync(user).ConfigureAwait(false);
                Console.WriteLine("Code:" + code + "ID: " + user.Id + "Password: " + passwordString);
                string emailBody = SendConfirmationEmail(code, user.Id, passwordString, "en");
                var subject = "Confirm your account at " + Constants.PRODUCT;
                await emailSender.SendEmailAsync(user.Email, subject, emailBody).ConfigureAwait(false);

                //Update the user 
                if (person != null)
                {
                    var registeredUser = userManager.FindByNameAsync(user.UserName).Result;
                    registeredUser.PersonId = (Guid)person.Id;
                    //registeredUser.ForcedPasswordChange = true;
                    await userManager.UpdateAsync(registeredUser).ConfigureAwait(false);
                }
                //just a comment on auth controller
            }

            return Ok();
        }

        [HttpPost]
        [Consumes("application/x-www-form-urlencoded")]
        [Route("WpRegister")]
        [ProducesResponseType(typeof(ServiceBadRequest), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> WpRegister([FromForm] SignUpViewModel signupModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var apiKeyString = configuration.GetValue<String>("WordPressWebHook:ApiKey");
            var apiKeyGuid = Guid.Parse(apiKeyString);

            if (signupModel?.ApiKey != apiKeyGuid)
            {
                ModelState.AddModelError("", "API Key sent in request is not valid");
                return BadRequest(ModelState);
            }

            var emailAddress = personEmailRepository.Find(null, p => p.Address.Equals(signupModel.Email, StringComparison.OrdinalIgnoreCase));
            if (emailAddress != null && emailAddress.TotalCount > 0)
            {
                ModelState.AddModelError("", "Email address already exists");
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser()
            {
                Name = signupModel.Name,
                UserName = signupModel.Email,
                Email = signupModel.Email,
                Utm_Campaign = signupModel.Utm_Campaign,
                Utm_Content = signupModel.Utm_Content,
                Utm_Medium = signupModel.Utm_Medium,
                Utm_Source = signupModel.Utm_Source,
                Utm_Term = signupModel.Utm_Term,
                Plan = signupModel.Plan
            };

            RandomPassword randomPass = new RandomPassword();
            string passwordString = randomPass.GenerateRandomPassword();
            var loginResult = await userManager.CreateAsync(user, passwordString).ConfigureAwait(false);

            if (!loginResult.Succeeded)
            {
                return GetErrorResult(loginResult);
            }
            else
            {
                //Add person email
                var emailIds = new List<EmailVerification>();
                var personEmail = new EmailVerification()
                {
                    PersonId = Guid.Empty,
                    Address = signupModel.Email,
                    IsVerified = false
                };
                emailIds.Add(personEmail);

                Person newPerson = new Person()
                {
                    Company = signupModel.Organization,
                    Department = signupModel.Department,
                    Name = signupModel.Name,
                    EmailVerifications = emailIds
                };
                var person = personRepository.Add(newPerson);


                string code = await userManager.GenerateEmailConfirmationTokenAsync(user).ConfigureAwait(false);
                string emailBody = SendConfirmationEmail(code, user.Id, passwordString, "en");
                var subject = "Confirm your account at " + Constants.PRODUCT;
                await emailSender.SendEmailAsync(user.Email, subject, emailBody).ConfigureAwait(false);

                //Update the user 
                if (person != null)
                {
                    var registeredUser = userManager.FindByNameAsync(user.UserName).Result;
                    registeredUser.PersonId = (Guid)person.Id;
                    registeredUser.ForcedPasswordChange = true;
                    await userManager.UpdateAsync(registeredUser).ConfigureAwait(false);
                }
            }

            return Ok();
        }

        /// <summary>
        /// Change / Reset with new password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("ChangePassword")]
        public async Task<IActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (applicationUser == null)
                return Unauthorized();
            
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!IsPasswordValid(model.NewPassword))
            {
                ModelState.AddModelError("Password", PasswordRequirementMessage(model.NewPassword));
                return BadRequest(ModelState);
            }

            applicationUser.ForcedPasswordChange = false;
            IdentityResult result = await userManager.ChangePasswordAsync(applicationUser, model.OldPassword, model.NewPassword).ConfigureAwait(false);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        /// <summary>
        /// Verify User Token before resetting the password
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("VerifyUserToken")]
        public async Task<IActionResult> VerifyUserToken(string userId, string code)
        {
            ApplicationUser user = await userManager.FindByIdAsync(userId).ConfigureAwait(false);
            if (user == null)
                return Redirect(string.Format("{0}{1}", configuration["WebAppUrl:Url"], configuration["WebAppUrl:NoUserExists"]));
            if (userManager.VerifyUserTokenAsync(user, userManager.Options.Tokens.PasswordResetTokenProvider, "ResetPassword", code).Result)
            {
                string baseUrl = string.Format(@"{0}{1}", configuration["WebAppUrl:Url"], configuration["WebAppUrl:forgotpassword"]);
                var callbackUrl = string.Format(@"{0}?userid={1}&token={2}", baseUrl, WebUtility.UrlEncode(userId), WebUtility.UrlEncode(code));
                return Redirect(callbackUrl);
            }
            else return Redirect(string.Format("{0}{1}", configuration["WebAppUrl:Url"], configuration["WebAppUrl:tokenerror"]));
        }

        /// <summary>
        /// Set new password 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [AllowAnonymous]
        [Route("SetPassword")]
        public async Task<IActionResult> SetPassword(ResetPasswordBindingModel model)
        {
            if (string.IsNullOrEmpty(model.UserId) || string.IsNullOrEmpty(model.NewPassword) || string.IsNullOrEmpty(model.Token))
            {
                ModelState.AddModelError("", "userId or password or token is missing");
                return BadRequest(ModelState);
            }

            if (!IsPasswordValid(model.NewPassword))
            {
                ModelState.AddModelError("Password", PasswordRequirementMessage(model.NewPassword));
                return BadRequest(ModelState);
            }

            ApplicationUser user = await userManager.FindByIdAsync(model.UserId).ConfigureAwait(false);
            user.ForcedPasswordChange = false;
            var token = WebUtility.UrlDecode(model.Token);
            if (user != null)
            {   
                var result = await userManager.ResetPasswordAsync(user, token, model.NewPassword);
                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
            }

            return Ok();
        }

        /// <summary>
        /// Set new password 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("SetUserPassword")]
        public async Task<IActionResult> SetUserPassword(SetPasswordBindingModel model)
        {
            if (string.IsNullOrEmpty(model.NewPassword))
            {
                ModelState.AddModelError("", "Password missing");
                return BadRequest(ModelState);
            }

            if (!IsPasswordValid(model.NewPassword))
            {
                ModelState.AddModelError("Password", PasswordRequirementMessage(model.NewPassword));
                return BadRequest(ModelState);
            }

            applicationUser.ForcedPasswordChange = false;
            applicationUser.PasswordHash = userManager.PasswordHasher.HashPassword(applicationUser, model.NewPassword);
            var result = await userManager.UpdateAsync(applicationUser).ConfigureAwait(true);
            

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        /// <summary>
        /// Confirm new user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("ConfirmEmail")]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                ModelState.AddModelError("ConfirmEmail","UserId / Code missing");
                return BadRequest(ModelState);
            }
            IdentityResult result;
            try
            {
                applicationUser = userManager.FindByIdAsync(userId).Result;
                if (applicationUser == null) //TODO - Need page to display message of user does not exists.
                    return Redirect(string.Format("{0}{1}", configuration["WebAppUrl:Url"], configuration["WebAppUrl:NoUserExists"]));
                result = await userManager.ConfirmEmailAsync(applicationUser, code);
            }
            catch (InvalidOperationException ioe)
            {
                // ConfirmEmailAsync throws when the userId is not found.
                return ioe.GetActionResult();
            }

            if (result.Succeeded)
            {
                //
                //emailverification 
                var emailVerification = emailVerificationRepository.Find(null, p => p.PersonId == applicationUser.PersonId && p.IsVerified != true)?.Items?.FirstOrDefault();
                if (emailVerification != null)
                {
                    var verifiedEmailAddress = personEmailRepository.Find(null, p => p.Address.Equals(emailVerification.Address, StringComparison.OrdinalIgnoreCase))?.Items?.FirstOrDefault();
                    if (verifiedEmailAddress == null)
                    {
                        var personEmail = new PersonEmail()
                        {
                            EmailVerificationId = emailVerification.Id,
                            IsPrimaryEmail = true,
                            PersonId = emailVerification.PersonId,
                            Address = emailVerification.Address
                        };
                        personEmailRepository.Add(personEmail);
                    }

                    //Verification completed
                    emailVerification.IsVerified = true;
                    emailVerificationRepository.Update(emailVerification);
                }
                
                return Redirect(string.Format("{0}{1}", configuration["WebAppUrl:Url"], configuration["WebAppUrl:login"]));
            }

            // If we got this far, something failed.
            return Redirect(string.Format("{0}{1}", configuration["WebAppUrl:Url"], configuration["WebAppUrl:tokenerror"]));
        }

        /// <summary>
        /// forgot password using email address
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            ApplicationUser user = await userManager.FindByEmailAsync(model.Email).ConfigureAwait(false);
            if (user != null)
            {
                string code = await userManager.GeneratePasswordResetTokenAsync(user).ConfigureAwait(false);
                string emailBody = SendForgotPasswordEmail(code, user.Id, "en");
                var subject = string.Format("Reset your password at {0}", Constants.PRODUCT);
                await emailSender.SendEmailAsync(user.Email, subject, emailBody).ConfigureAwait(false);
            }
            else
            {
                ModelState.AddModelError("Email", "Email address does not exist.");
                return BadRequest(ModelState);
            }
            return Ok();
        }

        /// <summary>
        /// Get user info for logged authenticated user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUserInfo")]
        public async Task<IActionResult> GetUserInfo()
        {
            if (applicationUser == null)
            {
                return Unauthorized();
            }


            string startsWith = "";
            int skip = 0;
            int take = 100;
            var personOrgs = membershipManager.Search(applicationUser.PersonId, startsWith, skip, take);
            var pendingAcessOrgs = membershipManager.PendingOrganizationAccess(applicationUser.PersonId);
            bool isUserConsentRequired = VerifyUserAgreementConsentStatus(applicationUser.PersonId);
            var authenticatedUser = new
            {
                personId = applicationUser.PersonId,
                email = applicationUser.Email,
                userName = applicationUser.UserName,
                token = Request.Headers["Authorization"].ToString().Replace("bearer ", "", StringComparison.OrdinalIgnoreCase),
                applicationUser.ForcedPasswordChange,
                isUserConsentRequired,
                IsJoinOrgRequestPending = (pendingAcessOrgs?.Items?.Count > 0)? true : false,
                myOrganizations = personOrgs?.Items
            };

            return Ok(authenticatedUser);
        }

        private bool VerifyUserAgreementConsentStatus(Guid personId)
        {
            bool isUserConsentRequired = false;
            UserAgreement userAgreement = termsConditionsManager.GetUserAgreement().Result;
            if (userAgreement != null)
            {
                isUserConsentRequired = termsConditionsManager.IsAccepted(userAgreement.Id.GetValueOrDefault(), personId).Result;
            }
            return !isUserConsentRequired;
        }

        /// <summary>
        /// Resend confirmation mail to registered email address 
        /// </summary>
        /// <param name="emailAddress">Email address needed for confirmation</param>
        /// <returns></returns>
        [HttpPut]
        [Route("ResendEmailConfirmation")]
        public async Task<IActionResult> ResendEmailConfirmation(string emailAddress) {

            if (applicationUser == null)
                return Unauthorized();
            try
            {
                //TODO - check email address is mapped to the logged in user

                //Resending 
                byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
                byte[] key = applicationUser.PersonId.ToByteArray();
                string token = Convert.ToBase64String(time.Concat(key).ToArray());

                string confirmationLink = Url.Action("ConfirmEmailAddress",
                                                    "Auth", new
                                                    {
                                                        emailAddress = emailAddress,
                                                        token = token
                                                    }, protocol: HttpContext.Request.Scheme);

                string emailBody = "";

                using (StreamReader reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Email/confirm-en.html")))
                {
                    emailBody = reader.ReadToEnd();
                    emailBody = emailBody.Replace("^Confirm^", confirmationLink);
                }


                var subject = "Confirm your email address at " + Constants.PRODUCT;
                await emailSender.SendEmailAsync(emailAddress, subject, emailBody).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
            return Ok();
        }

        /// <summary>
        /// Confirm email address
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("ConfirmEmailAddress")]
        public async Task<IActionResult> ConfirmEmailAddress(string emailAddress, string token)
        {
            //To decode the token to get the creation time, person Id:

            byte[] data = Convert.FromBase64String(token);
            byte[] _time = data.Take(8).ToArray();
            byte[] _key = data.Skip(8).ToArray();

            DateTime when = DateTime.FromBinary(BitConverter.ToInt64(_time, 0));
            Guid personId = new Guid(_key);
            if (when < DateTime.UtcNow.AddHours(-24))
            {
                //TODO - To route the page to display error 
                return Redirect(string.Format("{0}{1}", configuration["WebAppUrl:Url"], configuration["WebAppUrl:tokenerror"]));
            }
            //
            //emailverification 
            var emailVerification = emailVerificationRepository.Find(null, p => p.PersonId == personId && p.Address.Equals(emailAddress, StringComparison.OrdinalIgnoreCase) && p.IsVerified != true)?.Items?.FirstOrDefault();
            if (emailVerification != null)
            {
                var verifiedEmailAddress = personEmailRepository.Find(null, p => p.Address.Equals(emailVerification.Address, StringComparison.OrdinalIgnoreCase))?.Items?.FirstOrDefault();
                if (verifiedEmailAddress == null)
                {
                    var personEmail = new PersonEmail()
                    {
                        EmailVerificationId = emailVerification.Id,
                        IsPrimaryEmail = false,
                        PersonId = emailVerification.PersonId,
                        Address = emailVerification.Address
                    };
                    personEmailRepository.Add(personEmail);
                }

                //Verification completed
                emailVerification.IsVerified = true;
                emailVerificationRepository.Update(emailVerification);
            }
            return Redirect(string.Format("{0}{1}", configuration["WebAppUrl:Url"], configuration["WebAppUrl:emailaddressconfirmed"]));

        }

        /// <summary>
        /// Used to refresh expired access and old refresh token
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("Refresh")]
        public async Task<IActionResult> Refresh(RefreshModel model)
        {
            var principal = GetPrincipalFromExpiredToken(model.Token);
            var username = principal.Identity.Name;
            var savedRefreshToken = await GetRefreshToken(username, model.RefreshToken); //retrieve the refresh token from [AspNetUserTokens] table

            if (savedRefreshToken != model.RefreshToken)
            {
                await signInManager.SignOutAsync().ConfigureAwait(true);
                ModelState.AddModelError("Invalid Token", "Token is no longer valid. Please log back in.");
                return BadRequest(ModelState);
            }

            var newJwtToken = GenerateAccessToken(principal.Claims);
            var newRefreshToken = GenerateRefreshToken();
            await DeleteRefreshToken(username, model.RefreshToken).ConfigureAwait(true);
            await SaveRefreshToken(username, newRefreshToken).ConfigureAwait(true);

            return new ObjectResult(new
            {
                jwt = newJwtToken,
                refreshToken = newRefreshToken
            });
        }

        private async Task<string> GetRefreshToken(string username, string refreshToken)
        {
            var user = await userManager.FindByNameAsync(username).ConfigureAwait(true);
            var token = await userManager.GetAuthenticationTokenAsync(user, userManager.Options.Tokens.AuthenticatorTokenProvider, "refresh").ConfigureAwait(true);

            return token;
        }

        private async Task DeleteRefreshToken(string username, string refreshToken)
        {
            var user = await userManager.FindByNameAsync(username).ConfigureAwait(true);
            await userManager.RemoveAuthenticationTokenAsync(user, userManager.Options.Tokens.AuthenticatorTokenProvider, "refresh").ConfigureAwait(true);
        }

        private async Task SaveRefreshToken(string username, string newRefreshToken)
        {
            var user = await userManager.FindByNameAsync(username).ConfigureAwait(true);
            await userManager.SetAuthenticationTokenAsync(user, userManager.Options.Tokens.AuthenticatorTokenProvider, "refresh", newRefreshToken).ConfigureAwait(true);
        }

        #region - Private Methods

        private String GetToken(IdentityUser user)
        {
            var utcNow = DateTime.UtcNow;
            var claims = new Claim[]
            {
                        new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                        new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName)
            };

            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration.GetValue<String>("Tokens:Key")));
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            var jwt = new JwtSecurityToken(
                    signingCredentials: signingCredentials,
                    claims: claims,
                    notBefore: utcNow,
                    expires: utcNow.AddSeconds(this.configuration.GetValue<int>("Tokens:Lifetime")),
                    audience: configuration.GetValue<String>("Tokens:Audience"),
                    issuer: configuration.GetValue<String>("Tokens:Issuer")
                );

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        private string GenerateAccessToken(IEnumerable<Claim> claims)
        {
            var utcNow = DateTime.UtcNow;
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration.GetValue<String>("Tokens:Key")));
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

            var jwt = new JwtSecurityToken(
                    signingCredentials: signingCredentials,
                    claims: claims,
                    notBefore: utcNow,
                    expires: utcNow.AddSeconds(this.configuration.GetValue<int>("Tokens:Lifetime")),
                    audience: configuration.GetValue<String>("Tokens:Audience"),
                    issuer: configuration.GetValue<String>("Tokens:Issuer")
            );

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        private string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Tokens:Key"])),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }

        private IActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (IdentityError error in result.Errors)
                    {
                        if (error.Code.Equals("InvalidToken", StringComparison.OrdinalIgnoreCase)) {
                            ModelState.AddModelError("Invalid Email Token", "Email link expired or has been used");
                        } else ModelState.AddModelError("Signup", error.Description);
                    }
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private string SendConfirmationEmail(string code, string userId, string password, string language)
        {
            if (language == "en")
            {

                string confirmationLink = Url.Action("ConfirmEmail",
                                                 "Auth", new
                                                 {
                                                     userid = userId,
                                                     code = code
                                                 }, protocol: HttpContext.Request.Scheme);

                string emailBody = "";

                using (StreamReader reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Email/accountCreation.html")))
                {
                    emailBody = reader.ReadToEnd();
                    emailBody = emailBody.Replace("^Password^", password);
                    emailBody = emailBody.Replace("^Confirm^", confirmationLink);
                }

                return emailBody;
            }

            return "";

        }

        private string SendForgotPasswordEmail(string code, string userId, string language)
        {
            string confirmationLink = Url.Action("VerifyUserToken",
                                               "Auth", new
                                               {
                                                   userid = userId,
                                                   code = code
                                               }, protocol: HttpContext.Request.Scheme);

            //changed the action link because frontend team is handling the scenario and redirecting it to SetPassword api
            //string baseUrl = string.Format(@"{0}{1}", configuration["WebAppUrl:Url"], "/auth/forgot-reset-password");
            //var callbackUrl = string.Format(@"{0}?userid={1}&token={2}", baseUrl, WebUtility.UrlEncode(userId), WebUtility.UrlEncode(code));

            //string baseUrl = string.Format(@"{0}{1}", configuration["WebAppUrl:Url"], configuration["WebAppUrl:forgotpassword"]);
            if (language == "en")
            {
                StreamReader reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Email/forgotPassword-en.html"));
                string emailBody = reader.ReadToEnd();

                //var callbackUrl = string.Format(@"{0}?userid={1}&token={2}", baseUrl, WebUtility.UrlEncode(userId), WebUtility.UrlEncode(code));
                emailBody = emailBody.Replace("^resetpassword^", confirmationLink);
                return emailBody;
            }

            return null;
        }

        private bool IsPasswordValid(string password)
        {
            bool validPassword = true;
            var passwordPolicy = passwordPolicyRepository.Find(0, 0)?.Items?.FirstOrDefault();
            if (passwordPolicy != null)
            {
                PasswordOptions passwordOptions = new PasswordOptions() { 
                    RequiredLength = (int)passwordPolicy.MinimumLength,
                    RequireLowercase = (bool)passwordPolicy.RequireAtleastOneLowercase,
                    RequireNonAlphanumeric = (bool)passwordPolicy.RequireAtleastOneNonAlpha,
                    RequireUppercase = (bool)passwordPolicy.RequireAtleastOneUppercase,
                    RequiredUniqueChars = 0,
                    RequireDigit=(bool)passwordPolicy.RequireAtleastOneNumber
                };
                
                validPassword  = PasswordManager.IsValidPassword(password, passwordOptions);
            }
            return validPassword;
        }

        private string PasswordRequirementMessage(string password)
        {
            
            var passwordPolicy = passwordPolicyRepository.Find(0, 0)?.Items?.FirstOrDefault();
            if (passwordPolicy != null)
            {
                PasswordOptions passwordOptions = new PasswordOptions()
                {
                    RequiredLength = (int)passwordPolicy.MinimumLength,
                    RequireLowercase = (bool)passwordPolicy.RequireAtleastOneLowercase,
                    RequireNonAlphanumeric = (bool)passwordPolicy.RequireAtleastOneNonAlpha,
                    RequireUppercase = (bool)passwordPolicy.RequireAtleastOneUppercase,
                    RequiredUniqueChars = 0,
                    RequireDigit = (bool)passwordPolicy.RequireAtleastOneNumber
                };

                return PasswordManager.PasswordRequirementMessage(password, passwordOptions);
            }
            return string.Empty;
        }

        #endregion
    }

}
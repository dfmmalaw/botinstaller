﻿using System;
using System.Threading.Tasks;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Model.Membership;
using OpenBots.Server.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using OpenBots.Server.Security;
using Microsoft.AspNetCore.Authorization;
using System.Linq;

namespace OpenBots.Server.WebAPI.Controllers
{
    [Route("api/v1/Organizations/{organizationId}/[controller]")]
    [ApiController]
    [Authorize]
    public class AccessRequestsController : EntityController<AccessRequest>
    {
        IMembershipManager _manager;
        IAccessRequestsManager accessRequestManager;
        public AccessRequestsController(
            IAccessRequestRepository repository,
            IMembershipManager manager,
            IAccessRequestsManager accessRequestManager,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor) : base(repository, userManager, httpContextAccessor, manager)
        {
            _manager = manager;
            this.accessRequestManager = accessRequestManager;

            _manager.SetContext(base.SecurityContext); ;
            this.accessRequestManager.SetContext(base.SecurityContext);

        }

        /// <summary>
        /// Provides all the access requests for the given Organization Id.
        /// </summary>
        /// <param name="organizationId">Organization Identifier</param>
        /// <param name="filter">search using a field</param>
        /// <param name="orderBy">sort ascending or  descending</param>
        /// <param name="top">fetches the first 'n' number of rows</param>
        /// <param name="skip">skips the 'n' number of rows</param>
        /// <response code="200">Ok, If all the access requests for the Organization Id has been returned.</response>
        /// <response code="400">BadRequest,If the Organization ID is not provided or improper Guid. </response>
        /// <response code="403">Forbidden,Unauthorized access by user</response>
        /// <response code="404">NotFound, No access request exists for the given Organization Id.</response>
        /// <response code="422">Unprocessable entity,Validation error</response>
        /// <returns>Paginated List of all Access requests for the Organization Id</returns>
        [HttpGet]
        [ProducesResponseType(typeof(AccessRequest), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public PaginatedList<AccessRequest> Get(
            [FromRoute] string organizationId,
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0)
        {
            return accessRequestManager.GetAccessRequests(organizationId);//base.GetMany(organizationId);
        }

        /// <summary>
        /// Pending Access Requests
        /// </summary>
        /// <remarks>Provides the paginated pending access requests for the organization.</remarks>
        /// <param name="organizationId">The organization identifier.</param>
        /// <response code="200">OK,Paginated list of all access requests with Id and name of the user</response>
        /// <response code="400">BadRequest,If Organization Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access</response>
        /// <response code="422">Unprocessable entity,Validation error</response>
        /// <returns>A List of Pending Access Requests with ID and Name of the user.</returns>
        [HttpGet("Pending")]
        [ProducesResponseType(typeof(PaginatedList<PendingAccessRequest>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public async Task<IActionResult> GetPending(string organizationId)
        {
            Guid orgGuid = Guid.Parse(organizationId);
            try
            {
                var result = _manager.Pending(orgGuid);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Get the access requests for a particular organization.
        /// </summary>
        /// <remarks></remarks>
        /// <param name="organizationId"></param>
        /// <param name="id">Access Request Identifier.</param>
        /// <response code="200">Ok,If access request exists for the given Id</response>
        /// <response code="304">NotModified</response>
        /// <response code="400">BadRequest, if the Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access</response>
        /// <response code="404">NotFound, AccessRequest with the particular id does not exist.</response>
        /// <response code="422">Unprocessable Entity, Validation Error</response>
        /// <returns>Ok response, if access request exists for the given id</returns>
        [HttpGet("{id}", Name = "GetAccessRequest")]
        [ProducesResponseType(typeof(AccessRequest), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public async Task<IActionResult> Get(string organizationId, string id)
        {
            return await base.GetEntity(id);
        }

        /// <summary>
        /// Adds a new access request for the Organization
        /// </summary>
        /// <param name="organizationId">Organization Identifier</param>
        /// <param name="value">Access Request details</param>
        /// <response code="200">Ok,If the access request has been created successfully.</response>
        /// <response code="400">BadRequest,If the Organization Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access</response>
        /// <response code="409">Conflict,When access request with the particular id already exists.</response>
        /// <response code="422">UnprocessableEntity,validation error or cannot insert duplicate constraint.</response>
        /// <returns>Ok response,Creates a new access request with a new access request id.</returns>
        [HttpPost]
        [ProducesResponseType(typeof(AccessRequest), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public async Task<IActionResult> Post(string organizationId, [FromBody] AccessRequest value)
        {
            value.OrganizationId = new Guid(organizationId);
            return await base.PostEntity(value);
        }

        /// <summary>
        /// Update the access request 
        /// </summary>
        /// <remarks>Updates the access request with the particular id ,for the given organization </remarks>
        /// <param name="organizationId">Organization identifier</param>
        /// <param name="id">access request Id</param>
        /// <param name="value">new value of the access request to be updated .</param>
        /// <response code="200">OK,If the update of the access request for the particular id has been successful.</response>
        /// <response code="400">BadRequest,If the Id is not provided or Guid is not in proper format.</response>
        /// <response code="403">Forbidden,Unauthorized access by the user</response>
        /// <response code="404">NotFound,If no access request exists for the given id.</response>
        /// <response code="409">Conflict</response>
        /// <response code="422">Unprocessable Entity,Validation error.</response>
        /// <returns>Ok response , If the access request has been updated.</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public async Task<IActionResult> Put(string organizationId, string id, [FromBody] AccessRequest value)
        {
            value.OrganizationId = new Guid(organizationId);
            return await base.PutEntity(id, value);
        }

        /// <summary>
        /// Approves the specified AccessRequest by an Organization Administrator.
        /// </summary>
        /// <param name="organizationId">The organization identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <response code="200">OK,If the access request is approved.</response>
        /// <response code="400">BadRequest,if the Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,if the user doesnt have permission to approve access request.</response>
        /// <response code="422">Unprocessable Entity,Validation error.</response> 
        /// <returns>OK response if the approval is successful</returns> 
        [HttpPut("{id}/Approve")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Approve(string organizationId, string id)
        {
            if ( string.IsNullOrEmpty(id) || string.IsNullOrEmpty(organizationId))
            {
                ModelState.AddModelError("Approve", "Access or Organization Id not passed");
                return BadRequest(ModelState);
            }
            Guid entityId = new Guid(id);

            var orgmem = _manager.GetOrganizationMember(Guid.Parse(organizationId), SecurityContext.PersonId)?.Items?.FirstOrDefault();
            if (orgmem == null || (orgmem != null && orgmem.IsAdministrator == null) || (orgmem != null && orgmem.IsAdministrator.HasValue && orgmem.IsAdministrator == false))
            {
                ModelState.AddModelError("Approve", "Approve failed, administrator of an organization can only Approve");
                return BadRequest(ModelState);
            }

            Guid approvalRequestGuid = Guid.Parse(id);
            try
            {
                _manager.ApproveAccessRequest(approvalRequestGuid, SecurityContext);
                return Ok();

            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Rejects the specified AccessRequest by an Organization Administrator.
        /// </summary>
        /// <param name="organizationId">The organization identifier.</param>
        /// <param name="id">The Access Request identifier.</param>
        /// <response code="200">Ok,If the acccess Request is rejected.</response>
        /// <response code="400">BadRequest,If the Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,if the user doesnt have permission to reject access request</response>
        /// <response code="422">Unprocessable Entity,Validation error.</response>
        /// <returns>OK response if the access is rejected .</returns>
        [HttpPut("{id}/Reject")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Reject(string organizationId, string id)
        {
            if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(organizationId))
            {
                ModelState.AddModelError("Approve", "Access or Organization Id not passed");
                return BadRequest(ModelState);
            }
            Guid entityId = new Guid(id);

            var orgmem = _manager.GetOrganizationMember(Guid.Parse(organizationId), SecurityContext.PersonId)?.Items?.FirstOrDefault();
            if (orgmem == null || (orgmem != null && orgmem.IsAdministrator == null) || (orgmem != null && orgmem.IsAdministrator.HasValue && orgmem.IsAdministrator == false))
            {
                ModelState.AddModelError("Approve", "Approve failed, administrator of an organization can only Approve");
                return BadRequest(ModelState);
            }

            Guid approvalRequestGuid = Guid.Parse(id);

            try
            {
                _manager.RejectAccessRequest(approvalRequestGuid, SecurityContext);
                return Ok();

            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Deletes access request 
        /// </summary>
        /// <param name="id">Access request identifier.</param>
        /// <response code="200">OK, if the access request with the given id has been deleted.</response>
        /// <response code="400">BadRequest,if the id is not provided or not proper Guid.</response>
        /// <response code="403">Unauthorized access, if the user doesn't have permission to delete the access request.</response>
        /// <response code="422">Unprocessable Entity,Validation error.</response>
        /// <returns>OK response, if the access request with the given id has been deleted.</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public async Task<IActionResult> Delete(string id)
        {
            return await base.DeleteEntity(id);
        }


        /// <summary>
        /// Updates partial details of Access request.
        /// </summary>
        /// <param name="id">Access request identifier</param>
        /// <param name="value">details of Access request to be updated.</param>
        /// <response code="200">OK,If update of access requestis successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Access request details has been updated</returns>
        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id, [FromBody]JsonPatchDocument<AccessRequest> value)
        {
            return await base.PatchEntity(id, value);
        }
    }
}
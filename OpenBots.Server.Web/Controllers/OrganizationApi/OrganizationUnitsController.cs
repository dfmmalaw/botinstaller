﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Model.Membership;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using OpenBots.Server.Business;
using OpenBots.Server.Security;
using Microsoft.AspNetCore.Authorization;

namespace OpenBots.Server.WebAPI.Controllers
{
    
    [Route("api/v1/Organizations/{organizationId}/[controller]")]
    [ApiController]
    [Authorize]
    public class OrganizationUnitsController : EntityController<OrganizationUnit>
    {
        IMembershipManager membershipManager;
        
        public OrganizationUnitsController(
            IOrganizationUnitRepository repository,
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor) : base(repository,  userManager, httpContextAccessor, membershipManager)
        {
            this.membershipManager = membershipManager;
        


            this.membershipManager.SetContext(base.SecurityContext); ;
        

        }

        /// <summary>
        /// Gets all Organization units details(departments ) that are part of an Organization Id.
        /// </summary>
        /// <param name="organizationId">Organization identifier</param>
        /// <param name="filter">search using a field</param>
        /// <param name="orderBy">sort ascending or  descending</param>
        /// <param name="top">fetches the first 100 rows</param>
        /// <param name="skip">skips the 'n' number of rows</param>
        /// <response code="200">OK, Paginated list of the Organization Units bleongign to a particular organization</response>
        /// <response code="400">BadRequest,If the Organization ID is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,unauthorized access by user</response>
        /// <response code="404">NotFound</response>
        /// <response code="422">Unprocessable entity,validation error.</response>
        /// <returns>Paginated list of all the Organization Units for a particulatular Organization ID.</returns>
        [HttpGet]
        [ProducesResponseType(typeof(PaginatedList<OrganizationUnit>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<OrganizationUnit> Get(
            [FromRoute] string organizationId,
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0)
        {
            var departments = membershipManager.GetDepartments(organizationId);

            return departments;
        }

        /// <summary>
        /// Provides the Organization unit details for a particular id
        /// </summary>
        /// <param name="organizationId">organization identifier</param>
        /// <param name="id">Organization unit identifier</param>
        /// <response code="200">OK,If Organization unit detail is available with the given id .></response>
        /// <response code="400">BadRequest, If the Organization Id/Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access by user.</response>
        /// <response code="404">NotFound</response>
        /// <response code="422">Unprocessable entity,validation error.</response>
        /// <returns>Ok response, If an organization unit has been found with the particular id.</returns>
        [HttpGet("{id}", Name = "GetOrganizationUnit")]
        [ProducesResponseType(typeof(OrganizationUnit), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string organizationId, string id)
        {
            var departments = await base.GetEntity(id);

            return departments;
        }

        /// <summary>
        /// Adds a Organization unit to  an Organization
        /// </summary>
        /// <param name="organizationId">Organization identifier</param>
        /// <param name="value">.value of organization unit to be added</param>
        /// <response code="200">OK,If the unit has been added successfully to an organization</response>
        /// <response code="400">BadRequest,If the Organization Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access by user.</response>
        /// <response code="409">Conflict,concurrency error </response>
        /// <response code="422">UnprocessableEntity,If a duplicate record is being entered.</response> 
        /// <returns>Ok response,If the organization unit has been added to an Organization </returns>
        [HttpPost]
        [ProducesResponseType(typeof(OrganizationUnit), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public  async Task<IActionResult> Post(string organizationId, [FromBody] OrganizationUnit value)
        {
         
            if (value == null && string.IsNullOrEmpty(organizationId))
            {
                ModelState.AddModelError("Add", "Organization Id not passed");
                return BadRequest(ModelState);
            }
            Guid entityId = new Guid(organizationId);

            if (value == null || value.OrganizationId == null || !value.OrganizationId.HasValue || entityId != value.OrganizationId.Value)
            {
                ModelState.AddModelError("Update", "Organization IDs don't match");
                return BadRequest(ModelState);
            }

            var orgmem = membershipManager.GetOrganizationMember(Guid.Parse(organizationId), SecurityContext.PersonId)?.Items?.FirstOrDefault();
            if (orgmem == null || (orgmem != null && orgmem.IsAdministrator == null) || (orgmem != null && orgmem.IsAdministrator.HasValue && orgmem.IsAdministrator == false))
            {
                ModelState.AddModelError("Add", "Add department failed, administrator of an organization can only add departments");
                return BadRequest(ModelState);
            }

            var orgUnit = repository.Find(null, d => d.Name.ToLower(null) == value.Name.ToLower(null) && d.OrganizationId == value.OrganizationId)?.Items?.FirstOrDefault();
            if (orgUnit != null)
            {
                ModelState.AddModelError("Add", "Department Name already exists, cannot add duplicate");
                return BadRequest(ModelState);
            }

            value.OrganizationId = new Guid(organizationId);
            return await base.PostEntity(value);
        }

        /// <summary>
        /// Updates a member of an Organization unit
        /// </summary>
        /// <param name="organizationId">Organization identifier</param>
        /// <param name="id">ORganization unit identifier</param>
        /// <param name="value">value of the organization unit to be updated</param>
        /// <response code="200">OK, If the value has been updated for the Organization unit.</response>
        /// <response code="400">BadRequest,If the Organization Id/unit Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access</response>
        /// <response code="409">Conflict,concurrency error.</response>
        /// <response code="422">UnprocessableEntity,Validation error.</response>
        /// <returns>OK response,If the ORganization unit has been updated with the new value.</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string organizationId, string id, [FromBody] OrganizationUnit value)
        {
            if (value == null && string.IsNullOrEmpty(organizationId))
            {
                ModelState.AddModelError("Update", "Organization details not passed");
                return BadRequest(ModelState);
            }
            Guid entityId = new Guid(organizationId);

            if (value == null || value.OrganizationId == null || !value.OrganizationId.HasValue || entityId != value.OrganizationId.Value)
            {
                ModelState.AddModelError("Update", "Organization IDs don't match");
                return BadRequest(ModelState);
            }

            var orgmem = membershipManager.GetOrganizationMember(Guid.Parse(organizationId), SecurityContext.PersonId)?.Items?.FirstOrDefault();
            if (orgmem == null || (orgmem != null && orgmem.IsAdministrator == null) || (orgmem != null && orgmem.IsAdministrator.HasValue && orgmem.IsAdministrator == false))
            {
                ModelState.AddModelError("Update", "Update department failed, administrator of an organization can only update departments");
                return BadRequest(ModelState);
            }

            value.OrganizationId = new Guid(organizationId);
            return await base.PutEntity(id, value);
        }

        /// <summary>
        /// Deletes an organization unit 
        /// </summary>
        /// <param name="id">Organization unit identifier</param>
        /// <response code="200">OK, if the Organization member with the given id has been deleted for an Organization unit</response>
        /// <response code="400">BadRequest,if the id is not provided or not proper Guid.</response>
        /// <response code="403">Unauthorized access, if the user doesn't have permission to delete the Organization member.</response>
        /// <response code="422">Unprocessable Entity,Validation error.</response>
        /// <returns>OK response, if the Organization unit with the given id has been deleted.</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public  async Task<IActionResult> Delete(string organizationId, string id)
        {
            if (string.IsNullOrEmpty(organizationId) || string.IsNullOrEmpty(id))
            {
                ModelState.AddModelError("Update", "Organization Id / Department Id not passed");
                return BadRequest(ModelState);
            }
            Guid entityId = new Guid(id);
            var orgmem = membershipManager.GetOrganizationMember(Guid.Parse(organizationId), SecurityContext.PersonId)?.Items?.FirstOrDefault();
            if (orgmem == null || (orgmem != null && orgmem.IsAdministrator == null) || (orgmem != null && orgmem.IsAdministrator.HasValue && orgmem.IsAdministrator == false))
            {
                ModelState.AddModelError("Delete", "Delete department failed, administrator of an organization can only delete departments");
                return BadRequest(ModelState);
            }

            var orgUnit = repository.Find(null, d => d.Id == entityId && d.CanDelete == false)?.Items?.FirstOrDefault();
            if (orgUnit != null)
            {
                ModelState.AddModelError("Delete", "Department cannot be deleted ");
                return BadRequest(ModelState);
            }

            return await base.DeleteEntity(id);
        }

        /// <summary>
        /// Updates the partial details of Organization Unit.
        /// </summary>
        /// <param name="id">Organization Unit identifier</param>
        /// <param name="value">details of the Organization unit to be updated</param>
        /// <response code="200">OK,If update of Organization unit is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Organization unit details has been updated</returns>
        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string organizationId, string id,[FromBody] JsonPatchDocument<OrganizationUnit> value)
        {
            if (value == null || string.IsNullOrEmpty(id))
            {
                ModelState.AddModelError("Update", "Organization Department Id not passed");
                return BadRequest(ModelState);
            }

            var orgmem = membershipManager.GetOrganizationMember(Guid.Parse(organizationId), SecurityContext.PersonId)?.Items?.FirstOrDefault();
            if (orgmem == null || (orgmem != null && orgmem.IsAdministrator == null) || (orgmem != null && orgmem.IsAdministrator.HasValue && orgmem.IsAdministrator == false))
            {
                ModelState.AddModelError("Update", "Update failed, administrator of an organization can only update");
                return BadRequest(ModelState);
            }

            return await base.PatchEntity(id, value);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Model.Membership;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using OpenBots.Server.Security;
using OpenBots.Server.Business;
using Microsoft.AspNetCore.Authorization;

namespace OpenBots.Server.WebAPI.Controllers
{
    [Route("api/v1/Organizations/{organizationId}/[controller]")]
    [ApiController]
    [Authorize]
    public class OrganizationSettingsController : EntityController<OrganizationSetting>
    {
        public OrganizationSettingsController(
            IOrganizationSettingRepository repository, 
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor) : base(repository, userManager, httpContextAccessor, membershipManager)
        {
        }


        /// <summary>
        /// Gets all the Business process and their priorities and status for an Organization
        /// </summary>
        /// <param name="organizationId">Organization identifier</param>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="top"></param>
        /// <response code="200">OK,provides a paginated list of the business process priority and status. </response>
        /// <response code="400">BadRequest,If the organization Id is not provided or improper guid. </response>
        /// <response code="403">Forbidden,Unauthorized access by the user</response>
        /// <response code="404">NotFound</response>
        /// <response code="422">Unprocessable entity,validation error.</response>
        /// <param name="skip"></param>
        /// <returns>Paginated List of all the Business process status and priority for an organization</returns>
        [HttpGet]
        [ProducesResponseType(typeof(OrganizationSetting), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<OrganizationSetting> Get(
            [FromRoute] string organizationId,
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0
            )
        {
            return base.GetMany(organizationId);
        }

        /// <summary>
        /// Gets a particular  Business process for an organization.
        /// </summary>
        /// <param name="organizationId">Organization identifier</param>
        /// <param name="id">Organization Setting identifier</param>
        /// <response code="200">OK,Provides business process status and priority for Business process with the id and belongs to OrganizationId.></response>
        /// <response code="400">BadRequest, If the Organization Id/Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access by user.</response>
        /// <response code="404">NotFound</response>
        /// <response code="422">Unprocessable entity,validation error.</response>
        /// <returns>OK response,If the Business process with a specific id for an Organization has been returned</returns>
        [HttpGet("{id}", Name = "GetOrganizationSetting")]
        [ProducesResponseType(typeof(OrganizationSetting), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string organizationId, string id)
        {
            return await base.GetEntity(id);
        }

        /// <summary>
        /// Adds a Business process for the Organization 
        /// </summary>
        /// <param name="organizationId">Organization Identifier</param>
        /// <param name="value">details of the Business process, Status and priority</param>
        /// <response code="200">OK,If the new process has been added successfully.</response>
        /// <response code="400">BadRequest,If the Organization Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access by user.</response>
        /// <response code="409">Conflict,concurrency error </response>
        /// <response code="422">UnprocessableEntity,If a duplicate record is being entered.</response>
        /// <returns>OK response,If a Business process with a unique identifer has been added for an Organization.</returns>
        [HttpPost]
        [ProducesResponseType(typeof(OrganizationSetting), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post(string organizationId, [FromBody] OrganizationSetting value)
        {
            value.OrganizationId = new Guid(organizationId);
            return await base.PostEntity(value);
        }

        /// <summary>
        /// Updates the process for an Organization
        /// </summary>
        /// <param name="organizationId">Organization IDentifier</param>
        /// <param name="id">Organization Setting Identifier </param>
        /// <param name="value">Value to be updated</param>
        /// <response code="200">OK, If the value has been updated for the Organization setting for an Organization.</response>
        /// <response code="400">BadRequest,If the Organization Id/Setting Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access</response>
        /// <response code="409">Conflict,concurrency error.</response>
        /// <response code="422">UnprocessableEntity,Validation error.</response>
        /// <returns>OK response,If the ORganization setting has been updated with the new value.</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string organizationId, string id, [FromBody] OrganizationSetting value)
        {
            value.OrganizationId = new Guid(organizationId);
            return await base.PutEntity(id, value);
        }

        /// <summary>
        /// Deletes setting with the specific id
        /// </summary>
        /// <param name="id">Organization setting identifier</param>
        /// <response code="200">OK,If the setting has been deleted.</response>
        /// <response code="400">BadRequest,If the Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access</response>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(string id)
        {
            return await base.DeleteEntity(id);
        }


        /// <summary>
        ///Partial Updates the Organization settings 
        /// </summary>
        /// <param name="id">Organization setting identifier. </param>
        /// <param name="value">Values to be updated , Org ID/Business process key prefix/BP prioritiescsv/BP status CSV</param>
        /// <response code="200">OK,If update of Organization setting is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Organization settings values has been updated</returns>
        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id, [FromBody] JsonPatchDocument<OrganizationSetting> value)
        {
            return await base.PatchEntity(id, value);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Model.Membership;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using OpenBots.Server.Business;
using OpenBots.Server.Security;
using Microsoft.AspNetCore.Authorization;

namespace OpenBots.Server.WebAPI.Controllers
{
    [Route("api/v1/Organizations/{organizationId}/[controller]")]
    [ApiController]
    [Authorize]
    public class OrganizationUnitMembersController : EntityController<OrganizationUnitMember>
    {
        public OrganizationUnitMembersController(
            IOrganizationUnitMemberRepository repository, 
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor) : base(repository,  userManager, httpContextAccessor, membershipManager)
        {
        }


        /// <summary>
        /// Gets all the members of an Organization Unit (department)
        /// </summary>
        /// <param name="organizationId">organization identifier</param>
        /// <param name="filter">search using a field</param>
        /// <param name="orderBy">sort ascending or  descending</param>
        /// <param name="top">fetches the first 100 rows</param>
        /// <param name="skip">skips the 'n' number of rows</param>
        /// <response code="200">OK, Paginated list of the members of an Organization Unit</response>
        /// <response code="400">BadRequest,If the Organization ID is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,unauthorized access by user</response>
        /// <response code="404">NotFound</response>
        /// <response code="422">Unprocessable entity,validation error.</response>
        /// <returns>Paginated list of all the members of the Organization Unit.</returns>
        [HttpGet]
        [ProducesResponseType(typeof(OrganizationUnitMember), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<OrganizationUnitMember> Get(
            [FromRoute] string organizationId,
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0
            )
        {
            return base.GetMany(organizationId);
        }

        /// <summary>
        /// Gets the Organization unit member for a particular Organization.
        /// </summary>
        /// <param name="organizationId">organization identifier</param>
        /// <param name="id">Organization Unit member id </param>
        /// <response code="200">OK,If Organization unit is available with the id for an Organization.></response>
        /// <response code="400">BadRequest, If the Organization Id/Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access by user.</response>
        /// <response code="404">NotFound</response>
        /// <response code="422">Unprocessable entity,validation error.</response>
        /// <returns>Ok response, If the member has been found with the particular id.</returns>
        [HttpGet("{id}", Name = "GetOrganizationUnitMember")]
        [ProducesResponseType(typeof(OrganizationUnitMember), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string organizationId, string id)
        {
            return await base.GetEntity(id);
        }

        /// <summary>
        /// Adds a Organization unit member to  an Organization unit
        /// </summary>
        /// <param name="organizationId">organization identifier</param>
        /// <param name="value">details of the organization unit member</param>
        /// <response code="200">OK,If the member has been added successfully for an Organization unit</response>
        /// <response code="400">BadRequest,If the Organization Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access by user.</response>
        /// <response code="409">Conflict,concurrency error </response>
        /// <response code="422">UnprocessableEntity,If a duplicate record is being entered.</response> 
        /// <returns>Ok response,If the organization unit member has been added to Organization unit</returns>
        [HttpPost]
        [ProducesResponseType(typeof(OrganizationUnitMember), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post(string organizationId, [FromBody] OrganizationUnitMember value)
        {
            value.OrganizationId = new Guid(organizationId);
            return await base.PostEntity(value);
        }

        /// <summary>
        /// Updates a member of an Organization unit
        /// </summary>
        /// <param name="organizationId">organization identifier</param>
        /// <param name="id">Member Identifier</param>
        /// <param name="value">Member Value to be updated </param>
        /// <response code="200">OK, If the value has been updated for the Organization member for an Organization.</response>
        /// <response code="400">BadRequest,If the Organization Id/member Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access</response>
        /// <response code="409">Conflict,concurrency error.</response>
        /// <response code="422">UnprocessableEntity,Validation error.</response>
        /// <returns>OK response,If the ORganization member has been updated with the new value.</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string organizationId, string id, [FromBody] OrganizationUnitMember value)
        {
            value.OrganizationId = new Guid(organizationId);
            return await base.PutEntity(id, value);
        }

        /// <summary>
        /// Deletes member from an organization unit
        /// </summary>
        /// <param name="id">Org. member identifier</param>
        /// <response code="200">OK, if the Organization member with the given id has been deleted for an Organization unit</response>
        /// <response code="400">BadRequest,if the id is not provided or not proper Guid.</response>
        /// <response code="403">Unauthorized access, if the user doesn't have permission to delete the Organization member.</response>
        /// <response code="422">Unprocessable Entity,Validation error.</response>
        /// <returns>OK response, if the Organization member with the given id has been deleted for an organization unit.</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(string id)
        {
            return await base.DeleteEntity(id);
        }


        /// <summary>
        /// Updates the partial details of the Organization Unit member
        /// </summary>
        /// <param name="id">Organization unit member identifier</param>
        /// <param name="value">Value of the ORganization unit members to be updated - Org Id/Org UnitID/Person Id/IsAdmin</param>
        /// <response code="200">OK,If update of Organization is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Organization values has been updated</returns>
        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id,[FromBody] JsonPatchDocument<OrganizationUnitMember> value)
        {
            return await base.PatchEntity(id, value);
        }
    }
}
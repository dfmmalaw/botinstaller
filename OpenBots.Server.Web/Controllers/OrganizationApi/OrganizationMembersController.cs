﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Model.Membership;
using OpenBots.Server.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using OpenBots.Server.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Net;
using System.IO;

namespace OpenBots.Server.WebAPI.Controllers
{
    [Route("api/v1/Organizations/{organizationId}/[controller]")]
    [ApiController]
    [Authorize]
    public class OrganizationMembersController : EntityController<OrganizationMember>
    {

        IMembershipManager membershipManager;
        readonly ApplicationIdentityUserManager userManager;
        readonly IPersonRepository personRepository;
        readonly IEmailManager emailSender;
        readonly IAccessRequestsManager accessRequestManager;

        public OrganizationMembersController(IOrganizationMemberRepository repository, 
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor,
            IPersonRepository personRepository,
            IAccessRequestsManager accessRequestManager,
            IEmailManager emailSender) : base(repository,  userManager, httpContextAccessor, membershipManager)
        {
            this.membershipManager = membershipManager;
            this.userManager = userManager;
            this.personRepository = personRepository;
            this.emailSender = emailSender;
            this.accessRequestManager = accessRequestManager;

            this.membershipManager.SetContext(base.SecurityContext);
            this.personRepository.SetContext(base.SecurityContext);
            this.accessRequestManager.SetContext(base.SecurityContext);

        }


        /// <summary>
        /// Gets the People in the Organization
        /// </summary>
        /// <param name="organizationId">The organization identifier.</param>
        /// <param name="top">The top.</param>
        /// <param name="skip">The skip.</param>
        /// <returns>List of TeamMember (View Model) for each person in the Organization</returns>
        [HttpGet("People")]
        [ProducesResponseType(typeof(PaginatedList<TeamMember>), StatusCodes.Status200OK)]
        public async Task<IActionResult>  GetPeople(
            [FromRoute] string organizationId,
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "", 
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0)
        {
            try
            {
                ODataHelper<TeamMember> oData = new ODataHelper<TeamMember>();
                string queryString = "";

                if (HttpContext != null
                    && HttpContext.Request != null
                    && HttpContext.Request.QueryString != null
                    && HttpContext.Request.QueryString.HasValue)
                    queryString = HttpContext.Request.QueryString.Value;

                var newNode = oData.ParseOrderByQuerry(queryString);
                if (newNode == null)
                    newNode = new OrderByNode<TeamMember>();

                Guid orgId = Guid.Parse(organizationId);
                var result =  membershipManager.GetPeopleInOrganization(orgId, newNode.PropertyName, newNode.Direction, skip, top);                

                return Ok(result);
            }
            catch(Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Gets all the members of the given Organization
        /// </summary>
        /// <param name="organizationId">Organization identifier</param>
        /// <param name="filter">search using a field</param>
        /// <param name="orderBy">sort ascending or  descending</param>
        /// <param name="top">fetches the first 100 rows</param>
        /// <param name="skip">skips the 'n' number of rows</param>
        /// <response code="200">Ok, If all the members for the Organization Id has been returned.</response>
        /// <response code="400">BadRequest,If the Organization ID is not provided or improper Guid. </response>
        /// <response code="403">Forbidden,Unauthorized access by user</response>
        /// <response code="404">NotFound, No member exists for the given Organization Id.</response>
        /// <response code="422">Unprocessable entity,Validation error</response>
        /// <returns>Paginated List of all the members of an Organization.</returns>
        [HttpGet]
        [ProducesResponseType(typeof(OrganizationMember),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public PaginatedList<OrganizationMember> Get(
            [FromRoute] string organizationId,
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0)
        {
            return base.GetMany(organizationId);
        }

        /// <summary>
        /// Get all the members for a particular organization.
        /// </summary>
        /// <remarks></remarks>
        /// <param name="organizationId">Organization Id</param>
        /// <param name="id">Organization member Identifier.</param>
        /// <response code="200">Ok,If Organization member exists for the given Id</response>
        /// <response code="304">NotModified</response>
        /// <response code="400">BadRequest, if the Organization Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access</response>
        /// <response code="404">NotFound, Organization member with the particular id does not exist.</response>
        /// <response code="422">Unprocessable Entity, Validation Error</response>
        /// <returns>Ok response, if Organization member exists for the given  id</returns>
        [HttpGet("{id}", Name = "GetOrganizationMember")]
        [ProducesResponseType(typeof(OrganizationMember),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public async Task<IActionResult> Get(string organizationId, string id)
        {
            return await base.GetEntity(id);
        }


        /// <summary>
        /// Adds a new access member to the Organization
        /// </summary>
        /// <param name="organizationId">Organization Identifier</param>
        /// <param name="value">Org member details</param>
        /// <response code="200">Ok,If the member  has been added successfully.</response>
        /// <response code="400">BadRequest,If the Organization Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access by the user</response>
        /// <response code="409">Conflict,When Organization member  with the particular id already exists.</response>
        /// <response code="422">UnprocessableEntity,validation error or cannot insert duplicate constraint.</response>
        /// <returns>Ok response,Creates a new member  with a new Organization member id.</returns>
        [HttpPost]
        [ProducesResponseType(typeof(OrganizationMember), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public async Task<IActionResult> Post(string organizationId, [FromBody] OrganizationMember value)
        {
            value.OrganizationId = new Guid(organizationId);
            return await base.PostEntity(value);
        }

        /// <summary>
        /// Update the Organization member details
        /// </summary>
        /// <remarks>Updates the Organization member details with the particular id ,for the given organization </remarks>
        /// <param name="organizationId">Organization identifier</param>
        /// <param name="id">Org member  Id</param>
        /// <param name="value">new value of the Organization member  to be updated .</param>
        /// <response code="200">OK,If the update of the Org member for the particular id has been successful.</response>
        /// <response code="400">BadRequest,If the Id is not provided or Guid is not in proper format.</response>
        /// <response code="403">Forbidden,Unauthorized access by the user</response>
        /// <response code="404">NotFound,If no access request exists for the given id.</response>
        /// <response code="409">Conflict</response>
        /// <response code="422">Unprocessable Entity,Validation error.</response>
        /// <returns>Ok response , If the Organization member  has been updated.</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public async Task<IActionResult> Put(string organizationId, string id, [FromBody] OrganizationMember value)
        {
            value.OrganizationId = new Guid(organizationId);
            return await base.PutEntity(id, value);
        }

        [HttpPost()]
        [Route("InviteUser")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public async Task<IActionResult> InviteUser(string organizationId, [FromBody] InviteUserViewModel value)
        {          
            OrganizationMember teamMember; 
            value.OrganizationId = new Guid(organizationId);
            var user = new ApplicationUser();

            try
            {
                //TODO - Before add invitation, check for logged user organization admin rights
                var organizationMember = repository.Find(null, a => a.PersonId == SecurityContext.PersonId && a.OrganizationId == Guid.Parse(organizationId))?.Items.FirstOrDefault();
                if (organizationMember == null)
                {
                    throw new UnauthorizedAccessException();
                }

                //This is to check if the user is already in the system and where is part of the organization
                teamMember = membershipManager.InviteUser(value, SecurityContext);
                if (teamMember == null)
                {
                    user.UserName = value.Email;
                    user.Email = value.Email;
                    RandomPassword randomPass = new RandomPassword();
                    string passwordString = randomPass.GenerateRandomPassword();
                    var loginResult = await userManager.CreateAsync(user, passwordString).ConfigureAwait(false);

                    if (!loginResult.Succeeded)
                    {
                        return GetErrorResult(loginResult);
                    }
                    else
                    {
                        //Add person email
                        var emailIds = new List<EmailVerification>();
                        var personEmail = new EmailVerification()
                        {
                            PersonId = Guid.Empty,
                            Address = value.Email,
                            IsVerified = false
                        };
                        emailIds.Add(personEmail);

                        Person newPerson = new Person()
                        {
                            Company = value.Company,
                            Department = value.Department,
                            Name = value.Name,
                            EmailVerifications = emailIds
                        };
                        var person = personRepository.Add(newPerson);

                        string code = await userManager.GenerateEmailConfirmationTokenAsync(user).ConfigureAwait(false);
                        string emailBody = SendConfirmationEmail(code, user.Id, passwordString, "en");
                        var subject = "Confirm your account at " + Constants.PRODUCT;
                        await emailSender.SendEmailAsync(user.Email, subject, emailBody).ConfigureAwait(false);

                        //Update the user 
                        if (person != null)
                        {
                            var registeredUser = userManager.FindByNameAsync(user.UserName).Result;
                            registeredUser.PersonId = (Guid)person.Id;
                            registeredUser.ForcedPasswordChange = true;
                            await userManager.UpdateAsync(registeredUser).ConfigureAwait(false);

                            //Add to person to organization only if you are admin or add it to access request table
                            var member = repository.Find(null, a => a.PersonId == SecurityContext.PersonId && a.OrganizationId == Guid.Parse(organizationId))?.Items.FirstOrDefault();
                            var IsOrgAdmin = member.IsAdministrator.GetValueOrDefault(false);

                            if (IsOrgAdmin)
                            {
                                OrganizationMember newOrgMember = new OrganizationMember()
                                {
                                    PersonId = person.Id,
                                    OrganizationId = Guid.Parse(organizationId),
                                    IsAutoApprovedByEmailAddress = true,
                                    IsInvited = true
                                };
                                await base.PostEntity(newOrgMember).ConfigureAwait(false);
                            }
                            else {
                                //Add it to access requests
                                AccessRequest accessRequest = new AccessRequest() { 
                                    OrganizationId = Guid.Parse(organizationId),
                                    PersonId = person.Id,
                                    IsAccessRequested = true,
                                    AccessRequestedOn = DateTime.UtcNow
                                };

                                accessRequestManager.AddAccessRequest(accessRequest);
                            }

                        }
                    }
                }

                return Ok();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Deletes Organization member 
        /// </summary>
        /// <param name="id">Organization member identifier.</param>
        /// <response code="200">OK, if the Organization member with the given id has been deleted.</response>
        /// <response code="400">BadRequest,if the id is not provided or not proper Guid.</response>
        /// <response code="403">Unauthorized access, if the user doesn't have permission to delete the Organization member.</response>
        /// <response code="422">Unprocessable Entity,Validation error.</response>
        /// <returns>OK response, if the Organization member with the given id has been deleted.</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public async Task<IActionResult> Delete(string organizationId, string id)
        {
            //Check if the logged in user is member of the organization, do not allow self deletion from organization
            var orgId = new Guid(organizationId);
            var orgMemberId = new Guid(id);

            var orgmem = membershipManager.GetOrganizationMember(orgId, SecurityContext.PersonId)?.Items?.FirstOrDefault();
            if (orgmem == null || (orgmem != null && orgmem.IsAdministrator == null) || (orgmem != null && orgmem.IsAdministrator.HasValue && orgmem.IsAdministrator == false))
            {
                ModelState.AddModelError("Delete", "Remove from organization failed, administrator of an organization can only remove members");
                return BadRequest(ModelState);
            }

            var orgMem = repository.Find(null, p => p.OrganizationId == orgId && p.Id == orgMemberId)?.Items?.FirstOrDefault();
            
            //if member is the logged in user, do not delete
            if (orgMem != null && orgMem.PersonId == SecurityContext.PersonId) {
                ModelState.AddModelError("Delete", "cannot remove from the organization");
                return BadRequest(ModelState);
            }

            return await base.DeleteEntity(id);
        }


        /// <summary>
        /// Updates the partial details of Organization members.
        /// </summary>
        /// <param name="id">Organization member identifier.</param>
        /// <param name="value">values to be updated</param>
        /// <response code="200">OK,If update of Organization member is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Organization member values has been updated</returns>
        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id, [FromBody]JsonPatchDocument<OrganizationMember> value)
        {
            return await base.PatchEntity(id, value);
        }

        #region Private methods - Security

        //TODO - To be moved to security manager 
        private async Task<IActionResult> RegisterNewUser(SignUpViewModel signupModel)
        {
            IActionResult result;
            var user = new ApplicationUser() { 
                UserName = signupModel.Email, 
                Email = signupModel.Email,
                Utm_Campaign = signupModel.Utm_Campaign,
                Utm_Content = signupModel.Utm_Content,
                Utm_Medium = signupModel.Utm_Medium,
                Utm_Source = signupModel.Utm_Source,
                Utm_Term = signupModel.Utm_Term,
                Plan = signupModel.Plan
            };

            RandomPassword randomPass = new RandomPassword();
            string passwordString = randomPass.GenerateRandomPassword();
            var loginResult = await userManager.CreateAsync(user, passwordString).ConfigureAwait(false);

            if (!loginResult.Succeeded)
            {
                result = GetErrorResult(loginResult);
            }
            else
            {
                //Add person email
                var emailIds = new List<EmailVerification>();
                var personEmail = new EmailVerification()
                {
                    PersonId = Guid.Empty,
                    Address = signupModel.Email
                };
                emailIds.Add(personEmail);

                Person newPerson = new Person()
                {
                    Company = signupModel.Organization,
                    Department = signupModel.Department,
                    Name = signupModel.Name,
                    EmailVerifications = emailIds
                };
                var person = personRepository.Add(newPerson);

                string code = await userManager.GenerateEmailConfirmationTokenAsync(user).ConfigureAwait(false);
                string emailBody = SendConfirmationEmail(code, user.Id, passwordString, "en");
                var subject = "Confirm your account at " + Constants.PRODUCT;
                await emailSender.SendEmailAsync(user.Email, subject, emailBody).ConfigureAwait(false);

                //Update the user 
                if (person != null)
                {
                    var registeredUser = userManager.FindByNameAsync(user.UserName).Result;
                    registeredUser.PersonId = (Guid)person.Id;
                    registeredUser.ForcedPasswordChange = true;
                    await userManager.UpdateAsync(registeredUser).ConfigureAwait(false);
                }
                result = Ok();

            }

            return result;
        }

        private IActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (IdentityError error in result.Errors)
                    {
                        ModelState.AddModelError("Signup", error.Description);
                    }
                }

                //if (ModelState.IsValid)
                //{
                //    // No ModelState errors are available to send, so just return an empty BadRequest.
                //    return BadRequest();
                //}

                return BadRequest(ModelState);
            }

            return null;
        }

        private string SendConfirmationEmail(string code, string userId, string password, string language)
        {
            if (language == "en")
            {

                string confirmationLink = Url.Action("ConfirmEmail",
                                                 "Auth", new
                                                 {
                                                     userid = userId,
                                                     code = code
                                                 }, protocol: HttpContext.Request.Scheme);

                string emailBody = "";

                using (StreamReader reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Email/accountCreation.html")))
                {
                    emailBody = reader.ReadToEnd();
                    emailBody = emailBody.Replace("^Password^", password, StringComparison.OrdinalIgnoreCase);
                    emailBody = emailBody.Replace("^Confirm^", confirmationLink, StringComparison.OrdinalIgnoreCase);
                }

                return emailBody;
            }

            return "";

        }
        #endregion

    }
}
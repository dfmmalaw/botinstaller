﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace OpenBots.Server.WebAPI.Controllers.OrganizationApi
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class UserConsentController : EntityController<UserConsent>
    {
        readonly IUserConsentRepository repository;
        readonly ApplicationIdentityUserManager userManager;
        readonly IConfiguration configuration;
        readonly ILogger<UserConsentController> logger;
        readonly IMembershipManager membershipManager;
        readonly IPersonRepository personRepository;
        readonly IEmailManager emailSender;
        readonly IPersonEmailRepository personEmailRepository;
        readonly IEmailVerificationRepository emailVerificationRepository;
        readonly ITermsConditionsManager termsConditionsManager;


        public UserConsentController(
            IUserConsentRepository repository,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor,
            IConfiguration configuration,
            ILogger<UserConsentController> logger,
            IMembershipManager membershipManager,
            IPersonRepository personRepository,
            IPersonEmailRepository personEmailRepository,
            IEmailVerificationRepository emailVerificationRepository,
            ITermsConditionsManager termsConditionsManager,
        IEmailManager emailSender) : base(repository, userManager, httpContextAccessor, membershipManager)
        {
            this.repository = repository;
            this.userManager = userManager;
            this.configuration = configuration;
            this.logger = logger;
            this.membershipManager = membershipManager;
            this.personRepository = personRepository;
            this.emailSender = emailSender;
            this.personEmailRepository = personEmailRepository;
            this.emailVerificationRepository = emailVerificationRepository;

            this.repository.SetContext(base.SecurityContext); ;
            this.membershipManager.SetContext(base.SecurityContext);
            this.personRepository.SetContext(base.SecurityContext);           
            this.personEmailRepository.SetContext(base.SecurityContext);
            this.emailVerificationRepository.SetContext(base.SecurityContext);

        }

        
        /// <summary>
        /// Provides a list of all user consents
        /// </summary>
        /// <response code="200">OK,a Paginated list of all user consents</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// /// <returns>Paginated list of all user consents </returns>
        [HttpGet]
        [ProducesResponseType(typeof(UserConsent), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(PaginatedList<UserConsent>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<UserConsent> Get(
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0
            )
        {
            return base.GetMany();
        }

        /// <summary>
        /// Provides an UserConsent details for a particular organization Id.
        /// </summary>
        /// <param name="id">UserConsent id</param>
        /// <response code="200">OK, If an UserConsent exists with the given Id.</response>
        /// <response code="304">Not modified</response>
        /// <response code="400">BadRequest,If UserConsent id is not in proper format or proper Guid.</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">NotFound, when no UserConsent exists for the given UserConsent id</response>
        /// <returns>UserConsent details for the given Id</returns>
        [HttpGet("{id}", Name = "GetUserConsent")]
        [ProducesResponseType(typeof(UserConsent), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                var result = base.GetEntity(id).Result;
                if (result.GetType() == typeof(StatusCodeResult)) { return result; }
                var userConsent = ((OkObjectResult)result).Value as UserConsent;
              
                return Ok(userConsent);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Save user constent
        /// </summary>
        /// <remarks>
        /// Save user constent
        /// </remarks>
        /// <param name="value"></param>
        /// <response code="200">OK,user constent saved </response>
        /// <response code="400">BadRequest,When the user consent value is not in proper format</response>
        /// <response code="403">Forbidden, unauthorized access</response>
        ///<response code="409">Conflict,concurrency error</response> 
        /// <response code="422">UnprocessabileEntity,when a duplicate record is being entered.</response>
        /// <returns> newly created user constent </returns>
        [HttpPost]
        [ProducesResponseType(typeof(UserConsent), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post([FromBody] UserConsent value)
        {
            if (value == null)
            {
                ModelState.AddModelError("Save", "No data passed");
                return BadRequest(ModelState);
            }
           
            try
            {
                var userConsent = repository.Find(null, p => p.PersonId == SecurityContext.PersonId && p.UserAgreementID == value.UserAgreementID)?.Items?.FirstOrDefault();
                if (userConsent != null)
                {
                    userConsent.IsAccepted = value.IsAccepted;
                    if (value.IsAccepted)
                        userConsent.ExpiresOnUTC = DateTime.UtcNow.AddYears(1);
                    return await base.PutEntity(userConsent.Id.ToString(), userConsent).ConfigureAwait(false);
                }
                else
                {
                    var userAgreementConsent = new UserConsent()
                    {
                        PersonId = value.PersonId,
                        UserAgreementID = value.UserAgreementID,
                        IsAccepted = value.IsAccepted,
                        ExpiresOnUTC = DateTime.UtcNow.AddYears(1)
                    };
                    return await base.PostEntity(userAgreementConsent);
                }
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }


        /// <summary>
        /// Updates an UserConsent 
        /// </summary>
        /// <remarks>
        /// Provides an action to update an UserConsent, when UserConsent id and the new details of UserConsent are given
        /// </remarks>
        /// <param name="id">UserConsent Id,produces Bad request if Id is null or Id's don't match</param>
        /// <param name="value">UserConsent details to be updated</param>
        /// <response code="200">OK, If the UserConsent details for the given UserConsent Id has been updated.</response>
        /// <response code="400">BadRequest,if the UserConsent Id is null or Id's don't match</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>OK response with the updated value</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string id, [FromBody] UserConsent value)
        {
            if (value == null && string.IsNullOrEmpty(id))
            {
                ModelState.AddModelError("Update", "user consent details not passed");
                return BadRequest(ModelState);
            }
            Guid entityId = new Guid(id);
            value.Id = entityId;
            return await base.PutEntity(id, value).ConfigureAwait(false);

        }

        /// <summary>
        /// Deletes an UserConsent with a specified id from the UserConsents.
        /// </summary>
        /// <param name="id">UserConsent ID to be deleted- throws BadRequest if null or empty Guid/</param>
        /// <response code="200">OK,when UserConsent is softdeleted,( isDeleted flag is set to true in DB) </response>
        /// <response code="400">BadRequest,If Organization Id is null or empty Guid</response>
        /// <response code="403">Forbidden </response>
        /// <returns>OK response with deleted value </returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                ModelState.AddModelError("Delete", "UserConsent Id not passed");
                return BadRequest(ModelState);
            }

            return await base.DeleteEntity(id);
        }

        /// <summary>
        /// Updates partial details of Organization.
        /// </summary>
        /// <param name="id">Organization identifier</param>
        /// <param name="value">Value of the Organization to be updated.</param>
        /// <response code="200">OK,If update of Organization is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Organization values has been updated</returns>

        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id,
            [FromBody] JsonPatchDocument<UserConsent> value)
        {
            if (value == null && string.IsNullOrEmpty(id))
            {
                ModelState.AddModelError("Update", "Organization details not passed");
                return BadRequest(ModelState);
            }

            return await base.PatchEntity(id, value);
        }

    }
}


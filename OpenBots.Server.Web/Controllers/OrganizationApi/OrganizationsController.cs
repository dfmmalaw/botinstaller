﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Model.Membership;
using OpenBots.Server.Security;
using OpenBots.Server.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace OpenBots.Server.WebAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class OrganizationsController : EntityController<Organization>
    {
        IMembershipManager membershipManager;
        IOrganizationManager organizationManager;
        public OrganizationsController(
            IOrganizationRepository repository, 
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IOrganizationManager organizationManager,
            IHttpContextAccessor httpContextAccessor) : base(repository, userManager, httpContextAccessor, membershipManager)
        {
            this.membershipManager = membershipManager;
            this.organizationManager = organizationManager;


            this.membershipManager.SetContext(base.SecurityContext);
            this.organizationManager.SetContext(base.SecurityContext);

        }

        /// <summary>
        /// Provides a list of all organizations
        /// </summary>
        /// <response code="200">OK,a Paginated list of all Organizations</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// /// <returns>Paginated list of all Organizations </returns>
        [HttpGet]
        //        [ProducesResponseType(typeof(Organization), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(PaginatedList<Organization>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<Organization> Get(
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0
            )
        {
            return base.GetMany();
        }

        /// <summary>
        /// Provides count of organizations
        /// </summary>
        /// <response code="200">OK,an Organization count</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>All Organization count</returns>
        [AllowAnonymous]
        [HttpGet("TotalOrganizationCount")]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public int? Get()
        {
            return base.GetMany()?.TotalCount;
        }

        /// <summary>
        /// Provides an organization details for a particular organization Id.
        /// </summary>
        /// <param name="id">Organization id</param>
        /// <response code="200">OK, If an organization exists with the given Id.</response>
        /// <response code="304">Not modified</response>
        /// <response code="400">BadRequest,If Organization id is not in proper format or proper Guid.</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">NotFound, when no organization exists for the given Organization id</response>
        /// <returns>Organization details for the given Id</returns>
        [HttpGet("{id}", Name = "GetOrganization")]
        [ProducesResponseType(typeof(Organization), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                var orgmem = membershipManager.GetOrganizationMember(Guid.Parse(id), SecurityContext.PersonId)?.Items?.FirstOrDefault();

                if (orgmem == null)
                {
                    return Ok();
                }

                var result = base.GetEntity(id).Result;
                if (result.GetType() == typeof(StatusCodeResult)) { return result; }
                var organization = ((OkObjectResult)result).Value as Organization;
                //Need to get organization member using logged in person Id 
                
                if (orgmem != null)
                    organization.Members.Add(orgmem);

                return Ok(organization);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Adds a new Organization to the existing Organizations
        /// </summary>
        /// <remarks>
        /// Adds the organization with unique Organization Id to the existing organizations
        /// </remarks>
        /// <param name="value"></param>
        /// <response code="200">OK,new organization created and returned</response>
        /// <response code="400">BadRequest,When the Organization value is not in proper format</response>
        /// <response code="403">Forbidden, unauthorized access</response>
        ///<response code="409">Conflict,concurrency error</response> 
        /// <response code="422">UnprocessabileEntity,when a duplicate record is being entered.</response>
        /// <returns> newly created unique Organization Id with route name </returns>
        [HttpPost]
        [ProducesResponseType(typeof(Organization), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post([FromBody] Organization value)
        {
            if (value == null)
            {
                ModelState.AddModelError("Save", "No data passed");
                return BadRequest(ModelState);
            }            

            Guid entityId = Guid.NewGuid();
            if (value.Id == null || value.Id.HasValue || value.Id.Equals(Guid.Empty))
                value.Id = entityId;

            try
            {
                if (SecurityContext == null)
                {
                    ModelState.AddModelError("Add", "Create organization failed. Logged in user no longer exists in system.");
                    return BadRequest(ModelState);
                }

                //Same name organizatgion check
                var existingOrg = repository.Find(null, o => o.Name.Trim().ToLower() == value.Name.Trim().ToLower()).Items?.FirstOrDefault();
                if (existingOrg != null)
                {
                    ModelState.AddModelError("Add", "Organization name already exists. cannot add duplicate.");
                    return BadRequest(ModelState);
                }

                value.ProcessKeyPrefix = "P"; //TODO : Hardcoding to default
                var newOrganization = organizationManager.AddNewOrganization(value);
                return Ok(newOrganization);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }


        /// <summary>
        /// Updates an organization 
        /// </summary>
        /// <remarks>
        /// Provides an action to update an Organization, when Organization id and the new details of organization are given
        /// </remarks>
        /// <param name="id">Organization Id,produces Bad request if Id is null or Id's don't match</param>
        /// <param name="value">Organization details to be updated</param>
        /// <response code="200">OK, If the organization details for the given organization Id has been updated.</response>
        /// <response code="400">BadRequest,if the Organization Id is null or Id's don't match</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>OK response with the updated value</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string id, [FromBody] Organization value)
        {
            if (value == null && string.IsNullOrEmpty(id))
            {
                ModelState.AddModelError("Update", "Organization details not passed");
                return BadRequest(ModelState);
            }
            Guid entityId = new Guid(id);

            if (value == null || value.Id == null || !value.Id.HasValue || entityId != value.Id.Value)
            {
                ModelState.AddModelError("Update", "Organization IDs don't match");
                return BadRequest(ModelState);
            }

            var orgmem = membershipManager.GetOrganizationMember(Guid.Parse(id), SecurityContext.PersonId)?.Items?.FirstOrDefault();
            if (orgmem == null || (orgmem != null && orgmem.IsAdministrator == null) || (orgmem != null && orgmem.IsAdministrator.HasValue && orgmem.IsAdministrator == false))
            {
                ModelState.AddModelError("Update", "Update failed, administrator of an organization can only update");
                return BadRequest(ModelState);
            }

            //Same name organizatgion check
            var existingOrg = repository.Find(null, o => o.Name.Trim().ToLower() == value.Name.Trim().ToLower() && o.Id != entityId).Items?.FirstOrDefault();
            if (existingOrg != null)
            {
                ModelState.AddModelError("Update", "Organization name already exists. cannot add duplicate.");
                return BadRequest(ModelState);
            }

            var org = repository.GetOne(Guid.Parse(id));
            org.Name = !string.IsNullOrEmpty(value.Name) && !org.Name.Equals(value.Name, StringComparison.OrdinalIgnoreCase) ? value.Name : org.Name;
            org.IsPublic = value.IsPublic != null && value.IsPublic.HasValue ? value.IsPublic.Value : org.IsPublic.Value;
            if (value.IsVisibleToEmailDomain != null && value.IsVisibleToEmailDomain.HasValue) {
                org.IsVisibleToEmailDomain = value.IsVisibleToEmailDomain;
                if (org.IsVisibleToEmailDomain.GetValueOrDefault() && value.EMailDomain != null) { org.EMailDomain = value.EMailDomain; }
            } 

            return await base.PutEntity(id, org).ConfigureAwait(false);

        }

        /// <summary>
        /// Deletes an organization with a specified id from the Organizations.
        /// </summary>
        /// <param name="id">Organization ID to be deleted- throws BadRequest if null or empty Guid/</param>
        /// <response code="200">OK,when organization is softdeleted,( isDeleted flag is set to true in DB) </response>
        /// <response code="400">BadRequest,If Organization Id is null or empty Guid</response>
        /// <response code="403">Forbidden </response>
        /// <returns>OK response with deleted value </returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                ModelState.AddModelError("Delete", "Organization Id not passed");
                return BadRequest(ModelState);
            }

            var orgmem = membershipManager.GetOrganizationMember(Guid.Parse(id), SecurityContext.PersonId)?.Items?.FirstOrDefault();
            if (orgmem == null || (orgmem != null && orgmem.IsAdministrator == null) || (orgmem != null && orgmem.IsAdministrator.HasValue && orgmem.IsAdministrator == false))
            {
                ModelState.AddModelError("Update", "Delete failed, administrator of an organization can only delete");
                return BadRequest(ModelState);
            }

       

            return await base.DeleteEntity(id);
        }

        /// <summary>
        /// Updates partial details of Organization.
        /// </summary>
        /// <param name="id">Organization identifier</param>
        /// <param name="value">Value of the Organization to be updated.</param>
        /// <response code="200">OK,If update of Organization is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Organization values has been updated</returns>

        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id,
            [FromBody] JsonPatchDocument<Organization> value)
        {
            if (value == null && string.IsNullOrEmpty(id))
            {
                ModelState.AddModelError("Update", "Organization details not passed");
                return BadRequest(ModelState);
            }
            
            var orgmem = membershipManager.GetOrganizationMember(Guid.Parse(id), SecurityContext.PersonId)?.Items?.FirstOrDefault();
            if (orgmem == null || (orgmem != null && orgmem.IsAdministrator == null) || (orgmem != null && orgmem.IsAdministrator.HasValue && orgmem.IsAdministrator == false))
            {
                ModelState.AddModelError("Update", "Update failed, administrator of an organization can only update");
                return BadRequest(ModelState);
            }

            return await base.PatchEntity(id, value);
        }
    }
}

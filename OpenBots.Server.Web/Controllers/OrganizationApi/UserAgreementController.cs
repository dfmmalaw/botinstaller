﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace OpenBots.Server.WebAPI.Controllers.OrganizationApi
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class UserAgreementController : ReadOnlyEntityController<UserAgreement>
    {
        readonly IUserAgreementRepository repository;
        readonly ApplicationIdentityUserManager userManager;
        readonly IConfiguration configuration;
        readonly ILogger<UserConsentController> logger;
        readonly IMembershipManager membershipManager;
        readonly IPersonRepository personRepository;
        readonly IEmailManager emailSender;
        readonly IPersonEmailRepository personEmailRepository;
        readonly IEmailVerificationRepository emailVerificationRepository;
        readonly ITermsConditionsManager termsConditionsManager;

        public UserAgreementController(
            IUserAgreementRepository repository,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor,
            IConfiguration configuration,
            ILogger<UserConsentController> logger,
            IMembershipManager membershipManager,
            IPersonRepository personRepository,
            IPersonEmailRepository personEmailRepository,
            IEmailVerificationRepository emailVerificationRepository,
            ITermsConditionsManager termsConditionsManager,
            IEmailManager emailSender) : base(repository, userManager, httpContextAccessor, membershipManager)
        {
            this.repository = repository;
            this.userManager = userManager;
            this.configuration = configuration;
            this.logger = logger;
            this.membershipManager = membershipManager;
            this.personRepository = personRepository;
            this.emailSender = emailSender;
            this.personEmailRepository = personEmailRepository;
            this.emailVerificationRepository = emailVerificationRepository;
            this.termsConditionsManager = termsConditionsManager;

            this.repository.SetContext(base.SecurityContext);
            this.membershipManager.SetContext(base.SecurityContext);
            this.personRepository.SetContext(base.SecurityContext);
            this.personEmailRepository.SetContext(base.SecurityContext);
            this.emailVerificationRepository.SetContext(base.SecurityContext);
            this.termsConditionsManager.SetContext(base.SecurityContext);

        }


        /// <summary>
        /// Get latest terms and conditions for user consent
        /// </summary>
        /// <response code="200">OK,latest terms and conditions for user consent</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// /// <returns>user agreement</returns>
        [HttpGet]
        [ProducesResponseType(typeof(UserAgreement), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get()
        {
            var response = await termsConditionsManager.GetUserAgreement().ConfigureAwait(false);
            return Ok(response);
        }

        /// <summary>
        /// Check that the UserConcent has been recorded for logged user on the latest user agreement and IsAccepted is True AND has not Expired. 
        /// If there is NO record of acceptance for the latest version of the Terms and Conditions, then response will be FALSE which will force to have user consent before proceeding
        /// </summary>
        /// <response code="200">OK,True or False</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// /// <returns>bool</returns>
        [HttpGet("{id}/HasAcceptedAndNotExpired")]
        [ProducesResponseType(typeof(UserAgreement), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string id)
        {
            Guid entityId = new Guid(id);
            var response = await termsConditionsManager.IsAccepted(entityId, SecurityContext.PersonId).ConfigureAwait(false);
            return Ok(response);
        }


        ///// <summary>
        ///// Adds a new User Agreement
        ///// </summary>
        ///// <remarks>
        ///// 
        ///// </remarks>
        ///// <param name="value"></param>
        ///// <response code="200">OK,new UserAgreement created and returned</response>
        ///// <response code="400">BadRequest,When the UserAgreement value is not in proper format</response>
        ///// <response code="403">Forbidden, unauthorized access</response>
        /////<response code="409">Conflict,concurrency error</response> 
        ///// <response code="422">UnprocessabileEntity,when a duplicate record is being entered.</response>
        ///// <returns> newly created unique UserAgreement Id with route name </returns>
        //[HttpPost]
        //[ProducesResponseType(typeof(UserAgreement), StatusCodes.Status200OK)]
        //[Produces("application/json")]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(StatusCodes.Status409Conflict)]
        //[ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        //[ProducesDefaultResponseType]
        //public async Task<IActionResult> Post([FromBody] UserAgreement value)
        //{
        //    if (value == null)
        //    {
        //        ModelState.AddModelError("Save", "No data passed");
        //        return BadRequest(ModelState);
        //    }

        //    Guid entityId = Guid.NewGuid();
        //    if (value.Id == null || value.Id.HasValue || value.Id.Equals(Guid.Empty))
        //        value.Id = entityId;

        //    try
        //    {
        //        return await base.PostEntity(value).ConfigureAwait(false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.GetActionResult();
        //    }
        //}


        ///// <summary>
        ///// Updates an UserAgreement 
        ///// </summary>
        ///// <remarks>
        ///// Provides an action to update an UserAgreement, when UserAgreement id and the new details of UserAgreement are given
        ///// </remarks>
        ///// <param name="id">UserAgreement Id,produces Bad request if Id is null or Id's don't match</param>
        ///// <param name="value">UserAgreement details to be updated</param>
        ///// <response code="200">OK, If the UserAgreement details for the given UserAgreement Id has been updated.</response>
        ///// <response code="400">BadRequest,if the UserAgreement Id is null or Id's don't match</response>
        ///// <response code="403">Forbidden,unauthorized access</response>
        ///// <response code="422">UnprocessableEntity</response>
        ///// <returns>OK response with the updated value</returns>
        //[HttpPut("{id}")]
        //[ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        //[Produces("application/json")]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(StatusCodes.Status409Conflict)]
        //[ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        //[ProducesDefaultResponseType]
        //public async Task<IActionResult> Put(string id, [FromBody] UserAgreement value)
        //{
        //    if (value == null && string.IsNullOrEmpty(id))
        //    {
        //        ModelState.AddModelError("Update", "User Agreement details not passed");
        //        return BadRequest(ModelState);
        //    }
        //    Guid entityId = new Guid(id);
        //    value.Id = entityId;
        //    return await base.PutEntity(id, value).ConfigureAwait(false);

        //}

        ///// <summary>
        ///// Deletes an UserAgreement with a specified id from the UserAgreements.
        ///// </summary>
        ///// <param name="id">UserAgreement ID to be deleted- throws BadRequest if null or empty Guid/</param>
        ///// <response code="200">OK,when UserAgreement is softdeleted,( isDeleted flag is set to true in DB) </response>
        ///// <response code="400">BadRequest,If Organization Id is null or empty Guid</response>
        ///// <response code="403">Forbidden </response>
        ///// <returns>OK response with deleted value </returns>
        //[HttpDelete("{id}")]
        //[ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        //[Produces("application/json")]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesDefaultResponseType]
        //public async Task<IActionResult> Delete(string id)
        //{
        //    if (string.IsNullOrEmpty(id))
        //    {
        //        ModelState.AddModelError("Delete", "UserAgreement Id not passed");
        //        return BadRequest(ModelState);
        //    }

        //    return await base.DeleteEntity(id);
        //}

        ///// <summary>
        ///// Updates partial details of Organization.
        ///// </summary>
        ///// <param name="id">Organization identifier</param>
        ///// <param name="value">Value of the Organization to be updated.</param>
        ///// <response code="200">OK,If update of Organization is successful. </response>
        ///// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        ///// <response code="403">Forbidden,unauthorized access</response>
        ///// <response code="422">Unprocessable entity,validation error</response>
        ///// <returns>Ok response, if the partial Organization values has been updated</returns>

        //[HttpPatch("{id}")]
        //[ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        //[Produces("application/json")]
        //public async Task<IActionResult> Patch(string id,
        //    [FromBody] JsonPatchDocument<UserAgreement> value)
        //{
        //    if (value == null && string.IsNullOrEmpty(id))
        //    {
        //        ModelState.AddModelError("Update", "Organization details not passed");
        //        return BadRequest(ModelState);
        //    }
            
        //    return await base.PatchEntity(id, value);
        //}

    }
}

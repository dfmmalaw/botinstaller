﻿using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using StringToExpression.LanguageDefinitions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using OpenBots.Server.DataAccess.Exceptions;

namespace OpenBots.Server.WebAPI.Controllers
{

    public abstract class ReadOnlyEntityController<T> : ApplicationBaseController
        where T : class, IEntity, new()
    {
        protected readonly IReadOnlyEntityRepository<T> readRepository;
        

        protected ReadOnlyEntityController(IReadOnlyEntityRepository<T> repository,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor,
            IMembershipManager membershipManager
            ) : base(httpContextAccessor, userManager, membershipManager)
        {
            readRepository = repository;
            readRepository.SetContext(base.SecurityContext);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentid"></param>
        /// <returns></returns>
        /// <exception cref="EntityOperationException"></exception>
        /// <exception cref="EntityDoesNotExistException"></exception>
        /// <exception cref="UnauthorizedOperationException"></exception>
        /// <exception cref="EntityValidationException"></exception>
        protected virtual PaginatedList<T> GetMany(string parentid = "")
        {
            ODataHelper<T> oData = new ODataHelper<T>();

            string queryString = "";

            if (HttpContext != null
                && HttpContext.Request != null
                && HttpContext.Request.QueryString != null
                && HttpContext.Request.QueryString.HasValue)
                queryString = HttpContext.Request.QueryString.Value;

            oData.Parse(queryString);
            Guid parentguid = Guid.Empty;
            if (!string.IsNullOrEmpty(parentid))
                parentguid = new Guid(parentid);

            return readRepository.Find(parentguid, oData.Filter, oData.Sort, oData.SortDirection, oData.Skip, oData.Top);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parentid"></param>
        /// <returns></returns>
        /// <exception cref="EntityOperationException"></exception>
        /// <exception cref="EntityDoesNotExistException"></exception>
        /// <exception cref="UnauthorizedOperationException"></exception>
        /// <exception cref="EntityValidationException"></exception>
        protected virtual async Task<IActionResult> GetEntity(string id, string parentid = "")
        {
            T entity = null;
            entity = readRepository.GetOne(new Guid(id));
            if (entity == null)
            {
                ModelState.AddModelError("GetData", "Record does not exist or you do not have authorized access.");
                return BadRequest(ModelState);
            }

            string timeStamp = "\"" + Convert.ToBase64String(entity.Timestamp) + "\"";
            if (Request.Headers.ContainsKey("if-none-match"))
            {
                string etag = Request.Headers["if-none-match"];

                if (!string.IsNullOrEmpty(etag) && etag.Equals(timeStamp, StringComparison.InvariantCultureIgnoreCase))
                {
                    return StatusCode(304); // Not Modified if ETag is same as Current Timestamp
                }
            }
            try
            {
                Response.Headers.Add("ETag", timeStamp);
            }
            catch 
            {
                return Ok(entity);
            }

            return Ok(entity);
        }




    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Security;
using OpenBots.Server.ViewModel;
using OpenBots.Server.WebAPI.Controllers;
using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace OpenBots.Server.Web
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class CredentialsController : EntityController<Credential>
    {
        ICredentialManager credentialManager;
        private readonly IHttpContextAccessor httpContextAccessor;
        public CredentialsController(
            ICredentialRepository repository,
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            ICredentialManager credentialManager,
            IHttpContextAccessor httpContextAccessor) : base(repository, userManager, httpContextAccessor, membershipManager)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.credentialManager = credentialManager;
            this.credentialManager.SetContext(base.SecurityContext);
        }

        /// <summary>
        /// Provides a list of all Credentials
        /// </summary>
        /// <response code="200">OK,a Paginated list of all Credentials</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>Paginated list of all Credentials </returns>
        [HttpGet]
        [ProducesResponseType(typeof(PaginatedList<Credential>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<Credential> Get(
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0
            )
        {
            return base.GetMany();
        }

        /// <summary>
        /// Provides a Credential's details for a particular Credential Id.
        /// </summary>
        /// <param name="id">Credential id</param>
        /// <response code="200">OK, If an Credential exists with the given Id.</response>
        /// <response code="304">Not modified</response>
        /// <response code="400">BadRequest,If Credential id is not in proper format or proper Guid.</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">NotFound, when no Credential exists for the given Credential id</response>
        /// <returns>Credential details for the given Id</returns>
        [HttpGet("{id}", Name = "GetCredential")]
        [ProducesResponseType(typeof(PaginatedList<Credential>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                Guid entityId = new Guid(id);

                var existingAgent = repository.GetOne(entityId);
                if (existingAgent == null) return NotFound();

                if (credentialManager.ValidateRetrievalDate(existingAgent))
                {
                    return await base.GetEntity(id);
                }
                ModelState.AddModelError("Credential", "Current date does not fall withing the start and end date range");
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Provides a Credential's View details  for a particular Credential Id.
        /// </summary>
        /// <param name="id">Credential id</param>
        /// <response code="200">OK, If an Credential exists with the given Id.</response>
        /// <response code="304">Not modified</response>
        /// <response code="400">BadRequest,If Credential id is not in proper format or proper Guid.</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">NotFound, when no Credential exists for the given Credential id</response>
        /// <returns>Credential view details for the given Id</returns>
        [HttpGet("view/{id}")]
        [ProducesResponseType(typeof(PaginatedList<CredentialViewModel>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> View(string id)
        {
            try
            {
                Credential credential = new Credential();
                credential = readRepository.GetOne(new Guid(id));
                if (credential == null)
                {
                    ModelState.AddModelError("GetData", "Record does not exist or you do not have authorized access.");
                    return BadRequest(ModelState);
                }

                string timeStamp = "\"" + Convert.ToBase64String(credential.Timestamp) + "\"";
                if (Request.Headers.ContainsKey("if-none-match"))
                {
                    string etag = Request.Headers["if-none-match"];

                    if (!string.IsNullOrEmpty(etag) && etag.Equals(timeStamp, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return StatusCode(304); // Not Modified if ETag is same as Current Timestamp
                    }
                }
                try
                {
                    Response.Headers.Add("ETag", timeStamp);
                }
                catch
                {
                    return Ok(credentialManager.GetCredentialView(credential));
                }

                return Ok(credentialManager.GetCredentialView(credential));
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Adds a new Credential to the existing Credentials
        /// </summary>
        /// <remarks>
        /// Adds the Credential with unique Id to the existing Credentials
        /// </remarks>
        /// <param name="value"></param>
        /// <response code="200">OK,new Credential created and returned</response>
        /// <response code="400">BadRequest,When the Credential value is not in proper format</response>
        /// <response code="403">Forbidden, unauthorized access</response>
        ///<response code="409">Conflict,concurrency error</response> 
        /// <response code="422">UnprocessabileEntity,when a duplicate record is being entered.</response>
        /// <returns> newly created unique Credential Id with route name </returns>
        [HttpPost]
        [ProducesResponseType(typeof(Credential), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post([FromBody] Credential request)
        {
            if (request == null)
            {
                ModelState.AddModelError("Save", "No data passed");
                return BadRequest(ModelState);
            }

            Guid entityId = Guid.NewGuid();
            if (request.Id == null || request.Id.HasValue || request.Id.Equals(Guid.Empty))
                request.Id = entityId;

            try
            {
                applicationUser = userManager.GetUserAsync(httpContextAccessor.HttpContext.User).Result;

                if (request.PasswordSecret != null && applicationUser != null)
                {
                    request.PasswordHash = userManager.PasswordHasher.HashPassword(applicationUser, request.PasswordSecret);
                }

                return await base.PostEntity(request);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Updates an Credential 
        /// </summary>
        /// <remarks>
        /// Provides an action to update a Credential, when id and the new details of Credential are given
        /// </remarks>
        /// <param name="id">Credential Id,produces Bad request if Id is null or Id's don't match</param>
        /// <param name="value">Credential details to be updated</param>
        /// <response code="200">OK, If the Credential details for the given Credential Id has been updated.</response>
        /// <response code="400">BadRequest,if the Credential Id is null or Id's don't match</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>OK response with the updated value</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string id, [FromBody] Credential request)
        {
            try
            {
                Guid entityId = new Guid(id);

                var existingCredential = repository.GetOne(entityId);
                if (existingCredential == null) return NotFound();

                existingCredential.Name = request.Name;
                existingCredential.Provider = request.Provider;
                existingCredential.StartDate = request.StartDate;
                existingCredential.EndDate = request.EndDate;
                existingCredential.Domain = request.Domain;
                existingCredential.UserName = request.UserName;
                existingCredential.PasswordSecret = request.PasswordSecret;
                existingCredential.PasswordHash = request.PasswordHash;
                existingCredential.Certificate = request.Certificate;

                applicationUser = userManager.GetUserAsync(httpContextAccessor.HttpContext.User).Result;

                if (request.PasswordSecret != null && applicationUser != null)
                {
                    request.PasswordHash = userManager.PasswordHasher.HashPassword(applicationUser, request.PasswordSecret);
                }

                return await base.PutEntity(id, existingCredential);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Credential", ex.Message);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Deletes a Credential with a specified id from the Credential.
        /// </summary>
        /// <param name="id">Credential ID to be deleted- throws BadRequest if null or empty Guid/</param>
        /// <response code="200">OK,when Credential is softdeleted,( isDeleted flag is set to true in DB) </response>
        /// <response code="400">BadRequest,If Credential Id is null or empty Guid</response>
        /// <response code="403">Forbidden </response>
        /// <returns>OK response with deleted value </returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(string id)
        {
            return await base.DeleteEntity(id);
        }

        /// <summary>
        /// Updates partial details of Credential.
        /// </summary>
        /// <param name="id">Credential identifier</param>
        /// <param name="value">Value of the Credential to be updated.</param>
        /// <response code="200">OK,If update of Credential is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Credential values has been updated</returns>

        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id,
            [FromBody] JsonPatchDocument<Credential> request)
        {
            for (int i = 0; i < request.Operations.Count; i++)
            {
                if (request.Operations[i].op.ToString() == "replace" && request.Operations[i].path.ToString() == "/PasswordSecret")
                {
                    applicationUser = userManager.GetUserAsync(httpContextAccessor.HttpContext.User).Result;

                    var passwordHash = userManager.PasswordHasher.HashPassword(applicationUser, request.Operations[i].value.ToString());
                    request.Replace(e => e.PasswordHash, passwordHash);
                }
            }
            return await base.PatchEntity(id, request);
        }
    }
}

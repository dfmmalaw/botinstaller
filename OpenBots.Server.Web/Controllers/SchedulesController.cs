﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Security;
using OpenBots.Server.ViewModel;
using OpenBots.Server.ViewModel.AgentViewModels;
using OpenBots.Server.WebAPI.Controllers;
using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Reflection.PortableExecutable;
using System.Threading.Tasks;

namespace OpenBots.Server.Web.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class SchedulesController : EntityController<Schedule>
    {
        IScheduleManager manager;
        private IHttpContextAccessor _accessor;
        public SchedulesController(
            IScheduleRepository repository,
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IScheduleManager manager,
            IHttpContextAccessor accessor,
            IHttpContextAccessor httpContextAccessor) : base(repository, userManager, httpContextAccessor, membershipManager)
        {
            this.manager = manager;
            this.manager.SetContext(base.SecurityContext);
            _accessor = accessor;
        }

        /// <summary>
        /// Provides a list of all Schedules
        /// </summary>
        /// <response code="200">OK,a Paginated list of all Schedules</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>Paginated list of all Schedules </returns>
        [HttpGet]
        [ProducesResponseType(typeof(PaginatedList<Schedule>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<Schedule> Get(
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0
            )
        {
            return base.GetMany();
        }

        /// <summary>
        /// Provides an Schedule details for a particular Schedule Id.
        /// </summary>
        /// <param name="id">Schedule id</param>
        /// <response code="200">OK, If an Schedule exists with the given Id.</response>
        /// <response code="304">Not modified</response>
        /// <response code="400">BadRequest,If Schedule id is not in proper format or proper Guid.</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">NotFound, when no Schedule exists for the given Schedule id</response>
        /// <returns>Schedule details for the given Id</returns>
        [HttpGet("{id}", Name = "GetSchedule")]
        [ProducesResponseType(typeof(PaginatedList<Schedule>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                return await base.GetEntity(id);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Adds a new Schedule to the existing Schedules
        /// </summary>
        /// <remarks>
        /// Adds the Schedule with unique Schedule Id to the existing Schedules
        /// </remarks>
        /// <param name="value"></param>
        /// <response code="200">OK,new Schedule created and returned</response>
        /// <response code="400">BadRequest,When the Schedule value is not in proper format</response>
        /// <response code="403">Forbidden, unauthorized access</response>
        /// <response code="409">Conflict,concurrency error</response> 
        /// <response code="422">UnprocessabileEntity,when a duplicate record is being entered.</response>
        /// <returns> newly created unique Schedule Id with route name </returns>
        [HttpPost]
        [ProducesResponseType(typeof(Schedule), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post([FromBody] ScheduleViewModel request)
        {
            if (request == null)
            {
                ModelState.AddModelError("Save", "No data passed");
                return BadRequest(ModelState);
            }

            Guid entityId = Guid.NewGuid();

            try
            {
                Schedule requestObj = new Schedule();
                requestObj.Id = entityId;
                requestObj.Name = request.Name;
                requestObj.AgentName = request.AgentName;
                requestObj.CRONExpression = request.CRONExpression;
                requestObj.LastExecution = request.LastExecution;
                requestObj.NextExecution = request.NextExecution;
                requestObj.IsDisabled = request.IsDisabled;
                requestObj.ProjectId = request.ProjectId;
                requestObj.TriggerName = request.TriggerName;
                requestObj.Recurrence = request.Recurrence;
                requestObj.StartingType = request.StartingType;
                requestObj.StartJobOn = request.StartJobOn;
                requestObj.RecurrenceUnit = request.RecurrenceUnit;
                requestObj.JobRecurEveryUnit = request.JobRecurEveryUnit;
                requestObj.EndJobOn = request.EndJobOn;
                requestObj.EndJobAtOccurence = request.EndJobAtOccurence;
                requestObj.NoJobEndDate = request.NoJobEndDate;
                requestObj.Status = request.Status;
                requestObj.ExpiryDate = request.ExpiryDate;
                requestObj.StartDate = request.StartDate;

                return await base.PostEntity(requestObj);             
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }


        /// <summary>
        /// Updates an Schedule 
        /// </summary>
        /// <remarks>
        /// Provides an action to update an Schedule, when Schedule id and the new details of Schedule are given
        /// </remarks>
        /// <param name="id">Schedule Id,produces Bad request if Id is null or Id's don't match</param>
        /// <param name="value">Schedule details to be updated</param>
        /// <response code="200">OK, If the Schedule details for the given Schedule Id has been updated.</response>
        /// <response code="400">BadRequest,if the Schedule Id is null or Id's don't match</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>OK response with the updated value</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string id, [FromBody] ScheduleViewModel request)
        {
            try
            {
                Guid entityId = new Guid(id);
                
                var existingSchedule = repository.GetOne(entityId);
                if (existingSchedule == null) return NotFound();

                existingSchedule.Name = request.Name;
                existingSchedule.AgentName = request.AgentName;
                existingSchedule.CRONExpression = request.CRONExpression;
                existingSchedule.LastExecution = request.LastExecution;
                existingSchedule.NextExecution = request.NextExecution;
                existingSchedule.IsDisabled = request.IsDisabled;
                existingSchedule.ProjectId = request.ProjectId;
                existingSchedule.TriggerName = request.TriggerName;
                existingSchedule.Recurrence = request.Recurrence;
                existingSchedule.StartingType = request.StartingType;
                existingSchedule.StartJobOn = request.StartJobOn;
                existingSchedule.RecurrenceUnit = request.RecurrenceUnit;
                existingSchedule.JobRecurEveryUnit = request.JobRecurEveryUnit;
                existingSchedule.EndJobOn = request.EndJobOn;
                existingSchedule.EndJobAtOccurence = request.EndJobAtOccurence;
                existingSchedule.NoJobEndDate = request.NoJobEndDate;
                existingSchedule.Status = request.Status;
                existingSchedule.ExpiryDate = request.ExpiryDate;
                existingSchedule.StartDate = request.StartDate;

                return await base.PutEntity(id, existingSchedule);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Schedule", ex.Message);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Deletes an Schedule with a specified id from the Schedules.
        /// </summary>
        /// <param name="id">Schedule ID to be deleted- throws BadRequest if null or empty Guid/</param>
        /// <response code="200">OK,when Schedule is softdeleted,( isDeleted flag is set to true in DB) </response>
        /// <response code="400">BadRequest,If Schedule Id is null or empty Guid</response>
        /// <response code="403">Forbidden </response>
        /// <returns>OK response with deleted value </returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(string id)
        {
            return await base.DeleteEntity(id);
        }

        /// <summary>
        /// Updates partial details of Schedule.
        /// </summary>
        /// <param name="id">Schedule identifier</param>
        /// <param name="value">Value of the Schedule to be updated.</param>
        /// <response code="200">OK,If update of Schedule is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Schedule values has been updated</returns>
        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id,
            [FromBody] JsonPatchDocument<Schedule> request)
        {
            return await base.PatchEntity(id, request);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace OpenBots.Server.WebAPI.Controllers.Core
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class LookupValuesController : EntityController<LookupValue>
    {
        public LookupValuesController(
            ILookupValueRepository repository,
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IOrganizationManager organizationManager,
            IHttpContextAccessor httpContextAccessor) : base(repository,  userManager, httpContextAccessor, membershipManager)
        {           
        }

        /// <summary>
        /// Provides a list of all LookupValues
        /// </summary>
        /// <response code="200">OK,a Paginated list of all LookupValues</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// /// <returns>Paginated list of all LookupValues </returns>
        [HttpGet]
        //        [ProducesResponseType(typeof(LookupValue), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(PaginatedList<LookupValue>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<LookupValue> Get(
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0
            )
        {
            return base.GetMany();
        }

        /// <summary>
        /// Provides an LookupValue details for a particular LookupValue Id.
        /// </summary>
        /// <param name="codeType">LookupValue id</param>
        /// <response code="200">OK, If an LookupValue exists with the given Id.</response>
        /// <response code="304">Not modified</response>
        /// <response code="400">BadRequest,If LookupValue id is not in proper format or proper Guid.</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">NotFound, when no LookupValue exists for the given LookupValue id</response>
        /// <returns>LookupValue details for the given Id</returns>
        [HttpGet("{codeType}", Name = "GetLookupValue")]
        [ProducesResponseType(typeof(PaginatedList<LookupValue>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string codeType)
        {
            try
            {
                var lookup = repository.Find(null, p => p.CodeType.Equals(codeType, StringComparison.OrdinalIgnoreCase));
                if (lookup == null) {
                    ModelState.AddModelError("", "");
                    return BadRequest(ModelState);
                }

                var orderedItems = lookup.Items.OrderBy(x => x.SequenceOrder).ToList();
                lookup.Items.Clear();
                lookup.Items.AddRange(orderedItems);

                return Ok(lookup);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Adds a new LookupValue to the existing LookupValues
        /// </summary>
        /// <remarks>
        /// Adds the LookupValue with unique LookupValue Id to the existing LookupValues
        /// </remarks>
        /// <param name="value"></param>
        /// <response code="200">OK,new LookupValue created and returned</response>
        /// <response code="400">BadRequest,When the LookupValue value is not in proper format</response>
        /// <response code="403">Forbidden, unauthorized access</response>
        ///<response code="409">Conflict,concurrency error</response> 
        /// <response code="422">UnprocessabileEntity,when a duplicate record is being entered.</response>
        /// <returns> newly created unique LookupValue Id with route name </returns>
        [HttpPost]
        [ProducesResponseType(typeof(LookupValue), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post([FromBody] LookupValue value)
        {
            if (value == null)
            {
                ModelState.AddModelError("Save", "No data passed");
                return BadRequest(ModelState);
            }

            Guid entityId = Guid.NewGuid();
            if (value.Id == null || value.Id.HasValue || value.Id.Equals(Guid.Empty))
                value.Id = entityId;

            try
            {
                return await base.PostEntity(value);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }


        /// <summary>
        /// Updates an LookupValue 
        /// </summary>
        /// <remarks>
        /// Provides an action to update an LookupValue, when LookupValue id and the new details of LookupValue are given
        /// </remarks>
        /// <param name="id">LookupValue Id,produces Bad request if Id is null or Id's don't match</param>
        /// <param name="value">LookupValue details to be updated</param>
        /// <response code="200">OK, If the LookupValue details for the given LookupValue Id has been updated.</response>
        /// <response code="400">BadRequest,if the LookupValue Id is null or Id's don't match</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>OK response with the updated value</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string id, [FromBody] LookupValue value)
        {
            return await base.PutEntity(id, value);

        }

        /// <summary>
        /// Deletes an LookupValue with a specified id from the LookupValues.
        /// </summary>
        /// <param name="id">LookupValue ID to be deleted- throws BadRequest if null or empty Guid/</param>
        /// <response code="200">OK,when LookupValue is softdeleted,( isDeleted flag is set to true in DB) </response>
        /// <response code="400">BadRequest,If LookupValue Id is null or empty Guid</response>
        /// <response code="403">Forbidden </response>
        /// <returns>OK response with deleted value </returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(string id)
        {
            return await base.DeleteEntity(id);
        }

        /// <summary>
        /// Updates partial details of LookupValue.
        /// </summary>
        /// <param name="id">LookupValue identifier</param>
        /// <param name="value">Value of the LookupValue to be updated.</param>
        /// <response code="200">OK,If update of LookupValue is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial LookupValue values has been updated</returns>

        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id,
            [FromBody] JsonPatchDocument<LookupValue> value)
        {
            return await base.PatchEntity(id, value);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Security;
using OpenBots.Server.WebAPI.Controllers;
using OpenBots.Server.Business;
using OpenBots.Server.Model;
using OpenBots.Server.ViewModel;
using YamlDotNet.Core.Tokens;
using System.Linq;
using Microsoft.EntityFrameworkCore.Internal;

namespace OpenBots.Server.Web.Controllers
{
    /// <summary>
    /// AuditLog Controller
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class AuditLogsController : EntityController<AuditLog>
    {
        /// <summary>
        /// Audit Log Controller Constructor
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="membershipManager"></param>
        /// <param name="userManager"></param>
        /// <param name="httpContextAccessor"></param>
        public AuditLogsController(
            IAuditLogRepository repository,
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor) : base(repository, userManager, httpContextAccessor, membershipManager)
        {
        }

        /// <summary>
        /// Provides a list of all audit logs
        /// </summary>
        /// <response code="200">Ok, a Paginated list of all Audit Logs</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden, unauthorized access</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>Paginated list of all audit logs</returns>
        // GET: /api/v1/auditlogs
        [HttpGet]
        [ProducesResponseType(typeof(PaginatedList<AuditLog>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<AuditLog> Get(
        [FromQuery(Name = "$filter")] string filter = "",
        //Order audit logs by most recent date
        [FromQuery(Name = "$orderby")] string orderBy = "UpdatedOn desc",
        [FromQuery(Name = "$top")] int top = 100,
        [FromQuery(Name = "$skip")] int skip = 0
        )
        {
            return base.GetMany();
        }

        /// <summary>
        /// Provides a list of audit logs filtered by updated by (person id), service name, method name, or timestamp
        /// </summary>
        /// <response code="200">Ok, a Paginated list of all Audit Logs</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden, unauthorized access</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>Paginated list of all audit logs</returns>
        /// <returns>Paginated list of audit logs by filter</returns>
        // GET: /api/v1/auditlogs/results?$filter=ServiceName eq 'Service1' or MethodName eq 'Service1' or UpdatedBy eq 'Service1' or UpdatedOn eq 'Service1'
        [HttpGet("results")]
        [ProducesResponseType(typeof(PaginatedList<AuditLog>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<AuditLog> GetResults(
        //Filters will be put in on Client side (ServiceName, MethodName, UpdatedBy, UpdatedOn)
        [FromQuery(Name = "$filter")] string filter = "",
        //Order audit logs by most recent date
        [FromQuery(Name = "$orderby")] string orderBy = "UpdatedOn desc",
        [FromQuery(Name = "$top")] int top = 100,
        [FromQuery(Name = "$skip")] int skip = 0
        )
        {
            return base.GetMany();
        }

        /// <summary>
        /// Provides additional details on a specific Audit Log
        /// </summary>
        /// <param name="id">Audit Log Id</param>
        /// <response code="200">OK, If an Audit Log exists with the given Id</response>
        /// <response code="304">NotModified</response>
        /// <response code="400">BadRequest, If Audit Log Id is not in proper format or Guid</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">NotFound, When no Audit Log exists for the given Audit Log Id</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>Audit Log details for the given Id</returns>
        // GET /api/v1/auditlogs/5
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PaginatedList<AuditLog>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> GetDetails(Guid id)
        {
            try
            {
                return await base.GetEntity(id.ToString());
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Provides a list of all audit logs name
        /// </summary>
        /// <response code="200">Ok, a list of all Audit Logs</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden, unauthorized access</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>List of all audit logs</returns>
        // GET: /api/v1/auditlogs/AuditLogsLookup
        [HttpGet("AuditLogsLookup")]
        [ProducesResponseType(typeof(List<AuditLogsLookupViewModel>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public AuditLogsLookupViewModel AllAuditLogs()
        {
            var response = repository.Find(null, x => x.IsDeleted == false);
            AuditLogsLookupViewModel auditLogsList = new AuditLogsLookupViewModel();

            if (response != null)
            {
                auditLogsList.ServiceNameList = new List<string>();                
                foreach (var item in response.Items)
                {
                    auditLogsList.ServiceNameList.Add(item.ServiceName);

                }

                auditLogsList.ServiceNameList = auditLogsList.ServiceNameList.Distinct().ToList();
            }

            return auditLogsList;
        }
    }
}

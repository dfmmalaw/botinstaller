﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using OpenBots.Server.Business;
using OpenBots.Server.Security;
using Microsoft.AspNetCore.Authorization;
using System.IO;

namespace OpenBots.Server.WebAPI.Controllers
{
    [Route("api/v1/People/{personId}/[controller]")]
    [ApiController]
    [Authorize]
    public class EmailVerificationsController : EntityController<EmailVerification>
    {
        readonly IEmailManager emailSender;

        public EmailVerificationsController(
            IEmailVerificationRepository repository, 
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor,
            IEmailManager emailSender) : base(repository,  userManager, httpContextAccessor, membershipManager)
        {
            this.emailSender = emailSender;
        }


        /// <summary>
        /// Gets all the email verfication details for a particular person 
        /// </summary>
        /// <remarks>Gets all the details - email address,Is verified,Verification email count,Verfication code,expires on,Is verfication email sent,sent on for a person.</remarks>
        /// <param name="personId">Person IDentifier</param>
        /// <param name="filter">search using a field</param>
        /// <param name="orderBy">sort ascending or  descending</param>
        /// <param name="top">fetches the first 100 rows</param>
        /// <param name="skip">skips the 'n' number of rows</param>
        /// <response code="200">OK, Paginated list of the email verfications for a particular person.</response>
        /// <response code="400">BadRequest,If the Person ID is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,unauthorized access by user</response>
        /// <response code="404">NotFound</response>
        /// <response code="422">Unprocessable entity,validation error.</response>
        /// <returns>Paginated list of all the email verfications for a particular person.</returns>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(EmailVerification), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<EmailVerification> Get(
            [FromRoute] string personId,
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0)
        {
            return base.GetMany(personId);
        }


        /// <summary>
        /// Provides the email verification details for a particular id
        /// </summary>
        /// <remarks>Gets all the details - email address,Is verified,Verification email count,Verfication code,expires on,Is verfication email sent,sent on for a particular person.</remarks>
        /// <param name="personId">Person identifier</param>
        /// <param name="id">Email verfication id</param>
        /// <response code="200">OK,If email verfication detail is available with the given id .></response>
        /// <response code="400">BadRequest, If the Person Id/Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access by user.</response>
        /// <response code="404">NotFound</response>
        /// <response code="422">Unprocessable entity,validation error.</response>
        /// <returns>Ok response, If an email verification unit details  has been found with the particular id.</returns>
        [HttpGet("{id}", Name = "GetEmailVerification")]
        [ProducesResponseType(typeof(EmailVerification), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string personId, string id)
        {
            return await base.GetEntity(id);
        }


        /// <summary>
        /// Adds email verfication detail for a person
        /// </summary>
        /// <remarks>adds all the details - email address,Is verified,Verification email count,Verfication code,expires on,Is verfication email sent,sent on for a particular person.</remarks>
        /// <param name="personId">Person Identifier</param>
        /// <param name="value">Value of the email verfication detail to be added.</param>
        /// <response code="200">OK,If the email verfication  has been added successfully for a person</response>
        /// <response code="400">BadRequest,If the Person Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access by user.</response>
        /// <response code="409">Conflict,concurrency error </response>
        /// <response code="422">UnprocessableEntity,If a duplicate record is being entered.</response> 
        /// <returns>Ok response,If the email verification has been added for a person. </returns>
        [HttpPost]
        [ProducesResponseType(typeof(EmailVerification), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post(string personId, [FromBody] EmailVerification value)
        {
            value.PersonId = new Guid(personId);
            //Check the email address in verification email table
            var emailVerification = repository.Find(null, p=> p.Address.Equals(value.Address, StringComparison.OrdinalIgnoreCase))?.Items?.FirstOrDefault();
            
            //If not null then email address already exists
            if (emailVerification != null) {
                ModelState.AddModelError("EmailAddress", "email address already in use");
                return BadRequest(ModelState);
            }

            value.IsVerified = false;
            var verificationEmail = await base.PostEntity(value).ConfigureAwait(false);

            try
            {
                //TODO - check email address is mapped to the logged in user

                //Resending 
                byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
                byte[] key = applicationUser.PersonId.ToByteArray();
                string token = Convert.ToBase64String(time.Concat(key).ToArray());

                string confirmationLink = Url.Action("ConfirmEmailAddress",
                                                    "Auth", new
                                                    {
                                                        emailAddress = value.Address,
                                                        token = token
                                                    }, protocol: HttpContext.Request.Scheme);

                string emailBody = "";

                using (StreamReader reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Email/confirm-en.html")))
                {
                    emailBody = reader.ReadToEnd();
                    emailBody = emailBody.Replace("^Confirm^", confirmationLink);
                }


                var subject = "Confirm your email address at " + Constants.PRODUCT;
                await emailSender.SendEmailAsync(value.Address, subject, emailBody).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }

            return verificationEmail;
        }


        /// <summary>
        /// Updates the email verfication details for a person
        /// </summary>
        /// <remarks>updates all the details - email address,Is verified,Verification email count,Verfication code,expires on,Is verfication email sent,sent on for a particular person.</remarks>
        /// <param name="personId">Person identifier</param>
        /// <param name="id">Email verfication identifier</param>
        /// <param name="value">value to be updated for a person</param>
        /// <response code="200">OK, If the email verification value has been updated for the Person.</response>
        /// <response code="400">BadRequest,If the Person Id/email verification  Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access</response>
        /// <response code="409">Conflict,concurrency error.</response>
        /// <response code="422">UnprocessableEntity,Validation error.</response>
        /// <returns>OK response,If the value for email verification id has been updated with the new value.</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string personId, string id, [FromBody] EmailVerification value)
        {
            value.PersonId = new Guid(personId);
            return await base.PutEntity(id, value);
        }


        /// <summary>
        /// Deletes email verification details
        /// </summary>
        /// <remarks>deletes all the details - email address,Is verified,Verification email count,Verfication code,expires on,Is verfication email sent,sent on for a particular person.</remarks>
        /// <param name="id">Email verification identifier</param>
        /// <response code="200">OK, if the email verification  with the given id has been deleted.</response>
        /// <response code="400">BadRequest,if the id is not provided or not proper Guid.</response>
        /// <response code="403">Unauthorized access, if the user doesn't have permission to delete the Organization member.</response>
        /// <response code="422">Unprocessable Entity,Validation error.</response>
        /// <returns>OK response, if the email verification  with the given id has been deleted.</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(string id)
        {
            return await base.DeleteEntity(id);
        }

        /// <summary>
        /// Updates the portion of email verification details
        /// </summary>
        /// <param name="id">Email verification identifier</param>
        /// <param name="value">Values to be updated</param>
        /// <response code="200">OK,If update of values of email verfication is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial email verification values has been updated.</returns>
        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id, [FromBody]JsonPatchDocument<EmailVerification> value)
        {
            return await base.PatchEntity(id, value);
        }
    }
}
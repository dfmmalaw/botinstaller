﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using OpenBots.Server.Business;
using OpenBots.Server.Security;
using Microsoft.AspNetCore.Authorization;

namespace OpenBots.Server.WebAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class PeopleController : EntityController<Person>
    {
       // readonly ApplicationIdentityUserManager userManager;

        public PeopleController(
            IPersonRepository repository, 
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor,
            IMembershipManager membershipManager
            ) : base(repository, userManager, httpContextAccessor, membershipManager)
        {


        }

        /// <summary>
        /// Gets all the people 
        /// </summary>
        /// <remarks>
        /// returns a paginated list of all the people with their names,company and department
        /// </remarks>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="top">returns the top 100 </param>
        /// <param name="skip"></param>
        /// <response code="400">BadRequest: If it is not a proper Guid</response>
        /// <response code="200">OK: Paginated List of all people .</response>
        /// <response code="403">Forbidden: Person ID passed in the URL is not the same as the currently logged in user</response>
        /// <response code="422">UnprocessableEntity,Validation error</response>
        /// <returns>returns a paginated list of all the people with their names,company and department</returns>
        [HttpGet]
        [ProducesResponseType(typeof(Person), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public PaginatedList<Person> Get(
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0)
        {
            return base.GetMany();
        }


        /// <summary>
        /// Retrieves the person details for a particular user.
        /// </summary>
        /// <param name="id">People identifier</param>
        /// <response code="200">OK,If people detail is available for the given id .></response>
        /// <response code="400">BadRequest, If the people Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Person ID passed in the URL is not the same as the currently logged in user</response>
        /// <response code="404">NotFound</response>
        /// <response code="422">Unprocessable entity,validation error.</response>
        /// <returns>Ok response, If Person details  has been found for the particular id.</returns> 
        [HttpGet("{id}", Name = "GetPerson")]
        [ProducesResponseType(typeof(Person), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Get(string id)
        {
            return await base.GetEntity(id);
        }


        /// <summary>
        /// Adds a Person to a department 
        /// </summary>
        /// <param name="value"></param>
        /// <response code="200">OK,If the person details  has been added for a department</response>
        /// <response code="400">BadRequest,if improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access by user.</response>
        /// <response code="409">Conflict,concurrency error</response>
        /// <response code="422">UnprocessableEntity,If a duplicate record is being entered, Person record already exists</response> 
        /// <returns>Ok response,If the person detils has been added for a person. </returns>
        [HttpPost]
        [ProducesResponseType(typeof(Person), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post([FromBody] Person value)
        {
            return await base.PostEntity(value);
        }


        /// <summary>
        /// Updates person details in  a department
        /// </summary>
        /// <param name="id">person id</param>
        /// <param name="value">Details to be updated</param>
        /// <response code="200">OK,If the details of person has been updated.</response>
        /// <response code="400">BadRequest,if person id is not provided/improper Guid.</response>
        /// <response code="403">Forbidden,unauthorized access by user</response>
        /// <response code="409">Conflict,concurrency error</response>
        /// <response code="422">Unprocessable entity,validation error.</response>
        /// <returns>Ok response, if the person detials has been updated for the given person id.</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Put(string id, [FromBody] Person value)
        {
            //Use logged in User Context
            if (applicationUser == null)
            {
                return Unauthorized();
            }

            var existingPerson = repository.GetOne(applicationUser.PersonId);
            if (existingPerson == null) return NotFound();
            
            existingPerson.Name = value.Name;

            applicationUser.Name = value.Name;
            await userManager.UpdateAsync(applicationUser).ConfigureAwait(false);           


            return await base.PutEntity(id, existingPerson).ConfigureAwait(false);
        }   

        /// <summary>
        /// Deletes the person details
        /// </summary>
        /// <param name="id">Person id</param>
        /// <response code="200">OK, if the person details for the particular id has been deleted.</response>
        /// <response code="400">BadRequest,If person id is not provided or improper guid.</response>
        /// <response code="403">Forbidden,Person ID passed in the URL is not the same as the currently logged in user</response>
        /// <returns>Ok response,if the person details for the particular id has been deleted.</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Delete(string id)
        {
            return await base.DeleteEntity(id);
        }

        /// <summary>
        /// Updates Person details 
        /// </summary>
        /// <param name="id">People identifier</param>
        /// <param name="value">Firstname/Lastname/Company/Department to be updated</param>
        /// <response code="200">OK,If update of Person detail is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Person values has been updated</returns>
        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id, [FromBody]JsonPatchDocument<Person> value)
        {
            return await base.PatchEntity(id, value);
        }
    }
}
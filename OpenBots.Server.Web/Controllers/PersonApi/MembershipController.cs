﻿    using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using OpenBots.Server.Model.Membership;
using OpenBots.Server.Security;
using OpenBots.Server.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OpenBots.Server.Web;

namespace OpenBots.Server.WebAPI.Controllers.PersonApi
{
    [Route("api/v1/People/{personId}/Organizations")]
    [ApiController]
    [Authorize]
    public class MembershipController : EntityController<Organization>
    {

        IOrganizationMemberRepository orgMemberRepository;
        //IPersonEmailRepository emailRepository;
        IMembershipManager membershipManager;

        public MembershipController(
            IOrganizationRepository repository, 
            IOrganizationMemberRepository orgMemberRepository,
            IMembershipManager membershipManager, 
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor) : base(repository,  userManager, httpContextAccessor, membershipManager)
        {
            this.membershipManager = membershipManager;
            this.orgMemberRepository = orgMemberRepository;


            this.membershipManager.SetContext(base.SecurityContext);
            this.orgMemberRepository.SetContext(base.SecurityContext);

        }

        /// <summary>
        /// Provides a list of Organizations that can be presented to the user as Suggestions to apply for access request.
        /// </summary>
        /// <remarks>
        /// This method will return all Organizations that are visible to users with a certain email domain.
        /// All the Emails of the user are matched to the Allowed Domains of Organiation.
        /// </remarks>
        /// <param  name="personId" >ID of the currently logged in user. If the userID is not the same, then the request will be rejected</param>
        /// <returns>Paginated List of Organizations that are being suggested. The Object Organization will not have any child objects</returns>
        /// <response code="400">BadRequest: If no personId is provided or it is not a proper Guid</response>
        /// <response code="200">OK: Paginated List of Organizations that are being suggested. The Object Organization will not have any child objects</response>
        /// <response code="403">Forbidden: Person ID passed in the URL is not the same as the currently logged in user</response>
        [HttpGet("Suggestions")]
        [ProducesResponseType(typeof(PaginatedList<OrganizationListing>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ServiceBadRequest), StatusCodes.Status400BadRequest)]
        [Produces("application/json")]
        public async Task<IActionResult> GetSuggestions(
            string personId)
        {
            Guid personGuid = Guid.Empty;
            if (!Guid.TryParse(personId, out personGuid))
            {
                this.ModelState.AddModelError(nameof(personId), "");
                return BadRequest(ModelState);
            }

            // TODO: Bring this back when security is implemented.
            //if (personGuid != securityContext.PersonId)
            //    return Forbid();


            try
            {
                var listOfOrg = membershipManager.Suggestions(personGuid);
                return Ok(listOfOrg);
            }
            catch(Exception ex)
            {
                return ex.GetActionResult();
            }
           
        }

        /// <summary>
        /// Get Cards for Organizations I have access to.
        /// </summary>
        /// <param name="personId">PErson Identifier</param>
        /// <response code="200">Ok,Paginated list of ORganizations that user have access to</response>
        /// <response code="400">BadRequest,If the person Id is not entered or improper Guid</response>
        /// <response code="403">Forbidden: Person ID passed in the URL is not the same as the currently logged in user</response>
        /// <response code="404">NotFound</response>
        /// <response code="422">Unprocessable entity,Validation error</response>
        /// <returns>OrganizationCards for all Organizations user has access to.</returns>
        [HttpGet]
        [ProducesResponseType(typeof(PaginatedList<OrganizationCard>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public async Task<IActionResult> Get(string personId)
        {
            try
            {
                //Guid personGuid = Guid.Parse(personId);
                Guid personGuid = SecurityContext.PersonId;
                return Ok(membershipManager.MyOrganizations(personGuid, true));
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Get Organizations I have access to.
        /// </summary>
        /// <param name="personId">Person identifier</param>
        /// <param name="startsWith">Search criteria</param>
        /// <param name="skip">no of records to be skipped</param>
        /// <param name="take"></param>
        /// <response code="200">Ok,Paginated listing of all organizations the user has access to.</response>
        /// <response code="400">BadRequest, If the Person Id is not provided or improper Guid</response>
        /// <response code="403">Forbidden: Person ID passed in the URL is not the same as the currently logged in user</response>
        /// <response code="404">NotFound</response>
        /// <response code="422">UnprocessableEntity,validation error</response>
        /// <returns>OrganizationCards for all Organizations user has access to.</returns>
        [HttpGet("Search")]
        [ProducesResponseType(typeof(PaginatedList<OrganizationListing>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        [Produces("application/json")]
        public async Task<IActionResult> Get(
            string personId, 
            [FromQuery] string startsWith = "",
            [FromQuery(Name ="$skip")] int skip = 0,
            [FromQuery(Name = "$take")] int take = 10)
        {
            try
            {
                //Guid personGuid = Guid.Parse(personId);
                Guid personGuid = SecurityContext.PersonId;
                return Ok(membershipManager.Search(personGuid, startsWith, skip, take, false));
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }



        /// <summary>
        /// Request for Joining an Organization
        /// </summary>
        /// <param name="personId">UserID of the person logged in.</param>
        /// <param name="id">Organization ID that the user wants to be a member of </param>
        /// <response code="200">OK,if the user has been successfully added to the organization</response>
        /// <response code="400">BadRequest,If no personId is provided or it is not a proper Guid.</response>
        /// <response code="400">BadRequest,if no Organization Id is provided or it is not a proper Guid.</response> 
        /// <returns></returns>
        [HttpPost("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> Post(string personId, string id)
        {
            Guid personGuid = Guid.Empty;
            if (!Guid.TryParse(personId, out personGuid))
            {
                ModelState.AddModelError("JoinOrganization", "Person Id has errors");
                return BadRequest(ModelState);
            }
            Guid orgGuid = Guid.Empty;
            if (!Guid.TryParse(id, out orgGuid))
            {
                ModelState.AddModelError("JoinOrganization", "Organization Id has errors");
                return BadRequest(ModelState);
            }

            try
            {
                if (SecurityContext == null)
                {
                    ModelState.AddModelError("JoinOrganization", "Join organization failed. Logged in user no longer exists in system.");
                    return BadRequest(ModelState);
                }

                var accessRequest =  membershipManager.JoinOrganization(personGuid, orgGuid);
                if (accessRequest == null)
                {
                    ModelState.AddModelError("JoinOrganization", "Organization request pending");
                    return BadRequest(ModelState);
                }

                return Ok(accessRequest);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Revoke admin permission of an admin
        /// </summary>
        /// <param name="personId">user who's admin permission needs to be revoked</param>
        /// <param name="id">Organization Id</param>
        /// <returns></returns>
        [HttpPut("{id}/RevokeAdmin")]
        [Produces("application/json")]
        public async Task<IActionResult> RevokeAdmin(string personId, string id)
        {
            Guid personGuid = Guid.Empty;
            if (!Guid.TryParse(personId, out personGuid))
            {
                ModelState.AddModelError("RevokeAdminPermission", "Person Id has errors");
                return BadRequest(ModelState);
            }
            Guid orgGuid = Guid.Empty;
            if (!Guid.TryParse(id, out orgGuid))
            {
                ModelState.AddModelError("RevokeAdminPermission", "Organization Id has errors");
                return BadRequest(ModelState);
            }

            //Cannot revoke for self i.e. logged in user
            if (SecurityContext.PersonId == personGuid) {
                ModelState.AddModelError("RevokeAdminPermission", "Cannot revoke admin permission for self");
                return BadRequest(ModelState);
            }

            try
            {
                var orgMember = membershipManager.RevokeAdminPermisson(orgGuid, personGuid, SecurityContext);
                return Ok(orgMember);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }


        /// <summary>
        /// Grant admin permission to non-admin users of the organization
        /// </summary>
        /// <param name="personId">user who's admin permission needs to be grant</param>
        /// <param name="id">Organization Id</param>
        /// <returns></returns>
        [HttpPut("{id}/GrantAdmin")]
        [Produces("application/json")]
        public async Task<IActionResult> GrantAdmin(string personId, string id)
        {
            Guid personGuid = Guid.Empty;
            if (!Guid.TryParse(personId, out personGuid))
            {
                ModelState.AddModelError("GrantAdmin", "Person Id has errors");
                return BadRequest(ModelState);
            }
            Guid orgGuid = Guid.Empty;
            if (!Guid.TryParse(id, out orgGuid))
            {
                ModelState.AddModelError("GrantAdmin", "Organization Id has errors");
                return BadRequest(ModelState);
            }

            //Cannot revoke for self i.e. logged in user
            if (SecurityContext.PersonId == personGuid)
            {
                ModelState.AddModelError("GrantAdmin", "Cannot grant admin permission for self");
                return BadRequest(ModelState);
            }

            try
            {
                //Context had to used in controller and not passed to manager
                //TODO - remove context from manager
                //check if you are admin to revoke the admin rights of other users 
                bool IsOrgAdmin = Array.Exists<Guid>(SecurityContext.OrganizationId, o => o.CompareTo(orgGuid) == 0);

                if (IsOrgAdmin)
                {
                    var orgMember = membershipManager.GrantAdminPermission(orgGuid, personGuid);
                    return Ok(orgMember);
                }
                else {
                    ModelState.AddModelError("GrantAdmin", "Only organization admin can grant admin rights");
                    return BadRequest(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Leave a particular organization
        /// </summary>
        /// <param name="personId">ID of the currently logged in user. If the userID is not the same, then the request will be rejected</param>
        /// <param name="id">ID of the organization that the user wants to delete the membership from</param>
        /// <returns>OK response if the delete is successful or BadRequest if the userID or Organization ID is not provided/ not a proper Guid</returns>
        /// <response code="400">BadRequest,If no personId is provided or it is not a proper Guid</response>
        /// <responce code="200">Ok, when membershop to Organization has been deleted for the logged in user.</responce>
        /// <returns>Ok, if the membership to the organization is deleted for the user.</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Delete(string personId, string id)
        {
          
            Guid personGuid = Guid.Empty;
            if (!Guid.TryParse(personId, out personGuid))
            {
                ModelState.AddModelError("Delete", "Person Id has errors");
                return BadRequest(ModelState);
            }

            Guid orgGuid = Guid.Empty;
            if (!Guid.TryParse(id, out orgGuid))
            {
                ModelState.AddModelError("Delete", "Organization Id has errors");
                return BadRequest(ModelState);
            }

            var OrgMembers = orgMemberRepository.Find(null, a => a.OrganizationId == orgGuid)?.Items;
            var OrgMemberCount = OrgMembers?.Count;
            var OrgAdminCount = OrgMembers?.Where(a => a.IsAdministrator == true)?.Count();

            var member = OrgMembers?.Where(p => p.PersonId == personGuid).FirstOrDefault();

            if (member?.IsAdministrator == true)
            {
                if (OrgAdminCount < 2)
                {
                    ModelState.AddModelError("ExitOrg", "Sorry - Unable to exit from the organization since you are the only Admin of this organization, please designate another Admin before exiting this organization.");
                    return BadRequest(ModelState);
                }
                else if (OrgMemberCount < 2)
                {
                    ModelState.AddModelError("ExitOrg", "Admin cannot exit organization if no other members exist.");
                    return BadRequest(ModelState);
                }
            }

            try
            {
                if (member != null && member.Id.HasValue)
                    orgMemberRepository.Delete(member.Id.Value);
                else
                    throw new UnauthorizedAccessException();

                return Ok();
            }
            catch(Exception ex)
            {
                return ex.GetActionResult();
            }

        }
    }
}
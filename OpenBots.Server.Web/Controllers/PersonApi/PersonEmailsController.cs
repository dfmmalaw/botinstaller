﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Model.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using OpenBots.Server.Business;
using OpenBots.Server.Security;
using Microsoft.AspNetCore.Authorization;

namespace OpenBots.Server.WebAPI.Controllers
{
    [Route("api/v1/People/{personId}/[controller]")]
    [ApiController]
    [Authorize]
    public class PersonEmailsController : EntityController<PersonEmail>
    {
        public PersonEmailsController(
            IPersonEmailRepository repository, 
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor httpContextAccessor) : base(repository, userManager, httpContextAccessor, membershipManager)
        {
        }


        /// <summary>
        /// Gets all the email address and verification id for a particular person
        /// </summary>
        /// <remarks>
        /// returns a paginated list of all the email addresses for a particualr person.
        /// </remarks>
        /// <param name="personId">person identifier</param>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="top">returns the top 100</param>
        /// <param name="skip"></param>
        /// <response code="400">BadRequest: If person id is not provided/improper Guid</response>
        /// <response code="200">OK: Paginated List of all email addresses .</response>
        /// <response code="403">Forbidden: Person ID passed in the URL is not the same as the currently logged in user</response>
        /// <response code="422">UnprocessableEntity,Validation error</response>
        /// <returns>A paginated list of all the email addresses for a particular person.</returns>
        [HttpGet]
        [ProducesResponseType(typeof(PersonEmail), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public PaginatedList<PersonEmail> Get(
            [FromRoute] string personId,
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0)
        {
            return base.GetMany(personId);
        }


        /// <summary>
        /// Gets the email address and verfiication id for a particular email for person
        /// </summary>
        /// <param name="personId">person identifier</param>
        /// <param name="id">email identifier</param>
        /// <response code="200">OK,If email detail is available for the given id .></response>
        /// <response code="400">BadRequest, If the email Id is not provided or improper Guid.</response>
        /// <response code="403">Forbidden,Person ID passed in the URL is not the same as the currently logged in user</response>
        /// <response code="404">NotFound</response>
        /// <response code="422">Unprocessable entity,validation error.</response>
        /// <returns>Ok response, If email details  has been found for the particular id.</returns>
        [HttpGet("{id}", Name = "GetPersonEmail")]
        [ProducesResponseType(typeof(PersonEmail), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Get(string personId, string id)
        {
            return await base.GetEntity(id);
        }


        /// <summary>
        /// Adds a new email id and verification id for a person
        /// </summary>
        /// <param name="personId">person identifier</param>
        /// <param name="value">email details to be added </param>
        /// <response code="200">OK,If the email details  has been added for a department</response>
        /// <response code="400">BadRequest,if person id is not provided/improper Guid.</response>
        /// <response code="403">Forbidden,Unauthorized access by user.</response>
        /// <response code="409">Conflict,concurrency error </response>
        /// <response code="422">UnprocessableEntity,If a duplicate record is being entered, email detials already exists</response> 
        /// <returns>Ok response, if the details has been added</returns>
        [HttpPost]
        [ProducesResponseType(typeof(PersonEmail), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Post(string personId, [FromBody] PersonEmail value)
        {
            value.PersonId = new Guid(personId);
            return await base.PostEntity(value);
        }

        /// <summary>
        /// Updates the email details for person
        /// </summary>
        /// <param name="personId">person identifier</param>
        /// <param name="id">email identifier</param>
        /// <param name="value">value to be updated</param>
        /// <response code="200">OK,If the details of email has been updated.</response>
        /// <response code="400">BadRequest,if person id/email id is not provided/improper Guid.</response>
        /// <response code="403">Forbidden,unauthorized access by user</response>
        /// <response code="409">Conflict,concurrency error</response>
        /// <response code="422">Unprocessable entity,validation error.</response>
        /// <returns>Ok response,if the email details to be updated</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Put(string personId, string id, [FromBody] PersonEmail value)
        {
            value.PersonId = new Guid(personId);
            return await base.PutEntity(id, value);
        }

        /// <summary>
        /// Deletes the email details 
        /// </summary>
        /// <param name="id">email identifier</param>
        /// <response code="200">OK, if the email details for the particular id has been deleted.</response>
        /// <response code="400">BadRequest,If email id is not provided or improper guid.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <returns>Ok response,if the delete is successful.</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Delete(string id)
        {
            return await base.DeleteEntity(id);
        }

        /// <summary>
        /// Updates partial details of Person Email
        /// </summary>
        /// <param name="id">Person Email identifier</param>
        /// <param name="value">Values to be updated</param>
        /// <response code="200">OK,If update of Person Email details is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Person Email values has been updated</returns>
        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id, [FromBody]JsonPatchDocument<PersonEmail> value)
        {
            return await base.PatchEntity(id, value);
        }
    }
}
﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using OpenBots.Server.Business;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Model;
using OpenBots.Server.Model.Core;
using OpenBots.Server.Security;
using OpenBots.Server.ViewModel;
using OpenBots.Server.WebAPI.Controllers;
using System;
using System.Threading.Tasks;

namespace OpenBots.Server.Web.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class QueuesController : EntityController<Queue>
    {
        private IHttpContextAccessor _accessor;
        public QueuesController(
            IQueueRepository repository,
            IMembershipManager membershipManager,
            ApplicationIdentityUserManager userManager,
            IHttpContextAccessor accessor,
            IHttpContextAccessor httpContextAccessor) : base(repository, userManager, httpContextAccessor, membershipManager)
        {
            _accessor = accessor;
        }

        /// <summary>
        /// Provides a list of all Queues
        /// </summary>
        /// <response code="200">OK,a Paginated list of all Queue</response>
        /// <response code="400">BadRequest</response>
        /// <response code="403">Forbidden,unauthorized access</response>        
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>Paginated list of all Queues </returns>
        [HttpGet]
        [ProducesResponseType(typeof(PaginatedList<Queue>), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public PaginatedList<Queue> Get(
            [FromQuery(Name = "$filter")] string filter = "",
            [FromQuery(Name = "$orderby")] string orderBy = "",
            [FromQuery(Name = "$top")] int top = 100,
            [FromQuery(Name = "$skip")] int skip = 0
            )
        {
            return base.GetMany();
        }

        /// <summary>
        /// Provides an Queue details for a particular queue Id.
        /// </summary>
        /// <param name="id">Queue id</param>
        /// <response code="200">OK, If an Queue exists with the given Id.</response>
        /// <response code="304">Not modified</response>
        /// <response code="400">BadRequest,If Queue id is not in proper format or proper Guid.</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">NotFound, when no Queue exists for the given id</response>
        /// <returns>Queue details for the given Id</returns>
        [HttpGet("{id}", Name = "GetQueue")]
        [ProducesResponseType(typeof(Queue), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                return await base.GetEntity(id);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }

        /// <summary>
        /// Adds a new Queue
        /// </summary>
        /// <param name="value"></param>
        /// <response code="200">OK,new Queue created and returned</response>
        /// <response code="400">BadRequest,When the Queue value is not in proper format</response>
        /// <response code="403">Forbidden, unauthorized access</response>
        ///<response code="409">Conflict,concurrency error</response> 
        /// <response code="422">UnprocessabileEntity,when a duplicate record is being entered.</response>
        /// <returns> newly created unique Queue Id with route name </returns>
        [HttpPost]
        [ProducesResponseType(typeof(Queue), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Post([FromBody] QueueViewModel request)
        {
            if (request == null)
            {
                ModelState.AddModelError("Save", "No data passed");
                return BadRequest(ModelState);
            }

            Guid entityId = Guid.NewGuid();
            
            try
            {
                Queue requestObj = new Queue();
                requestObj.Id = entityId;
                requestObj.Name = request.Name;
                requestObj.Description = request.Description;

                return await base.PostEntity(requestObj);
            }
            catch (Exception ex)
            {
                return ex.GetActionResult();
            }
        }


        /// <summary>
        /// Updates an Queue 
        /// </summary>
        /// <remarks>
        /// Provides an action to update an Queue, when Queue id and the new details of Queue are given
        /// </remarks>
        /// <param name="id">Queue Id,produces Bad request if Id is null or Id's don't match</param>
        /// <param name="value">Queue details to be updated</param>
        /// <response code="200">OK, If the Queue details for the given Queue Id has been updated.</response>
        /// <response code="400">BadRequest,if the Queue Id is null or Id's don't match</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">UnprocessableEntity</response>
        /// <returns>OK response with the updated value</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put(string id, [FromBody] QueueViewModel request)
        {
            try
            {
                Guid entityId = new Guid(id);
                
                var existingQueue = repository.GetOne(entityId);
                if (existingQueue == null) return NotFound();

                existingQueue.Name = request.Name;
                existingQueue.Description = request.Description;
                
                return await base.PutEntity(id, existingQueue);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Queue", ex.Message);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Deletes an Queue with a specified id from the Queues.
        /// </summary>
        /// <param name="id">Queue ID to be deleted- throws BadRequest if null or empty Guid/</param>
        /// <response code="200">OK,when Queue is softdeleted,( isDeleted flag is set to true in DB) </response>
        /// <response code="400">BadRequest,If Queue Id is null or empty Guid</response>
        /// <response code="403">Forbidden </response>
        /// <returns>OK response with deleted value </returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(string id)
        {
            return await base.DeleteEntity(id);
        }

        /// <summary>
        /// Updates partial details of Queue.
        /// </summary>
        /// <param name="id">Queue identifier</param>
        /// <param name="value">Value of the Queue to be updated.</param>
        /// <response code="200">OK,If update of Queue is successful. </response>
        /// <response code="400">BadRequest,if the Id is null or Id's dont match.</response>
        /// <response code="403">Forbidden,unauthorized access</response>
        /// <response code="422">Unprocessable entity,validation error</response>
        /// <returns>Ok response, if the partial Queue values has been updated</returns>
        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [Produces("application/json")]
        public async Task<IActionResult> Patch(string id,
            [FromBody] JsonPatchDocument<Queue> request)
        {
            return await base.PatchEntity(id, request);
        }

    }
}

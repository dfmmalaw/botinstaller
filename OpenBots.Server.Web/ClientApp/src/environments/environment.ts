/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiUrl: 'https://openbotsserver-dev.azurewebsites.net/api/v1',
 hubUrl:'https://openbotsserver-dev.azurewebsites.net/notification',
  // hubUrl:'https://localhost:5001/notification', 
  // apiUrl: 'https://localhost:5001/api/v1',
  App_Insight_Key: '7a089d83-dc8c-4683-8e66-f7293173c246',
  isDebug: true,
};

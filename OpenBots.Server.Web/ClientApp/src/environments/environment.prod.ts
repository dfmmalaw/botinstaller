/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
export const environment = {
  production: true,
    apiUrl: 'https://openbotsserver-dev.azurewebsites.net/api/v1',
    hubUrl:'https://openbotsserver-dev.azurewebsites.net/notification',
   
};

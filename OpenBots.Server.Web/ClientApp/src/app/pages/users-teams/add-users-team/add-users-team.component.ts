
import { Component, OnInit, TemplateRef, Inject } from '@angular/core';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EMAIL_PATTERN } from '../../../@auth/components/constants';
import { getDeepFromObject } from '../../../@auth/helpers';
import { NB_AUTH_OPTIONS} from '@nebular/auth';
import { UsersTeamService } from '../users-team.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-add-users-team',
  templateUrl: './add-users-team.component.html',
  styleUrls: ['./add-users-team.component.scss']
})
export class AddUsersTeamComponent implements OnInit {
 
  OrganizationID: string;
  isAdmin: string;
  admin_name :string;
  get_allpeople : any =[]
  AmIAdmin: string;
  userinviteForm: FormGroup;
  submitted = false;
  view_dialog:any;
  toggle :boolean;


  constructor(protected userteamService:UsersTeamService, private formBuilder: FormBuilder, protected router :Router,
    private toastrService: NbToastrService) { }


  
  ngOnInit(): void {
    this.userinviteForm = this.formBuilder.group({
      name: [ '',[Validators.required ,Validators.minLength(2), Validators.maxLength(100)]],
      email: [ '',[Validators.required ,Validators.pattern("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}$")]],
  
  });
  }

    // convenience getter for easy access to form fields
    get f() { return this.userinviteForm.controls; }


  

  gotoadd() {
    this.router.navigate(['/pages/users/add-teams'])
  }
 
 


  onSubmit() {
    this.submitted = true;    
    if (this.userinviteForm.invalid) {
        return;
    }
  this.userteamService.invite_member(this.userinviteForm.value).subscribe(
      () => {
        this.toastrService.success('Invitation Sent  Successfully! ');
        this.userinviteForm.reset();
        this.router.navigate(['pages/users/teams-member'])
      }, (error) => {
        if(error.status == 500){
          this.submitted = false
          // this.toastrService.danger(error.error.errorObject.serviceErrors[0]);
          this.toastrService.danger(error.error.ServiceErrors[0]);
        }
        else {
          this.submitted = false;
          // this.toastrService.danger(error.error.errorObject.serviceErrors[0]);  
          this.toastrService.danger(error.error.ServiceErrors[0]);

        }
    
      });



       
}

onReset() {
    this.submitted = false;
    this.userinviteForm.reset();
}
 
}

import { Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { UsersTeamService } from '../users-team.service';

@Component({
  selector: 'ngx-requests-user',
  templateUrl: './requests-user.component.html',
  styleUrls: ['./requests-user.component.scss']
})
export class RequestsUserComponent implements OnInit {

  OrganizationID: string;
  isAdmin: string;
  admin_name :string;
  get_allpeople : any =[]
  pendingRequestList:any = []
  lengthOfRequest: any;

  constructor(protected userteamService:UsersTeamService,
    private toastrService: NbToastrService) { 
    this.OrganizationID = localStorage.getItem('ActiveOrganizationID');
    this.isAdmin = localStorage.getItem('isAdministrator');
    this.admin_name = localStorage.getItem('ActiveOrgname')
     
  }

  ngOnInit(): void {
    this.getRequests()
  }
 
  /**
   * TODO gets ending approval lists for a specific organization
   * @param null
   * @returns void
   */
  getRequests(): void {
    
    this.userteamService.get_people_pending(this.OrganizationID).subscribe(
      (data: any) => {
         
        this.pendingRequestList = data.items;
        this.lengthOfRequest = data.items.length
      }
    );
  }


   /**
    * TODO Approve Request
    * @param personId: string
    * @returns void
    */
   ApproveRequest(personId: string): void {
    const obj = {
      'organizationId': this.OrganizationID,
      'id': personId
    }
   
    this.userteamService.Approve_member(this.OrganizationID,personId ,obj).subscribe(
      () => {
        this.toastrService.success('You have successfully Approved');
        // this.toastr.success('Request Approved Successfully');
        this.getRequests();
      }
    );
  }


    /**
   * TODO Reject Access Request
   * @param personId: string
   * @returns void
   */
  DenyRequest(personId: string): void {
    const obj = {
      'organizationId': this.OrganizationID,
      'id': personId
    }
    
    this.userteamService.Reject_member(this.OrganizationID,personId, obj).subscribe(
      () => {
        this.toastrService.success('Request Rejected Successfully');
        this.getRequests();
      });
  }
}

import { NgModule } from '@angular/core';
import { UsersTeamsRoutingModule } from './users-teams-routing.module';
import { UsersComponent } from './users/users.component';
import { ThemeModule } from '../../@theme/theme.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RequestsUserComponent } from './requests-user/requests-user.component';
import { UsersTeamService } from './users-team.service';
import { SharedModule } from '../../shared';
import { AddUsersTeamComponent } from './add-users-team/add-users-team.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [UsersComponent, RequestsUserComponent, AddUsersTeamComponent],
  imports: [
    SharedModule,
    UsersTeamsRoutingModule,
    ThemeModule,
    ReactiveFormsModule,
    NgxPaginationModule,
  ],
  providers: [UsersTeamService],
})
export class UsersTeamsModule {}

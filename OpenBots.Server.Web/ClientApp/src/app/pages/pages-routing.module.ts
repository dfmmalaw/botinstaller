/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { TermGuard } from '../guards/term.guard';
import { LoginGuard } from '../guards/login.guard';
import { ECommerceComponent } from './e-commerce/e-commerce.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    canActivate: [LoginGuard],
    children: [
      {
        path: 'dashboard',
        component: ECommerceComponent,
        canActivate: [TermGuard, LoginGuard],
      },

      // new development
      {
        path: 'users',
        loadChildren: () =>
          import('./users-teams/users-teams.module').then(
            (m) => m.UsersTeamsModule
          ),
        canActivate: [LoginGuard],
      },

      {
        path: 'agents',
        loadChildren: () =>
          import('./agents/agents.module').then((m) => m.AgentsModule),
        canActivate: [LoginGuard],
      },

      {
        path: 'queueitems',
        loadChildren: () =>
          import('./queue-items/queue-items.module').then(
            (mod) => mod.QueueItemsModule
          ),
        canActivate: [LoginGuard],
      },
      {
        path: 'audit',
        loadChildren: () =>
          import('./audit/audit.module').then((mod) => mod.AuditModule),
        canActivate: [LoginGuard],
      },
      {
        path: 'binaryobjects',
        loadChildren: () =>
          import('./binary/binary.module').then((mod) => mod.BinaryModule),
        canActivate: [LoginGuard],
      },
      {
        path: 'asset',
        loadChildren: () =>
          import('./asset/asset.module').then((mod) => mod.AssestModule),
        canActivate: [LoginGuard],
      },
      {
        path: 'job',
        loadChildren: () =>
          import('./jobs/jobs.module').then((mod) => mod.JobsModule),
        canActivate: [LoginGuard],
      },
      {
        path: 'credentials',
        loadChildren: () =>
          import('./credentials/credentials.module').then(
            (mod) => mod.CredentialsModule
          ),
        canActivate: [LoginGuard],
      },
      {
        path: 'miscellaneous',
        loadChildren: () =>
          import('./miscellaneous/miscellaneous.module').then(
            (m) => m.MiscellaneousModule
          ),
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
        canActivate: [LoginGuard],
      },
      {
        path: '**',
        component: NotFoundComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}

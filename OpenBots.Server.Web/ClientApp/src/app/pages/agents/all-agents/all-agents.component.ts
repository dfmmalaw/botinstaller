import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { AgentsService } from '../agents.service';
import { Page } from '../../../interfaces/paginateInstance';
import { SignalRService } from '../../../@core/backend/common/services/signal-r.service';


@Component({
  selector: 'ngx-all-agents',
  templateUrl: './all-agents.component.html',
  styleUrls: ['./all-agents.component.scss']
})
export class AllAgentsComponent implements OnInit {
  show_allagents: any = [];
  sortDir = 1;
  view_dialog: any;
  del_id: any = [];
  toggle: boolean;
  feild_name: any = [];
  page: Page = {};
  show_perpage_size :boolean = false;
  per_page_num:any =[];
  constructor(protected router: Router, private dialogService: NbDialogService,
    protected agentService: AgentsService,
    public signalRService: SignalRService, 
    private toastrService: NbToastrService) {
    

     }
    
    
  
   
  

  ngOnInit(): void {
    // this.signalRService.startConnection();
    // this.signalRService.addsendnotificationListener();
    // this.signalRService.addBroadcastChartDataListener();
    // this.get_chart();
    this.page.pageNumber = 1;
    this.page.pageSize = 5;
    this.pagination(this.page.pageNumber, this.page.pageSize);
  }
  gotoadd() {
    this.router.navigate(['/pages/agents/new'])
  }
  gotoedit(id) {
    this.router.navigate(['/pages/agents/edit'], { queryParams: { id: id } })
  }
  gotodetail(id) {
    this.router.navigate(['/pages/agents/get-agents-id'], { queryParams: { id: id } })
  }
  onSortClick(event,fil_val) {
    let target = event.currentTarget,
      classList = target.classList;
    if (classList.contains('fa-chevron-up')) {
      classList.remove('fa-chevron-up');
      classList.add('fa-chevron-down');
      let sort_set = 'desc'
      this.sort(fil_val,sort_set)
      this.sortDir = -1;

    } else {
      classList.add('fa-chevron-up');
      classList.remove('fa-chevron-down');
      let sort_set = 'asc'
      this.sort(fil_val,sort_set)
      this.sortDir = 1;
    }
  }


  sort(filter_value,vale) {
    const skip = (this.page.pageNumber - 1) * this.page.pageSize;
    this.feild_name = filter_value+'+' + vale;
    this.agentService.get_AllAgent_order(this.page.pageSize, skip, this.feild_name).subscribe(
      (data: any) => {
        this.show_allagents = data.items
      });
  }


  patch_Agent(event, id) {
    this.toggle = event.target.checked
    this.agentService.patch_Agent(id, this.toggle).subscribe(
      (data: any) => {
        this.toastrService.success('Agent IsEnable Successfully');
        this.show_allagents = data.items;
      });
  }

  per_page(val){
    this.per_page_num = val
    this.show_perpage_size = true 
    this.page.pageSize = val
    const skip = (this.page.pageNumber - 1) * this.per_page_num;
    this.agentService.get_AllAgent(this.page.pageSize, skip).subscribe(
      (data: any) => {
        this.show_allagents = data.items
        this.page.totalCount = data.totalCount;
       
      });
  }

  
  open2(dialog: TemplateRef<any>, id: any) {
    this.del_id = [];
    this.view_dialog = dialog;
    this.dialogService.open(dialog);
    this.del_id = id;

  }

  del_agent(ref) {
    this.agentService.del_AgentbyID(this.del_id).subscribe(
      () => {
        this.toastrService.success('Agent Delete Successfully');
        ref.close();
        this.get_allagent(this.page.pageNumber, this.page.pageSize);
      });

  }



  get_allagent(top, skip) {
   
    this.agentService.get_AllAgent(top, skip).subscribe(
      (data: any) => {
        this.show_allagents = data.items
        this.page.totalCount = data.totalCount;
      });
  }


 
  pageChanged(event) {
    this.page.pageNumber = event;
    this.pagination(event, this.page.pageSize);
  }

  pagination(pageNumber, pageSize?) {
 
    // const top: number = pageSize;
    // const skip = (pageNumber - 1) * pageSize;
    // this.get_allagent(top, skip);



    if (this.show_perpage_size == false){
      const top: number = pageSize;
      const skip = (pageNumber - 1) * pageSize;
      this.get_allagent(top, skip);
    }
    else if ( this.show_perpage_size == true ){
      const top: number = this.per_page_num;
      const skip = (pageNumber - 1) * this.per_page_num;
      this.get_allagent(top, skip);
    }
  }




}

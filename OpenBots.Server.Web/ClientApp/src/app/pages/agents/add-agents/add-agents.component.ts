import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { NbToastrService } from '@nebular/theme';
import { AgentsService } from '../agents.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-add-agents',
  templateUrl: './add-agents.component.html',
  styleUrls: ['./add-agents.component.scss']
})
export class AddAgentsComponent implements OnInit {
  addagent: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,protected agentService : AgentsService, protected router :Router,
    private toastrService: NbToastrService, ) { }

  ngOnInit(): void {
    this.addagent = this.formBuilder.group({
      name: [ '',[Validators.required ,Validators.minLength(2), Validators.maxLength(100)]],
      machineName: ['', Validators.required],
      macAddresses: ['', Validators.required],
      ipAddresses: ['', Validators.required],
      isEnabled: ['', Validators.required],
  });
  }

    // convenience getter for easy access to form fields
    get f() { return this.addagent.controls; }


    onSubmit() {
      this.submitted = true;    
      if (this.addagent.invalid) {
          return;
      }
    this.agentService.add_Agent(this.addagent.value).subscribe(
        () => {
          this.toastrService.success('Add Agent  Successfully! ');
          this.router.navigate(['pages/agents/list'])
        }, (error) => {
          if(error.status == 500){
            this.submitted = false
            this.toastrService.danger('Internal Server Error');
          }
          else {
            this.submitted = false;
            this.toastrService.danger(error.error.ServiceErrors[0]);  

          }
      
        });
  }

  onReset() {
      this.submitted = false;
      this.addagent.reset();
  }

}

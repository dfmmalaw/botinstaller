import { NgModule } from '@angular/core';
import { AgentsRoutingModule } from './agents-routing.module';
import { AllAgentsComponent } from './all-agents/all-agents.component';
import { ThemeModule } from '../../@theme/theme.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { GetAgentsIdComponent } from './get-agents-id/get-agents-id.component';
import { AddAgentsComponent } from './add-agents/add-agents.component';
import { EditAgentsComponent } from './edit-agents/edit-agents.component';
import { AgentsService } from './agents.service';
// import { TimeagoPipe } from '../../@core/backend/common/services/timeago.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../shared';

@NgModule({
  declarations: [
    AllAgentsComponent,
    GetAgentsIdComponent,
    AddAgentsComponent,
    EditAgentsComponent,
    // TimeagoPipe,
  ],
  imports: [
    SharedModule,
    AgentsRoutingModule,
    ThemeModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
  ],
  exports: [ ],
  providers: [AgentsService],
})
export class AgentsModule {}

/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class AgentsService {
  headerData = new HttpHeaders({ 'Content-Type': 'application/json' });
  get apiUrl(): string {
    return environment.apiUrl;
  }

  constructor(private http: HttpClient) { }

  ////////Agent api's ////

  get_AllAgent(tpage:any,spage:any) {
    let getagentUrl = `/Agents?$orderby=createdOn+desc&$top=${tpage}&$skip=${spage}`;
    return this.http.get(`${this.apiUrl}` + getagentUrl);
  }

  get_AllAgent_order(tpage:any,spage:any,name) {
    let getagentUrl = `/Agents?$orderby=${name}&$top=${tpage}&$skip=${spage}`;
    return this.http.get(`${this.apiUrl}` + getagentUrl);
  }

  get_AgentbyID(id) {
    let getagentUrlbyId = `/Agents/${id}`;
    return this.http.get(`${this.apiUrl}` + getagentUrlbyId);
  }

  del_AgentbyID(id) {
    let getagentUrlbyId = `/Agents/${id}`;
    return this.http.delete(`${this.apiUrl}` + getagentUrlbyId);
  }

  add_Agent(obj) {
    let addagentUrl = `/Agents`;
    return this.http.post(`${this.apiUrl}` + addagentUrl, obj);
  }

  edit_Agent(id,obj) {
    let editagentUrl = `/Agents/${id}`;
    return this.http.put(`${this.apiUrl}` + editagentUrl, obj);
  }

  patch_Agent(id,isenable) {
    let obj = [{
      "op": "replace",
      "path": "/isEnabled",
      "value": isenable }]
    let editagentUrl = `/Agents/${id}`;
    return this.http.patch(`${this.apiUrl}` + editagentUrl, obj);
  }
  


}

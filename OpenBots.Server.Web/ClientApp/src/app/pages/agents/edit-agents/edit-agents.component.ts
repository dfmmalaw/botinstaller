import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../../../@core/backend/common/api/http.service';
import { NbToastrService } from '@nebular/theme';
import { AgentsService } from '../agents.service';

@Component({
  selector: 'ngx-edit-agents',
  templateUrl: './edit-agents.component.html',
  styleUrls: ['./edit-agents.component.scss']
})
export class EditAgentsComponent implements OnInit {
  addagent: FormGroup;
  submitted = false;
  agent_id :any =[];
  show_allagents :any =[];
  constructor(private acroute: ActivatedRoute, private router: Router, 
    private formBuilder: FormBuilder,protected agentService : AgentsService,
    private toastrService: NbToastrService,
    ) {
    this.acroute.queryParams.subscribe(params => {
      this.agent_id = params.id
      this.get_allagent(params.id)
    });
   }

   ngOnInit(): void {
    this.addagent = this.formBuilder.group({
      name: ['', Validators.required],
      machineName: ['', Validators.required],
      macAddresses: ['', Validators.required],
      ipAddresses: ['', Validators.required],
      isEnabled: ['', Validators.required],
  });
  }



  get_allagent(id){
    this.agentService.get_AgentbyID(id).subscribe(
      (data :any) => {
        this.show_allagents = data
        this.addagent.patchValue(data)
      });
  }
    // convenience getter for easy access to form fields
    get f() { return this.addagent.controls; }


    onSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.addagent.invalid) {
          return;
      }

    this.agentService.edit_Agent(this.agent_id,this.addagent.value).subscribe(
        () => {  
          this.toastrService.success('Agent Details Upate Successfully! ');
          this.router.navigate(['pages/agents/list'])
        });
    
 
  }

  onReset() {
      this.submitted = false;
      this.addagent.reset();
  }

}

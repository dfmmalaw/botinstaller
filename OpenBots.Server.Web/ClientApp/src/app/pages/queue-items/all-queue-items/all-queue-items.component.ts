import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { HttpService } from '../../../@core/backend/common/api/http.service';
import { Router } from '@angular/router';
import { QueueItem } from '../../../interfaces/queueItem';
import { NbDialogService } from '@nebular/theme';
import { Page } from '../../../interfaces/paginateInstance';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Queues } from '../../../interfaces/queues';
import { SignalRService } from '../../../@core/backend/common/services/signal-r.service';
import * as signalR from '@aspnet/signalr';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'ngx-all-queue-items',
  templateUrl: './all-queue-items.component.html',
  styleUrls: ['./all-queue-items.component.scss'],
})
export class AllQueueItemsComponent implements OnInit, OnDestroy {
  allQueueItemData: QueueItem[] = [];
  deleteId: string;
  page: Page = {};
  queuesArr: Queues[] = [];
  queueForm: FormGroup;
  show = true;
  hubConnection: signalR.HubConnection;
  itemsPerPage: number[] = [5, 10, 25, 50, 100];
  filterOrderBy: string;
  constructor(
    private httpService: HttpService,
    private router: Router,
    private dialogService: NbDialogService,
    private fb: FormBuilder,
    public signalRService: SignalRService
  ) {}

  ngOnInit(): void {
    this.getQueues();
    this.page.pageNumber = 1;
    this.page.pageSize = 5;
    this.queueForm = this.initQueueForm();
  }

  initQueueForm() {
    return this.fb.group({
      id: [''],
      name: [''],
      itemsPerPage: 5,
    });
  }

  getQeueItemsList(top: number, skip: number, orderBy?: string): void {
    let url: string;
    // ? (url = `QueueItems?$orderby=name+${orderBy}&$top=${top}&$skip=${skip}&$filter= Queueid eq guid'${this.queueForm.value.id}'`)
    // : (url = `QueueItems?$orderby=createdOn+desc&$top=${top}&$skip=${skip}&$filter= Queueid eq guid'${this.queueForm.value.id}'`);

    orderBy
      ? (url = `QueueItems?$orderby=${orderBy}&$top=${top}&$skip=${skip}&$filter= Queueid eq guid'${this.queueForm.value.id}'`)
      : (url = `QueueItems?$orderby=createdOn+desc&$top=${top}&$skip=${skip}&$filter= Queueid eq guid'${this.queueForm.value.id}'`);
    this.httpService.get(url).subscribe(
      (response) => {
        this.page.totalCount = response.totalCount;
        response && response.items
          ? (this.allQueueItemData = [...response.items])
          : (this.allQueueItemData = []);
      },
      (error) => {
        this.httpService.error(error.error.ServiceErrors[0]);
      }
    );
  }

  addItem(): void {
    this.router.navigate(['pages/queueitems/new']);
  }

  editQueueItem(id: string): void {
    this.router.navigate([`pages/queueitems/edit/${id}`]);
  }

  openDeleteDialog(ref: TemplateRef<any>, id: string): void {
    this.deleteId = id;
    this.dialogService.open(ref);
  }

  deleteQueueItem(ref): void {
    this.httpService.delete(`QueueItems/${this.deleteId}`).subscribe(
      () => {
        ref.close();
        this.httpService.success('Deleted Successfully');
        this.pagination(this.page.pageNumber, this.page.pageSize);
      },
      (error) => {
        this.httpService.error(error.error.ServiceErrors[0]);
      }
    );
  }

  viewQueueItem(id: string): void {
    this.router.navigate([`pages/queueitems/view/${id}`]);
  }

  pageChanged(event): void {
    this.filterOrderBy
      ? ((this.page.pageNumber = event),
        this.pagination(event, this.page.pageSize, `${this.filterOrderBy}`))
      : ((this.page.pageNumber = event),
        this.pagination(event, this.page.pageSize));
  }

  pagination(pageNumber: number, pageSize: number, orderBy?: string): void {
    const top = pageSize;
    this.page.pageSize = pageSize;
    const skip = (pageNumber - 1) * pageSize;
    this.getQeueItemsList(top, skip, orderBy);
  }

  onSortClick(event, param: string): void {
    let target = event.currentTarget,
      classList = target.classList;
    if (classList.contains('fa-chevron-up')) {
      classList.remove('fa-chevron-up');
      classList.add('fa-chevron-down');
      this.filterOrderBy = `${param}+asc`;
      this.pagination(this.page.pageNumber, this.page.pageSize, `${param}+asc`);
    } else {
      classList.remove('fa-chevron-down');
      classList.add('fa-chevron-up');
      this.filterOrderBy = `${param}+desc`;
      this.pagination(
        this.page.pageNumber,
        this.page.pageSize,
        `${param}+desc`
      );
    }
  }

  addQueue(): void {
    this.router.navigate(['pages/queueitems/addqueue']);
  }

  selectChange(event, param: string, order: string): void {
    event.target.value && param === 'pageSize'
      ? ((this.page.pageSize = +event.target.value),
        (this.page.pageNumber = 1),
        this.filterOrderBy
          ? this.pagination(
              this.page.pageNumber,
              this.page.pageSize,
              `${this.filterOrderBy}`
            )
          : this.pagination(this.page.pageNumber, this.page.pageSize))
      : event.target.value && param === 'nothing'
      ? this.filterOrderBy
        ? this.pagination(
            this.page.pageNumber,
            this.page.pageSize,
            `${this.filterOrderBy}`
          )
        : this.pagination(this.page.pageNumber, this.page.pageSize)
      : null;
    // if (event.target.value)
    //   this.pagination(this.page.pageNumber, this.page.pageSize);
  }

  getQueues(): void {
    const url = `Queues?$orderby=createdOn+desc`;
    this.httpService.get(url).subscribe(
      (response) => {
        response && response.items
          ? (this.queuesArr = [...response.items])
          : (this.queuesArr = []);
        this.queueForm.patchValue(this.queuesArr[0]);
        this.pagination(this.page.pageNumber, this.page.pageSize);
      },
      (error) => this.httpService.error(error.error.ServiceErrors[0])
    );
  }

  watchRealTimeData(event) {
    event.target.checked == true
      ? ((this.show = false), this.startConnection(), this.queueForm.disable())
      : this.closeHubConnection();
  }

  private startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(environment.hubUrl)
      .build();
    this.hubConnection
      .start()
      .then(() => {
        this.httpService.success('Watch mode is active now');
        this.hubConnection.on('sendnotification', (data) => {
          this.httpService.success(data);
          this.pagination(this.page.pageNumber, this.page.pageSize);
        });
      })
      .catch((err) => {
        this.httpService.error(err);
      });
  };

  closeHubConnection() {
    if (this.hubConnection) {
      this.show = true;
      this.queueForm.enable();
      this.hubConnection
        .stop()
        .then(() => this.httpService.warning('Watch mode is off now'));
      this.hubConnection = undefined;
    }
  }

  ngOnDestroy(): void {
    this.closeHubConnection();
  }
}

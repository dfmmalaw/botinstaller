import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../../../@core/backend/common/api/http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { QueueItem } from '../../../interfaces/queueItem';
import { Queues } from '../../../interfaces/queues';

@Component({
  selector: 'ngx-add-queue-items',
  templateUrl: './add-queue-items.component.html',
  styleUrls: ['./add-queue-items.component.scss'],
})
export class AddQueueItemsComponent implements OnInit {
  queueItemForm: FormGroup;
  queueItemsType: any = ['Entity', 'Text'];
  queueItemId: string;
  dataGetById: QueueItem;
  title = 'Add';
  btnText = 'Add';
  isSubmitted = false;
  queuesArr: Queues[] = [];
  constructor(
    private fb: FormBuilder,
    private httpService: HttpService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.queueItemId = this.route.snapshot.params['id'];
    this.getQueues();
    if (this.queueItemId) {
      this.getQueueDataById();
      this.title = 'Edit';
      this.btnText = 'Update';
    }
    this.queueItemForm = this.initializigQueueItemForm();
  }

  initializigQueueItemForm() {
    return this.fb.group({
      organizationId: localStorage.getItem('ActiveOrganizationID'),
      processID: null,
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(100),
        ],
      ],
      subtopic: ['', [Validators.minLength(1), Validators.maxLength(100)]],
      event: ['', [Validators.minLength(1), Validators.maxLength(100)]],
      source: ['', [Validators.minLength(1), Validators.maxLength(100)]],
      priority: [null, [Validators.pattern(/^[1-9][0-9]?$/)]],
      queueItemType: [''],
      entityType: [''],
      entityStatus: [''],
      dataJSON: [''],
      dataText: [''],
      queueID: ['', [Validators.required]],
    });
  }
  get controls() {
    return this.queueItemForm.controls;
  }

  addQueueItem(): void {
    this.isSubmitted = true;
    this.queueItemId ? this.updateItem() : this.addItem();
  }

  //TODO empty the form properties empty on selecting from dropdown
  onQueueItemchange(): void {
    this.queueItemForm.get('entityType').reset();
    this.queueItemForm.get('entityStatus').reset();
    this.queueItemForm.get('dataJSON').reset();
    this.queueItemForm.get('dataText').reset();
  }

  getQueueDataById(): void {
    this.httpService.get(`QueueItems/${this.queueItemId}`).subscribe(
      (response: QueueItem) => {
        this.queueItemForm.patchValue(response);
      },
      (error) => error.error.ServiceErrors[0]
    );
  }

  addItem(): void {
    this.httpService.post('QueueItems', this.queueItemForm.value).subscribe(
      () => {
        this.httpService.success('Queue item created successfully');
        this.navigateToQueueItemsList();
        this.isSubmitted = false;
        this.queueItemForm.reset();
      },
      (error) => {
        this.httpService.error(error.error.ServiceErrors[0]);
        this.isSubmitted = false;
      }
    );
  }

  updateItem(): void {
    this.httpService
      .put(`QueueItems/${this.queueItemId}`, this.queueItemForm.value)
      .subscribe(
        () => {
          this.httpService.success('Queue item updated successfully');
          this.navigateToQueueItemsList();
          this.isSubmitted = false;
          this.queueItemForm.reset();
        },
        (error) => {
          this.httpService.error(error.error.ServiceErrors[0]);
          this.isSubmitted = false;
        }
      );
  }

  navigateToQueueItemsList(): void {
    this.router.navigate(['pages/queueitems']);
  }

  getQueues(): void {
    const url = `Queues?$orderby=createdOn+desc`;
    this.httpService.get(url).subscribe(
      (response) => {
        response && response.items
          ? (this.queuesArr = [...response.items])
          : (this.queuesArr = []);
      },
      (error) => error.error.ServiceErrors[0]
    );
  }
}

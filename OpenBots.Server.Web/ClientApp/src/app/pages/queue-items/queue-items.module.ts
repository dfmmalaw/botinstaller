import { NgModule } from '@angular/core';
import { QueueItemsRoutingModule } from './queue-items-routing.module';
import { AddQueueItemsComponent } from './add-queue-items/add-queue-items.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AllQueueItemsComponent } from './all-queue-items/all-queue-items.component';
import { ViewQueueItemComponent } from './view-queue-item/view-queue-item.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { QueueComponent } from './queue/queue.component';
import { SharedModule } from '../../shared';

@NgModule({
  declarations: [
    AddQueueItemsComponent,
    AllQueueItemsComponent,
    ViewQueueItemComponent,
    QueueComponent,
  ],
  imports: [
    SharedModule,
    QueueItemsRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
  ],
})
export class QueueItemsModule {}

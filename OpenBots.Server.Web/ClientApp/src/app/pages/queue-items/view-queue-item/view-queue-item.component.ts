import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../../../@core/backend/common/api/http.service';
import { QueueItem } from '../../../interfaces/queueItem';

@Component({
  selector: 'ngx-view-queue-item',
  templateUrl: './view-queue-item.component.html',
  styleUrls: ['./view-queue-item.component.scss'],
})
export class ViewQueueItemComponent implements OnInit {
  showQueueItemForm: FormGroup;
  queueItemId: string;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private httpService: HttpService
  ) {}

  ngOnInit(): void {
    this.queueItemId = this.route.snapshot.params['id'];
    this.showQueueItemForm = this.initializeForm();
    if (this.queueItemId) {
      this.getQueueDataById();
    }
  }

  initializeForm() {
    return this.fb.group({
      organizationId: [''],
      processID: null,
      name: [''],
      subtopic: [''],
      event: [''],
      source: [''],
      priority: ['', [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      queueItemType: [''],
      entityType: [''],
      entityStatus: [''],
      dataJSON: [''],
      dataText: [''],
      dontDequeueUntil: [''],
      dontDequeueAfter: [''],
      isDequeued: [''],
      isLocked: [''],
      lockedOn: [''],
      lockedUntil: [''],
      lockedBy: [''],
      lockTransactionKey: [''],
      retryCount: 0,
      lastOccuredError: [''],
      timestamp: [''],
      isError: [''],
    });
  }

  getQueueDataById(): void {
    this.httpService
      .get(`QueueItems/${this.queueItemId}`)
      .subscribe((response: QueueItem) => {
        response.isDequeued = this.changeBooleanValue(response.isDequeued);
        response.isLocked = this.changeBooleanValue(response.isLocked);
        response.isError = this.changeBooleanValue(response.isError);
        this.showQueueItemForm.patchValue(response);
        this.showQueueItemForm.disable();
      });
  }
  changeBooleanValue(value): string {
    if (value == true) {
      return 'Yes';
    } else {
      return 'No';
    }
  }
}

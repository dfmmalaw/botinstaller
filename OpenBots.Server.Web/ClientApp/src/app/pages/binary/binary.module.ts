import { NgModule } from '@angular/core';
import { BinaryRoutingModule } from './binary-routing.module';
import { BinaryComponent } from './binary/binary.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../shared';
@NgModule({
  declarations: [BinaryComponent],
  imports: [
    SharedModule,
    BinaryRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
  ],
})
export class BinaryModule {}

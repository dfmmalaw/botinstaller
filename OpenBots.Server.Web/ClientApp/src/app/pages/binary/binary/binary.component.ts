import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpService } from '../../../@core/backend/common/api/http.service';
import { BinaryObjects } from '../../../interfaces/binaryObjects';
import { Page } from '../../../interfaces/paginateInstance';
import { NbToastrService, NbDialogService } from '@nebular/theme';

@Component({
  selector: 'ngx-binary',
  templateUrl: './binary.component.html',
  styleUrls: ['./binary.component.scss'],
})
export class BinaryComponent implements OnInit {
  binaryObjectsForm: FormGroup;
  binaryObjectsData: BinaryObjects[] = [];
  page: Page = {};
  deleteId: string;
  itemsPerPage: number[] = [5, 10, 25, 50, 100];
  constructor(
    private fb: FormBuilder,
    private httpService: HttpService,
    private dialogService: NbDialogService,
    private toastrService: NbToastrService
  ) {}

  ngOnInit(): void {
    this.binaryObjectsForm = this.initializeBinaryForm();
    this.page.pageNumber = 1;
    this.page.pageSize = 5;
    this.pagination(this.page.pageNumber, this.page.pageSize);
  }

  initializeBinaryForm() {
    return this.fb.group({
      itemsPerPage: 5,
      name: [''],
      organizationId: [''],
      contentType: [''],
      corelationEntityId: [''],
      corelationEntity: [''],
      storagePath: [''],
      storageProvider: [''],
      sizeInBytes: [],
      hashCode: [''],
      id: [''],
      isDeleted: [],
      createdBy: [''],
      createdOn: [''],
      deletedBy: [''],
      deleteOn: [''],
      timestamp: [''],
      updatedOn: [''],
      updatedBy: [''],
    });
  }

  getAllBinaryObjects(top, skip, orderBy): void {
    const url = `BinaryObjects?$top=${top}&$skip=${skip}&$orderby=createdOn+desc`;
    this.httpService.get(url).subscribe((response) => {
      response && response.items
        ? (this.binaryObjectsData = [...response.items])
        : (this.binaryObjectsData = []);
      this.page.totalCount = response.totalCount;
    });
  }

  openDeleteDialog(ref: TemplateRef<any>, id: string): void {
    this.deleteId = id;
    this.dialogService.open(ref);
  }

  deleteBinaryObjects(ref): void {
    this.httpService.delete(`BinaryObjects/${this.deleteId}`).subscribe(
      () => {
        ref.close();
        this.toastrService.success('Deleted Successfully');
        this.pagination(this.page.pageNumber, this.page.pageSize);
      },
      (error) => {
        this.toastrService.danger(error.error.ServiceErrors[0]);
      }
    );
  }

  pageChanged(event): void {
    this.page.pageNumber = event;
    this.pagination(event, this.page.pageSize);
  }

  pagination(pageNumber: number, pageSize: number, orderBy?: string): void {
    const top = pageSize;
    this.page.pageSize = pageSize;
    const skip = (pageNumber - 1) * pageSize;
    this.getAllBinaryObjects(top, skip, orderBy);
  }

  selectChange(event): void {
    if (event.target.value) {
      this.page.pageNumber = 1;
      this.page.pageSize = +event.target.value;
      this.pagination(this.page.pageNumber, this.page.pageSize);
    }
  }
}

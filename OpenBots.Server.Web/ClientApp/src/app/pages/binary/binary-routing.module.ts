import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BinaryComponent } from './binary/binary.component';


const routes: Routes = [{path:'', component:BinaryComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BinaryRoutingModule { }

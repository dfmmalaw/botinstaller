import { Injectable } from '@angular/core';
 
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
 

@Injectable({
  providedIn: 'root'
})
export class AssetService {
  headerData = new HttpHeaders({ 'Content-Type': 'application/json' });
  get apiUrl(): string {
    return environment.apiUrl;
  }

  constructor(private http: HttpClient) { }

  ////////Asset api's ////

  get_AllAsset(tpage:any,spage:any) {
    let getagentUrl = `/Assets?$orderby=createdOn desc&$top=${tpage}&$skip=${spage}`;
    return this.http.get(`${this.apiUrl}` + getagentUrl);
  }
 
  get_AllAsset_order(tpage:any,spage:any,name) {
    let getagentUrl = `/Assets?$orderby=${name}&$top=${tpage}&$skip=${spage}`;
    return this.http.get(`${this.apiUrl}` + getagentUrl);
  }

  get_AssetbyId(id) {
    let getagentUrlbyId = `/Assets/${id}`;
    return this.http.get(`${this.apiUrl}` + getagentUrlbyId);
  }

  del_AssetbyID(id) {
    let getagentUrlbyId = `/Assets/${id}`;
    return this.http.delete(`${this.apiUrl}` + getagentUrlbyId);
  }

  add_Asset(obj) {
    let addassetUrl = `/Assets`;
    return this.http.post(`${this.apiUrl}` + addassetUrl, obj);
  }

  edit_Asset(id,obj) {
    let editassetUrl = `/Assets/${id}`;
    return this.http.put(`${this.apiUrl}` + editassetUrl, obj);
  }


   
  


}

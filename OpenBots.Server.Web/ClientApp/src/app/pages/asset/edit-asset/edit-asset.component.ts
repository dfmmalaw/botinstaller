import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { AssetService } from '../asset.service';
 

@Component({
  selector: 'ngx-edit-asset',
  templateUrl: './edit-asset.component.html',
  styleUrls: ['./edit-asset.component.scss']
})
export class EditAssetComponent implements OnInit {
  jsonValue: any = [];
  assetagent: FormGroup;
  submitted = false;
  agent_id :any =[];
  show_allagents :any =[];
  constructor(private acroute: ActivatedRoute, private router: Router, 
    private formBuilder: FormBuilder,protected assetService : AssetService,
    private toastrService: NbToastrService,
    ) {
    this.acroute.queryParams.subscribe(params => {
      this.agent_id = params.id
      this.get_allagent(params.id)
    });
   }

   ngOnInit(): void {
    this.assetagent = this.formBuilder.group({
      // name: ['', Validators.required],
      // valueJson: ['', Validators.required],
      // type: ['', Validators.required],
      binaryObjectID: [''],
      createdBy: [''],
      createdOn: [''],
      deleteOn: [''],
      deletedBy: [''],
      id: [''],
      isDeleted: [''],
      jsonValue: [''],
      name: [''],
      numberValue: [''],
      textValue: [''],
      timestamp: [''],
      type: [''],
      updatedBy: [''],
      updatedOn: [''],
  });
  }



  get_allagent(id){
    this.assetService.get_AssetbyId(id).subscribe(
      (data :any) => {
        this.show_allagents = data
        if (data.jsonValue) {
          this.jsonValue = data.jsonValue;
          this.jsonValue = JSON.parse(this.jsonValue);
        }
        this.assetagent.patchValue(data)
      });
  }
    // convenience getter for easy access to form fields
    get f() { return this.assetagent.controls; }


    onSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.assetagent.invalid) {
          return;
      }

    this.assetService.edit_Asset(this.agent_id,this.assetagent.value).subscribe(
        () => {  
          this.toastrService.success('Asset Details Upate Successfully! ');
          this.router.navigate(['pages/asset/list'])
        });
    
 
  }

  onReset() {
      this.submitted = false;
      this.assetagent.reset();
  }

}

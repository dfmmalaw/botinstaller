import { NgModule } from '@angular/core';
import { AssestRoutingModule } from './asset-routing.module';
import { AllAssetComponent } from './all-asset/all-asset.component';
import { ThemeModule } from '../../@theme/theme.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { AssetService } from './asset.service';
import { AddAssetComponent } from './add-asset/add-asset.component';
import { GetAssetIdComponent } from './get-asset-id/get-asset-id.component';
import { PrettyPrintPipe } from '../../@theme/pipes/pretty-print.pipe';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { EditAssetComponent } from './edit-asset/edit-asset.component';
import { SharedModule } from '../../shared';
import { FileSaverModule } from 'ngx-filesaver';

@NgModule({
  declarations: [
    AllAssetComponent,
    AddAssetComponent,
    GetAssetIdComponent,
    PrettyPrintPipe,
    EditAssetComponent,
  ],
  imports: [
    SharedModule,
    AssestRoutingModule,
    ThemeModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxJsonViewerModule,
    FileSaverModule
  ],
  providers: [AssetService],
})
export class AssestModule {}

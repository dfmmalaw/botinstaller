import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { NbToastrService } from '@nebular/theme';

import { Router } from '@angular/router';
import { AssetService } from '../asset.service';

@Component({
  selector: 'ngx-add-asset',
  templateUrl: './add-asset.component.html',
  styleUrls: ['./add-asset.component.scss']
})
export class AddAssetComponent implements OnInit {
  save_value:any =[]
  addasset: FormGroup;
  submitted = false;
  json: boolean = false;
  numbervalue: boolean = false;
  textvalue: boolean = false;
  value = ['JSON', 'Number', 'Text']
  constructor(private formBuilder: FormBuilder, protected assetService: AssetService, protected router: Router,
    private toastrService: NbToastrService) { }

  ngOnInit(): void {
  
    this.addasset = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
      JsonValue: ['', [  Validators.minLength(2), Validators.maxLength(100000)]],
      TextValue: ['', [Validators.minLength(2), Validators.maxLength(100000)]],
      NumberValue: ['', [Validators.minLength(2), Validators.maxLength(1000), Validators.pattern("^[0-9]+(\.[0-9]*){0,1}$")]],
      type: ['', Validators.required],
    });
   
  }

  // convenience getter for easy access to form fields
  get f() { return this.addasset.controls; }



  onSubmit() {
    this.submitted = true;
    if (this.addasset.invalid) {
      return;
    }
    if ( this.json == true && this.numbervalue == false && this.textvalue == false){
      this.save_value ={
        name: this.addasset.value.name,
        JsonValue: this.addasset.value.JsonValue,
        type: this.addasset.value.type,
      }
    }
    else if ( this.json == false && this.numbervalue == true && this.textvalue == false){
        this.save_value ={
        name: this.addasset.value.name,
        NumberValue:this.addasset.value.NumberValue,
        type: this.addasset.value.type,
      }
    }
    else  if ( this.json == false && this.numbervalue == false && this.textvalue == true){
      this.save_value ={
        name: this.addasset.value.name,
        TextValue:this.addasset.value.TextValue,
        type: this.addasset.value.type,
      }
    }
    this.assetService.add_Asset(this.save_value).subscribe(
      () => {
        this.toastrService.success('Asset Add  Successfully! ');
        this.router.navigate(['pages/asset/list'])
      }, (error) => {
        if (error.status == 500) {
          this.submitted = false
          this.toastrService.danger('Internal Server Error');
        }
        else {
          this.submitted = false;
          this.toastrService.danger(error.error.ServiceErrors[0]);
        }
      });
  }

  onReset() {
    this.submitted = false;
    this.addasset.reset();
  }
  get_val(val) {
 
    if (val == 'JSON') {
      this.json = true;
      this.numbervalue = false;
      this.textvalue = false;
      
    }
    else if (val == 'Number') {
      this.numbervalue = true;

      this.json = false;
      this.textvalue = false;
    }
    else if (val == 'Text') {
      this.textvalue = true;
      this.json = false;
      this.numbervalue = false;

    }
  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JobsService {
  headerData = new HttpHeaders({ 'Content-Type': 'application/json' });
  get apiUrl(): string {
    return environment.apiUrl;
  }

  constructor(private http: HttpClient) { }

  ////////Jobs api's ////

  get_AllJobs(tpage:any,spage:any) {
    let getJobstUrl = `/Jobs?$orderby=createdOn+desc&$top=${tpage}&$skip=${spage}`;
    return this.http.get(`${this.apiUrl}` + getJobstUrl);
  }

  get_AllJobs_order(tpage:any,spage:any,name) {
    let getJobsUrl = `/Jobs?$orderby=${name}&$top=${tpage}&$skip=${spage}`;
    return this.http.get(`${this.apiUrl}` + getJobsUrl);
  }

  get_jobs_id(id) {
    let getagentUrlbyId = `/Jobs/${id}`;
    return this.http.get(`${this.apiUrl}` + getagentUrlbyId);
  }
  



}

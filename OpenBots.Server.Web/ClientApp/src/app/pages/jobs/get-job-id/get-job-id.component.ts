import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobsService } from '../jobs.service';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'ngx-get-job-id',
  templateUrl: './get-job-id.component.html',
  styleUrls: ['./get-job-id.component.scss']
})
export class GetJobIdComponent implements OnInit {
  
  jsonValue: any = [];
  show_alljobs: any = [];
  showjobs: FormGroup;
  pipe = new DatePipe('en-US');
  now = Date();
  show_createdon: any = [];

  constructor(private acroute: ActivatedRoute,private formBuilder: FormBuilder, 
    protected jobService: JobsService) {
    this.acroute.queryParams.subscribe(params => {
      this.get_job(params.id)
    
    });
  }

  ngOnInit(): void {
    this.showjobs = this.formBuilder.group({
    agentId: [''],
    createdBy: [''],
    createdOn: [''],
    deleteOn: [''],
    deletedBy: [''],
    dequeueTime: [''],
    endTime:[''],
    enqueueTime: [''],
    id: [''],
    isDeleted: [''],
    isSuccessful: [''],
    jobStatus: [''],
    message: [''],
    processId: [''],
    startTime:[''],
    timestamp: [''],
    updatedBy: ['']
    });
   
  }

 

  
  get_job(id) {
    this.jobService.get_jobs_id(id).subscribe((data: any) => {
      this.show_alljobs = data;
      const datepipe: DatePipe = new DatePipe('en-US')
      data.endTime = datepipe.transform(data.endTime, 'MM-dd-yyyy HH:mm:ss a');
      data.startTime = datepipe.transform(data.startTime, 'MM-dd-yyyy HH:mm:ss a');
      data.enqueueTime = datepipe.transform(data.enqueueTime, 'MM-dd-yyyy HH:mm:ss a');
      data.dequeueTime = datepipe.transform(data.dequeueTime, 'MM-dd-yyyy HH:mm:ss a');
      
      this.showjobs.patchValue(data);
      this.showjobs.disable();
    });
  }
   
   
}

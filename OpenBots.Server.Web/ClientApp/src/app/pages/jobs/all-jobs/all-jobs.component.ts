import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import {  NbDialogService } from '@nebular/theme';
import { Page } from '../../../interfaces/paginateInstance';
import { JobsService } from '../jobs.service';

@Component({
  selector: 'ngx-all-jobs',
  templateUrl: './all-jobs.component.html',
  styleUrls: ['./all-jobs.component.scss']
})
export class AllJobsComponent implements OnInit {
  show_alljobs: any = [];
  sortDir = 1;
  view_dialog: any;
  del_id: any = [];
  toggle: boolean;
  feild_name: any = [];
  page: Page = {};
  show_perpage_size :boolean = false;
  per_page_num:any =[];
  constructor(protected router: Router, private dialogService: NbDialogService,
    protected jobService: JobsService) { }
    
    
  
   
  

  ngOnInit(): void {
    
    this.page.pageNumber = 1;
    this.page.pageSize = 5;
    this.pagination(this.page.pageNumber, this.page.pageSize);
  }
   
 
  gotodetail(id) {
    this.router.navigate(['/pages/job/get-jobs-id'], { queryParams: { id: id } })
  }
  sort(vale) {
    const skip = (this.page.pageNumber - 1) * this.page.pageSize;
    this.feild_name = 'MachineName+' + vale;
    this.jobService.get_AllJobs_order(this.page.pageSize, skip, this.feild_name).subscribe(
      (data: any) => {
        this.show_alljobs = data.items
      });
  }


 
  per_page(val){
    this.per_page_num = val
    this.show_perpage_size = true 
    this.jobService.get_AllJobs(this.per_page_num, this.page.pageNumber).subscribe(
      (data: any) => {
        this.show_alljobs = data.items
        this.page.totalCount = data.totalCount;
        this.page.pageSize = data.pageSize;
      });
  }

  
  open2(dialog: TemplateRef<any>, id: any) {
    this.del_id = [];
    this.view_dialog = dialog;
    this.dialogService.open(dialog);
    this.del_id = id;

  }

  



  get_AllJobs(top, skip) {
    this.feild_name = 'MachineName'
    this.jobService.get_AllJobs(top, skip).subscribe(
      (data: any) => {
        this.show_alljobs = data.items
        this.page.totalCount = data.totalCount;
      });
  }


  onSortClick(event) {
    let target = event.currentTarget,
      classList = target.classList;
    if (classList.contains('fa-chevron-up')) {
      classList.remove('fa-chevron-up');
      classList.add('fa-chevron-down');
      let sort_set = 'desc'
      this.sort(sort_set)
      this.sortDir = -1;

    } else {
      classList.add('fa-chevron-up');
      classList.remove('fa-chevron-down');
      let sort_set = 'asc'
      this.sort(sort_set)
      this.sortDir = 1;
    }
  }
  pageChanged(event) {
    this.page.pageNumber = event;
    this.pagination(event, this.page.pageSize);
  }

  pagination(pageNumber, pageSize?) {
 
    // const top: number = pageSize;
    // const skip = (pageNumber - 1) * pageSize;
    // this.get_AllJobs(top, skip);



    if (this.show_perpage_size == false){
      const top: number = pageSize;
      const skip = (pageNumber - 1) * pageSize;
      this.get_AllJobs(top, skip);
    }
    else if ( this.show_perpage_size == true ){
      const top: number = this.per_page_num;
      const skip = (pageNumber - 1) * this.per_page_num;
      this.get_AllJobs(top, skip);
    }
  }




}

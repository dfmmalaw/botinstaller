import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobsRoutingModule } from './jobs-routing.module';
import { AllJobsComponent } from './all-jobs/all-jobs.component';
import { GetJobIdComponent } from './get-job-id/get-job-id.component';
import { JobsService } from './jobs.service';
import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../shared';
import { TimeagoPipe } from '../../@core/backend/common/services/timeago.pipe';

@NgModule({
  declarations: [AllJobsComponent, GetJobIdComponent],
  imports: [
    CommonModule,
    JobsRoutingModule,
    ThemeModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule
  ],providers:[
    JobsService
  ]
})
export class JobsModule { }

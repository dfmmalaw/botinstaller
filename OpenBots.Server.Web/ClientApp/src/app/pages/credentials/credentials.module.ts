import { NgModule } from '@angular/core';
import { CredentialsRoutingModule } from './credentials-routing.module';
import { SharedModule } from '../../shared';
import { CredentialsComponent } from './credentials/credentials.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ViewCredentialsComponent } from './view-credentials/view-credentials.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddCredentialsComponent } from './add-credentials/add-credentials.component';

@NgModule({
  declarations: [
    CredentialsComponent,
    ViewCredentialsComponent,
    AddCredentialsComponent,
  ],
  imports: [
    SharedModule,
    CredentialsRoutingModule,
    NgxPaginationModule,
    ReactiveFormsModule,
  ],
})
export class CredentialsModule {}

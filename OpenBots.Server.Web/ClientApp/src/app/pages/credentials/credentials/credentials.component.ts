import { Component, OnInit, TemplateRef } from '@angular/core';
import { Credentils } from '../../../@core/interfaces/credentials';
import { HttpService } from '../../../@core/backend/common/api/http.service';
import { Page } from '../../../interfaces/paginateInstance';
import { NbDialogService } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-credentials',
  templateUrl: './credentials.component.html',
  styleUrls: ['./credentials.component.scss'],
})
export class CredentialsComponent implements OnInit {
  credentialsArr: Credentils[] = [];
  page: Page = {};
  deleteId: string;
  filterOrderBy: string;
  itemsPerPage: number[] = [5, 10, 25, 50, 100];

  constructor(
    private httpService: HttpService,
    private dialogService: NbDialogService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.page.pageNumber = 1;
    this.page.pageSize = 5;
    this.pagination(this.page.pageNumber, this.page.pageSize);
  }

  pagination(pageNumber: number, pageSize: number, orderBy?: string): void {
    const top = pageSize;
    this.page.pageSize = pageSize;
    const skip = (pageNumber - 1) * pageSize;
    this.getAllCredentials(top, skip, orderBy);
  }

  getAllCredentials(top, skip, orderBy?): void {
    let url: string;
    orderBy
      ? (url = `Credentials?$orderby=${orderBy}&$top=${top}&$skip=${skip}`)
      : (url = `Credentials?$orderby=createdOn+desc&$top=${top}&$skip=${skip}`);
    this.httpService.get(url).subscribe((response) => {
      console.log('res', response);
      response && response.items
        ? ((this.credentialsArr = [...response.items]),
          (this.page.totalCount = response.totalCount))
        : (this.credentialsArr = []);
    });
  }

  viewCredential(id: string) {
    this.router.navigate([`/pages/credentials/view/${id}`]);
  }

  pageChanged(event): void {
    this.page.pageNumber = event;
    this.filterOrderBy
      ? this.pagination(event, this.page.pageSize, `${this.filterOrderBy}`)
      : this.pagination(event, this.page.pageSize);
  }

  deleteCredential(ref): void {
    this.httpService
      .delete(`Credentials/${this.deleteId}`, { observe: 'response' })
      .subscribe((response) => {
        console.log('response', response);
        this.pagination(this.page.pageNumber, this.page.pageSize);
      });
  }

  openDeleteDialog(ref: TemplateRef<any>, id: string): void {
    this.deleteId = id;
    this.dialogService.open(ref);
  }

  addCredential(): void {
    this.router.navigate([`/pages/credentials/add`]);
  }

  editCredential(id: string): void {
    this.router.navigate([`/pages/credentials/edit/${id}`]);
  }

  onSortClick(event, param: string): void {
    let target = event.currentTarget,
      classList = target.classList;
    if (classList.contains('fa-chevron-up')) {
      classList.remove('fa-chevron-up');
      classList.add('fa-chevron-down');
      this.filterOrderBy = `${param}+asc`;
      this.pagination(this.page.pageNumber, this.page.pageSize, `${param}+asc`);
    } else {
      classList.remove('fa-chevron-down');
      classList.add('fa-chevron-up');
      this.filterOrderBy = `${param}+desc`;
      this.pagination(
        this.page.pageNumber,
        this.page.pageSize,
        `${param}+desc`
      );
    }
  }

  selectChange(event): void {
    this.page.pageSize = +event.target.value;
    this.page.pageNumber = 1;
    event.target.value && this.filterOrderBy
      ? this.pagination(
          this.page.pageNumber,
          this.page.pageSize,
          `${this.filterOrderBy}`
        )
      : this.pagination(this.page.pageNumber, this.page.pageSize);
  }
}

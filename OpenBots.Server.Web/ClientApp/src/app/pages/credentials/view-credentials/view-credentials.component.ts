import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../../../@core/backend/common/api/http.service';

@Component({
  selector: 'ngx-view-credentials',
  templateUrl: './view-credentials.component.html',
  styleUrls: ['./view-credentials.component.scss'],
})
export class ViewCredentialsComponent implements OnInit {
  credentialViewForm: FormGroup;
  currentUrlId: string;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private httpService: HttpService
  ) {}

  ngOnInit(): void {
    this.currentUrlId = this.route.snapshot.params['id'];
    if (this.currentUrlId) {
      this.getCredentialById();
    }
    this.credentialViewForm = this.initizlizeForm();
  }

  initizlizeForm() {
    return this.fb.group({
      name: [''],
      certificate: [''],
      createdBy: [''],
      createdOn: [''],
      deleteOn: null,
      deletedBy: [''],
      domain: [''],
      endDate: [],
      id: [''],
      isDeleted: false,
      passwordHash: [''],
      passwordSecret: [''],
      provider: [''],
      startDate: [],
      timestamp: [''],
      updatedBy: null,
      updatedOn: null,
      userName: [''],
    });
  }

  getCredentialById(): void {
    this.httpService
      .get(`Credentials/${this.currentUrlId}`, { observe: 'response' })
      .subscribe((response) => {
        console.log('ress', response);
        if (response && response.status == 200) {
          this.credentialViewForm.patchValue(response.body);
          this.credentialViewForm.disable();
        }
      });
  }
}

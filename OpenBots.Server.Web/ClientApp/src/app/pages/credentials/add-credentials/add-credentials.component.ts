import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../../../@core/backend/common/api/http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDateService } from '@nebular/theme';

@Component({
  selector: 'ngx-add-credentials',
  templateUrl: './add-credentials.component.html',
  styleUrls: ['./add-credentials.component.scss'],
})
export class AddCredentialsComponent implements OnInit {
  credentialForm: FormGroup;
  currentUrlId: string;
  min: Date;
  title = 'Add';
  credentialsObj: Credential;
  isSubmitted = false;
  constructor(
    private fb: FormBuilder,
    private httpService: HttpService,
    private router: Router,
    private route: ActivatedRoute,
    protected dateService: NbDateService<Date>
  ) {}

  ngOnInit(): void {
    this.currentUrlId = this.route.snapshot.params['id'];
    this.min = this.dateService.addMonth(this.dateService.today(), 0);
    this.credentialForm = this.initializeForm();
    if (this.currentUrlId) {
      this.title = 'Update';
      this.getCredentialsById();
    }
  }

  initializeForm() {
    return this.fb.group({
      name: ['', [Validators.required]],
      provider: [''],
      domain: [''],
      userName: [''],
      passwordSecret: [''],
      certificate: [''],
      startDate: [''],
      endDate: [''],
    });
  }

  get controls() {
    return this.credentialForm.controls;
  }

  addCredentials(): void {
    this.isSubmitted = true;
    if (this.credentialForm.valid) {
      this.currentUrlId ? this.updateCredentials() : this.addCred();
    }
  }
  addCred() {
    console.log('value', this.credentialForm.value);
    this.httpService
      .post('Credentials', this.credentialForm.value, { observe: 'response' })
      .subscribe(
        (response) => {
          if (response && response.status == 201) {
            this.credentialForm.reset();
            this.router.navigate(['/pages/credentials']);
          }
        },
        (error) => {
          this.httpService.error(error.error.serviceErrors[0]);
          this.isSubmitted = false;
        }
      );
  }

  updateCredentials() {
    this.httpService
      .put(`Credentials/${this.currentUrlId}`, this.credentialForm.value, {
        observe: 'response',
      })
      .subscribe(
        (response) => {
          if (response) {
            this.credentialForm.reset();
            this.router.navigate(['/pages/credentials']);
          }
        },
        (error) => {
          this.httpService.error(error.error.serviceErrors[0]);
          this.isSubmitted = false;
        }
      );
  }

  getCredentialsById(): void {
    this.httpService
      .get(`Credentials/${this.currentUrlId}`)
      .subscribe((response: any) => {
        console.log('res', response);
        if (response) {
          this.credentialsObj = { ...response };
          this.credentialForm.patchValue(response);
        }
      });
  }
}

/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { NbMenuItem } from '@nebular/theme';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class PagesMenu {
  getMenu(): Observable<NbMenuItem[]> {
    const dashboardMenu: NbMenuItem[] = [
      {
        title: 'Dashboard',
        icon: 'shopping-cart-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },
    ];

    const menu: NbMenuItem[] = [
      {
        title: 'Teams',
        icon: 'lock-outline',
        children: [
          {
            title: 'All Teams Member',
            link: '/pages/users/teams-member',
          },
          {
            title: 'Request Teams Member',
            link: '/pages/users/request-teams',
          },
        ],
      },
      {
        title: 'Auth',
        icon: 'lock-outline',
        children: [
          // {
          //   title: 'Login',
          //   link: '/auth/login',
          // },
          // {
          //   title: 'Register',
          //   link: '/auth/register',
          // },
          // {
          //   title: 'Forget Password',
          //   link: '/auth/request-password',
          // },
          {
            title: 'Change Password',
            link: '/auth/reset-password',
          },
        ],
      },
      {
        title: 'Agents',
        icon: 'lock-outline',
        children: [
          {
            title: 'All Agents',
            link: '/pages/agents/list',
          },

          {
            title: 'Add Agent',
            link: '/pages/agents/new',
          },
        ],
      },
      {
        title: 'QueueItems',
        icon: 'lock-outline',
        children: [
          {
            title: 'All QueueItems',
            link: '/pages/queueitems',
          },
          {
            title: 'Add-QueueItem',
            link: '/pages/queueitems/new',
          },
          {
            title: 'Add-Queue',
            link: '/pages/queueitems/addqueue',
          },
        ],
      },
      {
        title: 'Audit Logs',
        icon: 'lock-outline',
        children: [
          {
            title: 'All Audit log',
            link: '/pages/audit/list',
          },
        ],
      },
      {
        title: 'Binary Objects',
        icon: 'lock-outline',
        children: [
          {
            title: 'All BinaryObjects',
            link: '/pages/binaryobjects',
          },
        ],
      },
      {
        title: 'Asset',
        icon: 'lock-outline',
        children: [
          {
            title: 'All Asset',
            link: '/pages/asset/list',
          },

          {
            title: 'Add Asset',
            link: '/pages/asset/add',
          },
        ],
      },
      {
        title: 'Credentials',
        icon: 'lock-outline',
        children: [
          {
            title: 'All Credentials',
            link: '/pages/credentials',
          },
          {
            title: 'Add Credentials',
            link: '/pages/credentials/add',
          },
        ],
      },
      {
        title: 'Jobs',
        icon: 'lock-outline',
        children: [
          {
            title: 'All Jobs',
            link: '/pages/job/list',
          },
        ],
      },
    ];

    // return of([...dashboardMenu,...user, ...menu]);
    return of([...dashboardMenu, ...menu]);
  }
}

import { NgModule } from '@angular/core';
import { AuditRoutingModule } from './audit-routing.module';
import { AllAuditLogComponent } from './all-audit-log/all-audit-log.component';
import { AuditService } from './audit.service';
import { ThemeModule } from '../../@theme/theme.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { GetAuditIdComponent } from './get-audit-id/get-audit-id.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { SharedModule } from '../../shared';

@NgModule({
  declarations: [AllAuditLogComponent, GetAuditIdComponent],
  imports: [
    SharedModule,
    AuditRoutingModule,
    ThemeModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxJsonViewerModule,
  ],
  exports: [],
  providers: [AuditService],
})
export class AuditModule {}

import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { Page } from '../../../interfaces/paginateInstance';
import { AuditService } from '../audit.service';

@Component({
  selector: 'ngx-all-audit-log',
  templateUrl: './all-audit-log.component.html',
  styleUrls: ['./all-audit-log.component.scss']
})
export class AllAuditLogComponent implements OnInit {
  show_allagents: any = [];
  show_service_name: any = [];
  select_serice_name: any = []
  service_name_page: boolean = false;
  sortDir = 1;
  view_dialog: any;
  del_id: any = [];
  toggle: boolean;
  feild_name: any = [];
  page: Page = {};
  value: any = [];
  service_name_Arr = [];
  show_perpage_size: boolean = false;
  per_page_num: any = []
  constructor(protected router: Router, private dialogService: NbDialogService,
    protected auditService: AuditService, private toastrService: NbToastrService) {
    this.service_name();
  }

  service_name() {
    this.service_name_Arr = [];
    this.auditService.get_servicename().subscribe(
      (data: any) => {
        this.show_service_name = data.serviceNameList
        // console.log(this.show_service_name)
        // this.show_service_name.forEach((item, index) => {
        //   if (this.service_name_Arr.findIndex(i => i.serviceName == item.serviceName) === -1) {
        //     this.service_name_Arr.push(item)
        //   }
        // });
        // this.show_service_name = this.service_name_Arr;
        // console.log(this.service_name_Arr)
      });
  }

  gotodetail(id) {
    this.router.navigate(['/pages/audit/get-audit-id'], { queryParams: { id: id } })
  }

  ngOnInit(): void {
    this.page.pageNumber = 1;
    this.page.pageSize = 5;
    this.pagination(this.page.pageNumber, this.page.pageSize);
  }


  sort(filter_value, vale) {
    if (this.service_name_page == true) {
      const skip = (this.page.pageNumber - 1) * this.page.pageSize;
      this.feild_name = filter_value + '+' + vale;
      this.auditService.get_AllAgent_order_by_servicename(`ServiceName eq '${this.select_serice_name}'`, this.page.pageSize, skip, this.feild_name).subscribe(
        (data: any) => {
          this.show_allagents = data.items
          this.value = data.items.serviceName
        });
    }
    else if (this.service_name_page == false) {
      const skip = (this.page.pageNumber - 1) * this.page.pageSize;
      this.feild_name = filter_value + '+' + vale;
      this.auditService.get_AllAgent_order(this.page.pageSize, skip, this.feild_name).subscribe(
        (data: any) => {
          this.show_allagents = data.items
          this.value = data.items.serviceName
        });
    }

  }




  get_service_name(val) {
    this.service_name_page = true
    this.select_serice_name = val
    const skip = (this.page.pageNumber - 1) * this.per_page_num;
    this.auditService.filter_servicename(`ServiceName eq '${this.select_serice_name}'`, this.page.pageSize, skip).subscribe(
      (data: any) => {
        this.show_allagents = data.items
        this.page.totalCount = data.totalCount;

      });
  }

  per_page(val) {
    if (this.service_name_page == true) {
      this.service_name_page = true
      this.per_page_num = val
      this.page.pageSize = val;
      this.show_perpage_size = true
      const skip = (this.page.pageNumber - 1) * this.per_page_num;
      if (this.feild_name.length != 0) {
        this.auditService.filter_servicename_order_by(`ServiceName eq '${this.select_serice_name}'`, this.page.pageSize, skip, this.feild_name).subscribe(
          (data: any) => {
            this.show_allagents = data.items
            this.page.totalCount = data.totalCount;

          });
      }
      else if (this.feild_name.length == 0) {
        this.auditService.filter_servicename(`ServiceName eq '${this.select_serice_name}'`, this.page.pageSize, skip).subscribe(
          (data: any) => {
            this.show_allagents = data.items
            this.page.totalCount = data.totalCount;

          });
      }
    }
    else if (this.service_name_page == false) {
      this.page.pageSize = val;
      this.per_page_num = val
      console.log(this.feild_name)
      const skip = (this.page.pageNumber - 1) * this.per_page_num;
      if (this.feild_name.length != 0) {
        this.show_perpage_size = true
        this.auditService.get_AllAgent_order(this.page.pageSize, skip, this.feild_name).subscribe(
          (data: any) => {
            this.show_allagents = data.items
            this.page.totalCount = data.totalCount;
          });
      }
      else if (this.feild_name.length == 0) {

        this.show_perpage_size = true
        this.auditService.get_AllAudits(this.page.pageSize, skip).subscribe(
          (data: any) => {
            this.show_allagents = data.items
            this.page.totalCount = data.totalCount;

          });
      }

    }
  }

  get_allagent(top, skip) {

    this.auditService.get_AllAudits(top, skip).subscribe(
      (data: any) => {
        this.show_allagents = data.items
        this.page.totalCount = data.totalCount;
      });
  }


  onSortClick(event, filter_val) {
    let target = event.currentTarget,
      classList = target.classList;
    if (classList.contains('fa-chevron-up')) {
      classList.remove('fa-chevron-up');
      classList.add('fa-chevron-down');
      let sort_set = 'desc'
      this.sort(filter_val, sort_set)
      this.sortDir = -1;

    } else {
      classList.add('fa-chevron-up');
      classList.remove('fa-chevron-down');
      let sort_set = 'asc'
      this.sort(filter_val, sort_set)
      this.sortDir = 1;
    }
  }
  pageChanged(event) {
    this.page.pageNumber = event;
    this.pagination(event, this.page.pageSize);
  }

  pagination(pageNumber, pageSize?) {

    if (this.service_name_page == true) {
      if (this.show_perpage_size == false) {
        const top: number = pageSize;
        const skip = (pageNumber - 1) * pageSize;
        this.service_name_page = true
        this.auditService.filter_servicename(`ServiceName eq '${this.select_serice_name}'`, top, skip).subscribe(
          (data: any) => {
            this.show_allagents = data.items
            this.page.totalCount = data.totalCount;

          });
      }
      else if (this.show_perpage_size == true) {
        if (this.feild_name.length != 0) {
          const top: number = this.per_page_num;
          const skip = (pageNumber - 1) * this.per_page_num;
          this.service_name_page = true
          this.auditService.filter_servicename_order_by(`ServiceName eq '${this.select_serice_name}'`, top, skip, this.feild_name).subscribe(
            (data: any) => {
              this.show_allagents = data.items
              this.page.totalCount = data.totalCount;

            });
        }
        else if (this.feild_name.length == 0) {
          const top: number = this.per_page_num;
          const skip = (pageNumber - 1) * this.per_page_num;
          this.service_name_page = true
          this.auditService.filter_servicename(`ServiceName eq '${this.select_serice_name}'`, top, skip).subscribe(
            (data: any) => {
              this.show_allagents = data.items
              this.page.totalCount = data.totalCount;

            });
        }

      }
    }
    else {
      if (this.show_perpage_size == false) {
        const top: number = pageSize;
        const skip = (pageNumber - 1) * pageSize;
        if (this.feild_name.length == 0) {
          this.get_allagent(top, skip);
        }
        else if (this.feild_name.length != 0) {
          this.auditService.get_AllAgent_order(top, skip, this.feild_name).subscribe(
            (data: any) => {
              this.show_allagents = data.items
              this.page.totalCount = data.totalCount;

            });
        }


      }
      else if (this.show_perpage_size == true) {

        if (this.feild_name.length == 0) {
          const top: number = pageSize;
          const skip = (pageNumber - 1) * pageSize;
          this.get_allagent(top, skip);
        }
        else if (this.feild_name.length != 0) {
          const top: number = this.per_page_num;
          const skip = (pageNumber - 1) * this.per_page_num;
          this.auditService.get_AllAgent_order(top, skip, this.feild_name).subscribe(
            (data: any) => {
              this.show_allagents = data.items
              this.page.totalCount = data.totalCount;

            });
        }
      }
    }
  }

}
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllAuditLogComponent } from './all-audit-log/all-audit-log.component';
import { GetAuditIdComponent } from './get-audit-id/get-audit-id.component';

const routes: Routes = [
  {
    path: 'list',
    component: AllAuditLogComponent,
  },
  {
    path: 'get-audit-id',
    component: GetAuditIdComponent,
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditRoutingModule { }

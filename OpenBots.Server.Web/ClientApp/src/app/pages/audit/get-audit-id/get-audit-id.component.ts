import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuditService } from '../audit.service';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';

@Component({
  selector: 'ngx-get-audit-id',
  templateUrl: './get-audit-id.component.html',
  styleUrls: ['./get-audit-id.component.scss']
})
export class GetAuditIdComponent implements OnInit {
  createdOn :any =[]
  show_allaudit: any = []
  changedToJson :any =[];
  changedFromJson :any =[]
  constructor(private acroute: ActivatedRoute,
    protected agentService: AuditService) {
    this.acroute.queryParams.subscribe(params => {
      this.get_allagent(params.id)
    
    });
  }

  ngOnInit(): void {
    let now = moment();
  
    console.log(moment.locale('de'))
    console.log('hello world', now.format('L'));  
    console.log('hello world', now.format('LL'));  
    console.log('hello world', now.format('LLL'));  
    console.log('hello world', now.format('LLLL'));  
        console.log(now.add(7, 'days').format('YYYY-MM-DD  HH:mm:ss a'));
  }

  get_allagent(id) {
    
    this.agentService.get_Audit_id(id).subscribe(
      (data: any) => { 
        this.show_allaudit = data;
       this.changedToJson = data.changedToJson;
       this.changedFromJson = data.changedFromJson;
       this.changedToJson = JSON.parse(this.changedToJson)
       this.changedFromJson = JSON.parse(this.changedFromJson)
       this.createdOn =  moment(data.createdOn).format('YYYY-MM-DD  HH:mm:ss a')
        console.log(moment(data.createdOn).format('LLLL'))
 
 

      //  show_allaudit.createdOn
      });
     
  }
   
   
}

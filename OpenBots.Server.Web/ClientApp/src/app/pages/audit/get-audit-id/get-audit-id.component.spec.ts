import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetAuditIdComponent } from './get-audit-id.component';

describe('GetAuditIdComponent', () => {
  let component: GetAuditIdComponent;
  let fixture: ComponentFixture<GetAuditIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetAuditIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetAuditIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

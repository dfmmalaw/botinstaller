import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
const momentConstructor = moment;
@Pipe({
  name: 'timeDate',
})
export class TimeDatePipe implements PipeTransform {
  transform(value: moment.MomentInput, ...args: any[]): string {
    if (!value) {
      return '';
    }
    // let localTime = moment.utc(value).toDate();
    // console.log('local', localTime);
    // console.log(moment(localTime).format(args[0]));
    // console.log(moment.utc(value).local());
    // moment.locale('de');
    // console.log('value', value);
    // console.log('args', args);
    // console.log('time ', momentConstructor(value).format(args[0]));
    return momentConstructor(value).format(args[0]);
  }
}

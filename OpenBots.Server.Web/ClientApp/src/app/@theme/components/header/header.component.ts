/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { LayoutService } from '../../../@core/utils';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { User } from '../../../@core/interfaces/common/users';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: User;

  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private layoutService: LayoutService,
    protected router: Router,
  ) {}

  getMenuItems() {
    // const userLink = this.user ? '/pages/users/current/' : '';
    return [
      // { title: 'Profile', link: userLink, queryParams: { profile: true } },
      { title: 'Log out', link: '/auth/logout' },
    ];
  }

  ngOnInit() {
    
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
  logout() {
    localStorage.clear();

    // localStorage.removeItem('accessToken');
    // localStorage.removeItem('refreshToken');
    // localStorage.removeItem('UserInfo');
    // localStorage.removeItem('UserEmail');
    // localStorage.removeItem('isPasswordSet');
    // localStorage.removeItem('UserName');
    // localStorage.removeItem('return_email')
    // localStorage.removeItem('OrganizationListLength');
    // localStorage.removeItem('ActiveOrganizationID');
    // localStorage.removeItem('ShareProcess_Flag')
    // localStorage.removeItem('ShareproceActiveOrganizationID')
    // localStorage.removeItem('ActiveOrganizationName');
    // localStorage.removeItem('personId');
    // localStorage.removeItem('isAdministrator');
    // localStorage.removeItem('isUserConsentRequired');
    // localStorage.removeItem('IsJoinOrgRequestPending');
    // localStorage.setItem('IsOrganizationChanged', 'false');
    // localStorage.removeItem('setFlag');

    this.router.navigate(['auth/login']);

  }

   

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();
    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }
}

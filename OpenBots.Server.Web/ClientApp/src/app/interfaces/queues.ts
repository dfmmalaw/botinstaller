export interface Queues {
    createdBy: string;
createdOn: string;
deleteOn: null
deletedBy: string;
description: string;
id: string;
isDeleted: boolean;
name: string;
timestamp: string;
updatedBy: null
updatedOn: null
}
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { NbToastrService } from '@nebular/theme';
import { AuthService } from '../@core/backend/common/services/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private toastrService: NbToastrService, private authService: AuthService) { }
    
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<any> {
    const token = localStorage.getItem('accessToken');
    if (token) {
      request = this.attachToken(request, token);
    }
    return next.handle(request).pipe(catchError(error => {
      if(error.status == 401){
        return this.handleError(request, next)
      }
      return throwError(error);
    }));
  }
   handleError(request: HttpRequest<unknown>, next: HttpHandler){
  return this.authService.refreshToken().pipe(
    switchMap((token: any) => {
      localStorage.setItem('accessToken', token.jwt);
      localStorage.setItem('refreshToken', token.refreshToken);
      return next.handle(this.attachToken(request, token.jwt));
    })
  )
  }

attachToken(request: HttpRequest<unknown>, token: string) {
    return request.clone({
      setHeaders: { 'Authorization': `Bearer ${token}` }
    });
  }
}

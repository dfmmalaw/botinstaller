import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { NbToastrService } from '@nebular/theme';

@Injectable()
export class HttpService {
  headerData = new HttpHeaders({ 'Content-Type': 'application/json' });
  get apiUrl(): string {
    return environment.apiUrl;
  }

  constructor(
    private http: HttpClient,
    private toastrService: NbToastrService
  ) {}

  get(endpoint: string, options?): Observable<any> {
    return this.http.get(`${this.apiUrl}/${endpoint}`, options);
  }

  post(endpoint: string, data, options?): Observable<any> {
    return this.http.post(`${this.apiUrl}/${endpoint}`, data, options);
  }

  put(endpoint: string, data, options?): Observable<any> {
    return this.http.put(`${this.apiUrl}/${endpoint}`, data, options);
  }

  delete(endpoint: string, options?): Observable<any> {
    return this.http.delete(`${this.apiUrl}/${endpoint}`, options);
  }

  GetTokenWithUserInfo(Email: string, Password: string): Observable<any> {
    var loginCredentials = {
      email: Email,
      password: Password,
    };
    return this.http.post(`${this.apiUrl}` + '/Auth/token', loginCredentials, {
      headers: this.headerData,
    });
  }

  ////auth api's changepassword after login

  ChangePassword(password_key) {
    let headerData = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(
      `${this.apiUrl}` + '/Auth/ChangePassword',
      password_key
    );
  }

  ///auth api's set password after if falg return isforcechangepassword  login
  SetForgotPassword(password: string, token, userID: string) {
    var formData = {
      newPassword: password,
      userId: userID,
      token: token,
    };
    return this.http.put(`${this.apiUrl}` + '/Auth/SetPassword', formData, {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
        'Content-Type': 'application/json',
      }),
    });
  }

  /////auth api's forgot password before login
  ForgotPassword(email, company) {
    const formData = { email: email, company: company };
    const headerData = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(`${this.apiUrl}` + '/Auth/ForgotPassword', formData, {
      headers: headerData,
    });
  }

  success(param): void {
    this.toastrService.success(`${param}`);
  }

  error(param): void {
    this.toastrService.danger(`${param}`);
  }

  warning(param): void {
    this.toastrService.warning(`${param}`);
  }
}

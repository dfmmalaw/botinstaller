import { Injectable } from '@angular/core';
import { HttpService } from '../api/http.service';
import { HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/internal/operators/tap';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  headerData = new HttpHeaders({ 'Content-Type': 'application/json' });
  constructor(private httpService: HttpService, private toastrService: NbToastrService, private router : Router) { }

  refreshToken() {
    const obj = {
      token: localStorage.getItem('accessToken'),
      refreshToken: localStorage.getItem('refreshToken')
    };
    return this.httpService.post('Auth/Refresh', obj, this.headerData).pipe((tap(response => {
      this.storeToken(response);
    },
   error => {
     if(error.status == 400){
      this.toastrService.danger(error.error.serviceErrors[0]);
      this.router.navigate(['auth/login'])
      localStorage.clear();
     } else {
      this.toastrService.danger(error.error.serviceErrors[0]);
      this.router.navigate(['auth/login'])
      localStorage.clear();
     }
   }
    )
    ));
  }

  storeToken(data) {
    localStorage.setItem('accessToken', data.jwt);
    localStorage.setItem('refreshToken', data.refreshToken);
  }
}

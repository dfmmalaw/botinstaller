using System;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using OpenBots.Server.DataAccess;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Model.Membership;
using OpenBots.Server.Security;
using OpenBots.Server.Security.DataAccess;
using OpenBots.Server.WebAPI.Controllers;
using OpenBots.Server.Web.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.ApplicationInsights;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Filters;
using HealthChecksUISettings = HealthChecks.UI.Configuration.Settings;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using OpenBots.Server.Web.Hubs;

namespace OpenBots.Server.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureKestrel(services);

            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = 209715200;
                x.MultipartBodyLengthLimit = 209715200;
                x.MultipartHeadersLengthLimit = 209715200;
            });

            DependencyManager.ConfigureServices(services);

            //services.AddCors(options =>
            //{
            //    options.AddPolicy("EnableCORS", builder => builder
            //    .WithOrigins("http://localhost:4200")
            //    .AllowAnyMethod()
            //    .AllowAnyHeader());
            //});

            string[] origins = new string[2];
            origins[0] = "http://localhost:4200";
            origins[1] = "https://openbotsserver-dev.azurewebsites.net";

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder => builder
                .WithOrigins(origins)
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
            });

            services
                    .AddControllers()
                    .AddNewtonsoftJson();

            ConfigureLogging(services);

            ConfigureMVCWithSPA(services);

            ConfigureSignalR(services);

            //services.AddCors(options =>
            //{
            //    options.AddPolicy("EnableCORS", builder =>
            //    {
            //        builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().Build();
            //    });
            //});

      

            services.AddApplicationInsightsTelemetry();
            services.AddMvcCore();

            ConfigureDbContext(services);
            //ConfigureDatabases(services);
            ConfigureIdentity(services);
            ConfigureSwaggerDocumentation(services);

            AddHealthCheck(services);
        }

        private void ConfigureIdentity(IServiceCollection services)
        {
            //Security configuration
            services.AddIdentity<ApplicationUser, IdentityRole>(config =>
            {
                config.Password.RequireDigit = false;
                config.Password.RequiredLength = 1;
                config.Password.RequireLowercase = false;
                config.Password.RequireNonAlphanumeric = false;
                config.Password.RequireUppercase = false;
            })
                .AddUserManager<ApplicationIdentityUserManager>()
                .AddEntityFrameworkStores<SecurityDbContext>()
                .AddDefaultTokenProviders();

            #region Add Authentication
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Tokens:Key"]));
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(config =>
            {
                config.RequireHttpsMetadata = false;
                config.SaveToken = true;
                config.TokenValidationParameters = new TokenValidationParameters()
                {
                    IssuerSigningKey = signingKey,
                    ValidateAudience = true,
                    ValidAudience = this.Configuration["Tokens:Audience"],
                    ValidateIssuer = true,
                    ValidIssuer = this.Configuration["Tokens:Issuer"],
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ClockSkew = TimeSpan.Zero
                };
                config.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }
                        return Task.CompletedTask;
                    }
                };
            });

            services.AddIdentityCore<ApplicationUser>(options => { }).AddEntityFrameworkStores<SecurityDbContext>();
            services.AddScoped<IUserStore<ApplicationUser>, UserOnlyStore<ApplicationUser, SecurityDbContext>>();
            services.Configure<IdentityOptions>(options =>
            {
                options.User.RequireUniqueEmail = true;
            });

            #endregion
        }

        private static void ConfigureSignalR(IServiceCollection services)
        {
            services.AddSignalR();
        }

        private static void ConfigureLogging(IServiceCollection services)
        {
            services.AddLogging(
                logging =>
                {
                    logging.AddApplicationInsights();
                    logging.AddFilter<ApplicationInsightsLoggerProvider>("", LogLevel.Trace); //you can set the logLevel here
                });
        }

        private static void ConfigureMVCWithSPA(IServiceCollection services)
        {
            services.AddMvc(options => options.Filters.Add(typeof(AccessActionFilter))).AddMvcOptions(o => o.EnableEndpointRouting = false)
              .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
              .ConfigureApiBehaviorOptions(options =>
              {
                  options.InvalidModelStateResponseFactory = context =>
                  {
                      var problems = new ServiceBadRequest(context, null);
                      return new BadRequestObjectResult(problems);
                  };
              });

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });


        }

        protected virtual IServiceCollection ConfigureDbContext(IServiceCollection services)
        {
            var isSqlServerConnection = Configuration.GetValue<bool>("DbOption:UseSqlServer");

            if (isSqlServerConnection)
            {
                services.AddDbContext<SecurityDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("Sql")).UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll),
                ServiceLifetime.Transient);

                services.AddDbContext<StorageContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("Sql")).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking),
                ServiceLifetime.Transient);
            }
            else
            {
                services.AddDbContext<SecurityDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("Sql")).UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll),
                ServiceLifetime.Transient);

                services.AddDbContext<StorageContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("Sql")).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
             , ServiceLifetime.Transient);
            }

            return services;
        }

        private void ConfigureDatabases(IServiceCollection services)
        {
            services.AddDbContext<StorageContext>(options =>
             options.UseSqlServer(Configuration.GetConnectionString("Default")).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
             , ServiceLifetime.Transient);

            //DB Context for security
            services.AddDbContext<SecurityDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("Default")).UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll)
            , ServiceLifetime.Transient);
        }

        private void ConfigureSwaggerDocumentation(IServiceCollection services)
        {
            if (bool.Parse(Configuration["App:SwaggerEnabled"]))
            {
                services.AddSwaggerExamplesFromAssemblyOf<PersonEmailsController>();
                services.AddSwaggerExamplesFromAssemblyOf<Organization>();
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "OpenBots Server API", Version = "v1" });

                    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                    {
                        Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                        Name = "Authorization",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.Http,
                        Scheme = "bearer",
                        BearerFormat = "JWT"
                    });
                    c.OperationFilter<SecurityRequirementsOperationFilter>();

                    c.ExampleFilters();
                    var apiCommentPath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "OpenBots.Server.Web.xml");
                    var modelCommentPath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "OpenBots.Server.Model.xml");
                    c.IncludeXmlComments(apiCommentPath, true);
                    c.IncludeXmlComments(modelCommentPath, true);
                });
            }
        }

        private void AddHealthCheck(IServiceCollection services)
        {
            if (bool.Parse(Configuration["HealthChecks:IsEnabled"]))
            {
                services.AddHealthChecks()
                    .AddCheck<AppVersionHealthCheck>("Version")
                    .AddDbContextCheck<StorageContext>("Database")
                    .AddApplicationInsightsPublisher();

                var healthCheckUISection = Configuration.GetSection("HealthChecks")?.GetSection("HealthChecksUI");
                services.Configure<HealthChecksUISettings>(settings =>
                {
                    healthCheckUISection.Bind(settings, c => { c.BindNonPublicProperties = true; });
                });
                services.AddHealthChecksUI().AddInMemoryStorage();
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHttpsRedirection();

            }



            var logger = loggerFactory.CreateLogger("Logging");
            app.ConfigureExceptionHandler(logger);
            //app.UseCors("EnableCORS");
            app.UseCors("CorsPolicy");
            app.UseDefaultFiles();
            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }
            app.UseAuthentication();
            app.UseMvc();
            app.UseRouting();
            app.UseAuthorization();

            // UpdateDatabase(app);

            //app.UseSignalR(routes =>
            //{
            //    routes.MapHub<NotificationHub>("/notificationHub");
            //});

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<NotificationHub>("/notification");
                ConfigureHealthcheck(endpoints, app);
            });


            if (bool.Parse(Configuration["App:SwaggerEnabled"]))
            {
                // Enable middleware to serve generated Swagger as a JSON endpoint.
                app.UseSwagger();

                // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
                // specifying the Swagger JSON endpoint.
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "OpenBots API v1");
                    //c.RoutePrefix = string.Empty;
                });
            }


            // Added Service Bus functionality for Message Processing from within Service 
            // or Other Microservices
            var bus = app.ApplicationServices.GetService<IQueueSubscriber>();
            if (bus != null)
                bus.Register();

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                    //spa.UseProxyToSpaDevelopmentServer("http://localhost:4200");
                }
            });
        }
        private void ConfigureHealthcheck(IEndpointRouteBuilder endpoints, IApplicationBuilder app)
        {
            if (bool.Parse(Configuration["HealthChecks:IsEnabled"]))
            {
                string healthcheckEndpoint = Configuration["HealthChecks:Endpoint"];
                if (string.IsNullOrWhiteSpace(healthcheckEndpoint))
                    healthcheckEndpoint = "/health";
                app.UseHealthChecks(healthcheckEndpoint, new HealthCheckOptions()
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
            }

            if (bool.Parse(Configuration.GetSection("HealthChecks").GetSection("HealthChecksUI")["HealthChecksUIEnabled"]))
            {
                string healthcheckUI = Configuration.GetSection("HealthChecks").GetSection("HealthChecksUI")["UIRelativePath"];
                string healthcheckAPI = Configuration.GetSection("HealthChecks").GetSection("HealthChecksUI")["ApiRelativePath"];
                if (string.IsNullOrWhiteSpace(healthcheckUI))
                    healthcheckUI = "/healthcheck-ui";

                if (string.IsNullOrWhiteSpace(healthcheckAPI))
                    healthcheckAPI = "/healthcheck-api";

                endpoints.MapHealthChecksUI(a => { a.UIPath = healthcheckUI; a.UseRelativeApiPath = true; a.ApiPath = healthcheckAPI; });
            }
        }

        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<SecurityDbContext>())
                {
                    context.Database.Migrate();
                }
                using (var context = serviceScope.ServiceProvider.GetService<StorageContext>())
                {
                    context.Database.Migrate();
                }

           
            }
        }

        private void ConfigureKestrel(IServiceCollection services)
        {
            if (bool.Parse(Configuration["Kestrel:IsEnabled"]))
            {
                services.Configure<Microsoft.AspNetCore.Server.Kestrel.Core.KestrelServerOptions>(options =>
            {
                options.Listen(new System.Net.IPEndPoint(System.Net.IPAddress.Any, 443),
                    listenOptions =>
                    {
                        var certPassword = Configuration.GetValue<string>("Kestrel:Certificates:Default:Password");
                        var certPath = Configuration.GetValue<string>("Kestrel:Certificates:Default:Path");
                        var cert = new System.Security.Cryptography.X509Certificates.X509Certificate2(certPath, certPassword);
                        listenOptions.UseHttps(new HttpsConnectionAdapterOptions()
                        {
                            ServerCertificate = cert
                        });
                    });

                options.AddServerHeader = false;
                options.Limits.MaxRequestLineSize = 16 * 1024; // 16KB Limit
            });
            }
        }
    }
}

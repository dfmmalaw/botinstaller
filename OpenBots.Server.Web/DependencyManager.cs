﻿using OpenBots.Server.Business;
using OpenBots.Server.Core;
using OpenBots.Server.DataAccess;
using OpenBots.Server.DataAccess.Repositories;
using OpenBots.Server.Infrastructure;
using OpenBots.Server.Infrastructure.Azure;
using OpenBots.Server.Infrastructure.Azure.Email;
using OpenBots.Server.Infrastructure.Email;
using OpenBots.Server.Model.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenBots.Server.Web
{
    public static class DependencyManager
    {
        public static void ConfigureServices(IServiceCollection services)
        {
           
           // services.AddTransient<UserSecurityContext>();

           
            

            services.AddTransient(typeof(IAccessRequestRepository), typeof(AccessRequestRepository));
            services.AddTransient(typeof(IOrganizationRepository), typeof(OrganizationRepository));

            services.AddTransient(typeof(IOrganizationUnitRepository), typeof(OrganizationUnitRepository));
            services.AddTransient(typeof(IOrganizationMemberRepository), typeof(OrganizationMemberRepository));
            services.AddTransient(typeof(IOrganizationUnitMemberRepository), typeof(OrganizationUnitMemberRepository));
            services.AddTransient(typeof(IOrganizationSettingRepository), typeof(OrganizationSettingRepository));

            services.AddTransient(typeof(IPersonRepository), typeof(PersonRepository));
            services.AddTransient(typeof(IPersonEmailRepository), typeof(PersonEmailRepository));
            services.AddTransient(typeof(IPersonPhoneRepository), typeof(PersonPhoneRepository));
            services.AddTransient(typeof(IEmailVerificationRepository), typeof(EmailVerificationRepository));
            services.AddTransient(typeof(IAspNetUsersRepository), typeof(AspNetUsersRepository));


            services.AddTransient(typeof(IMembershipManager), typeof(MembershipManager));
            services.AddTransient(typeof(IAccessRequestsManager), typeof(AccessRequestsManager));
            services.AddTransient(typeof(ITermsConditionsManager), typeof(TermsConditionsManager));
            services.AddTransient(typeof(IPasswordPolicyRepository), typeof(PasswordPolicyRepository));
            services.AddTransient(typeof(IOrganizationManager), typeof(OrganizationManager));

            services.AddTransient(typeof(ILookupValueRepository), typeof(LookupValueRepository));
            services.AddTransient(typeof(IApplicationVersionRepository), typeof(ApplicationVersionRepository));
            services.AddTransient(typeof(IQueueItemRepository), typeof(QueueItemRepository));
            services.AddTransient(typeof(IQueueItemManager), typeof(QueueItemManager));
            services.AddTransient(typeof(IBinaryObjectRepository), typeof(BinaryObjectRepository));
            services.AddTransient(typeof(IAgentRepository), typeof(AgentRepository));
            services.AddTransient(typeof(IAgentManager), typeof(AgentManager));
            services.AddTransient(typeof(IAssetRepository), typeof(AssetRepository));

            services.AddTransient(typeof(IJobRepository), typeof(JobRepository));
            services.AddTransient(typeof(IJobManager), typeof(JobManager));
            services.AddTransient(typeof(ICredentialRepository), typeof(CredentialRepository));
            services.AddTransient(typeof(ICredentialManager), typeof(CredentialManager));
            services.AddTransient(typeof(IUserAgreementRepository), typeof(UserAgreementRepository));
            services.AddTransient(typeof(IUserConsentRepository), typeof(UserConsentRepository));
            services.AddTransient(typeof(IAuditLogRepository), typeof(AuditLogRepository));
            services.AddTransient(typeof(IQueueRepository), typeof(QueueRepository));

            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();


            //Blob Storage

            // Queue & Service Bus
            services.AddTransient(typeof(AzureQueueSetting), typeof(AzureQueueSetting));
            services.AddTransient(typeof(IEntityOperationEventSink), typeof(NullEntityOperationEventSink));
            services.AddTransient(typeof(IQueuePublisher), typeof(NullQueueManager));
            services.AddTransient(typeof(IQueueSubscriber), typeof(NullQueueManager));

            // Email Services
            
            services.AddTransient(typeof(SendEmailSetting), typeof(SendEmailSetting));
            services.AddTransient(typeof(SmtpEmailSetting), typeof(SmtpEmailSetting));
            services.AddTransient(typeof(AzureSendEmailSetting), typeof(AzureSendEmailSetting));
            services.AddTransient(typeof(ISendEmailChore), typeof(AzureSendEmailChore));
            services.AddTransient(typeof(IEmailManager), typeof(EmailManager));
        }

    }
}
